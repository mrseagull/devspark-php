<?php

namespace DevSpark;

use DevSpark\Engine\Config;
use DevSpark\Engine\Core;
use DevSpark\Engine\Knowledgebase\Knowledgebase;
use DevSpark\Engine\Utils;
use DevSpark\Utils\FileUpload;
use DevSpark\Utils\ResourceHelper;
use DOMDocument;
use DOMXPath;
use Illuminate\Support\Facades\Route;

class Router
{



    private static function isApiPath() {
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        return strpos($path, '/api') !== false;
    }


    private static function isLaravel()
    {

        if (app() instanceof \Illuminate\Foundation\Application) {
            return true;
        } else {
            return false;
        }
    }

    private static $routerRef = null;

    private static function post($url, $callback)
    {
        if (self::isLaravel()) {
            Route::post($url, $callback);
        } else {
            if (self::$routerRef) {
                self::$routerRef->post($url, $callback);
            }
        }
    }

    private static function get($url, $callback)
    {
        if (self::isLaravel()) {
            Route::get($url, $callback);
        } else {
            if (self::$routerRef) {
                self::$routerRef->get($url, $callback);
            }
        }
    }

    private static function group($url, $callback)
    {
        if (self::isLaravel()) {
            Route::prefix($url)->group($callback);
        } else {
            if (self::$routerRef) {
                self::$routerRef->group(['prefix' => $url], $callback);
            }
        }
    }




    public static function registerRoutes($router = null)
    {

        
       Core::$packagePath  = realpath(dirname(__FILE__));
        
        self::$routerRef = $router;

        if (!function_exists('app_path')) {
            function app_path($path = '')
            {
                return app('path') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
            }
        }


        view()->addNamespace('barabaas', Core::$packagePath.'/resources/views');




        self::post('file/{type}/upload', function ($type, \Illuminate\Http\Request $request) {

            $file = FileUpload::init($request, $type);
            return response()->json($file, 200);
        });


        self::post('manager/graph', function (\Illuminate\Http\Request $request) {

            $timezone =  $request->header('timezone');
            if ($timezone) {
                date_default_timezone_set($timezone);
            }

            return \DevSpark\GraphQL\ManagerGraphQL::init($request->all());
        });

        self::get('manager/graph/codegen', function (\Illuminate\Http\Request $request) {

            $timezone =  $request->header('timezone');
            if ($timezone) {
                date_default_timezone_set($timezone);
            }

            return \DevSpark\GraphQL\ManagerGraphQL::init($request->all(), true);
        });
        self::get('manager/playground', function (\Illuminate\Http\Request $request) {

            return (string) view('barabaas::playground', ["url" => (self::isApiPath() ? "/api" : "")."/manager/graph"]);
         
        });


        self::post('manager/payment/callback', function (\Illuminate\Http\Request $request) {




            $fields = $request->fields ?? [];



            $manager = mDB::collection($fields['key'])->findOne([
                "_id" => mDB::id($fields['user']),
            ]);


            $amount = $fields['amount'] ?? null;

            if (isset($manager->_id) && $amount) {

                mDB::collection($fields['key'] . "_payments")->insertOne([
                    "manager"     => $manager->_id,
                    "amount"      => (int) $amount,
                    "currency"    => "RUB",
                    "description" => "Пополнение счета",
                    "created_at"  => time(),
                ]);

                $balance = 0;

                if (isset($manager->balance)) {
                    $balance = $manager->balance;
                }
                $balance = $balance + $amount;

                mDB::collection($fields['key'])->updateOne([
                    '_id' => $manager->_id,
                ], [
                    '$set' => [
                        'balance' => $balance,
                    ],
                ]);

                return response()->json([], 200);
            }
            return response()->json([], 200);
        });



        self::get("admin/assets/{file}.{encoding}", function ($file, $encoding) {
            ResourceHelper::get($file, $encoding);
        });


        self::group("knowledgebase/", function () {




            self::get("assets/{file}.{encoding}", function ($file, $encoding) {
                Knowledgebase::getAsset($file, $encoding);
            });





            self::get("/update", function () {
                Knowledgebase::update();
            });

            self::group("{lang}/", function ($lang) {



                self::get("/", function ($lang) {
                    if (Knowledgebase::setLang($lang))
                        return view('barabaas::knowledgebase.main');
                    else
                        return view('barabaas::knowledgebase.404');
                });



                self::get("/changelog", function ($lang) {
                    if (Knowledgebase::setLang($lang))
                        return view('barabaas::knowledgebase.changelog');
                    else
                        return view('barabaas::knowledgebase.404');
                });



                self::get("/{key}", function ($lang, $key) {
                    Knowledgebase::setLang($lang);
                    return view('barabaas::knowledgebase.category', ['key' => $key]);
                });

                self::get("/{key}/{articleKey}", function ($lang, $key, $articleKey) {
                    Knowledgebase::setLang($lang);
                    return view('barabaas::knowledgebase.article', ['key' => $key, 'articleKey' => $articleKey]);
                });
            });
        });



        self::get("admin", function () {

            $hash =  \Composer\InstalledVersions::getReference("devspark/devspark-php") ?? "none";


            $url = (string)  url('/').(self::isApiPath() ? "/api" : "");
          
           
            $admin =  view('barabaas::admin', [
                'title' => Config::$title ?? "",
                'url' => $url ?? "",
                'logo' => Config::$logo ?? "",
                'hash' => $hash ?? ""
            ]);

            return  response($admin, 200);
        });


        self::group("cron/", function () {



            self::get("/fields/migration", function () {
                //migration from old format fields


                $models = Core::getAllModels();
                foreach ($models as $model) {
                    if ($model->fields) {
                        foreach ($model->fields as $field) {
                            if ($field->input  == 'rangedate') {


                                $res = mDB::collection($model->collection)->aggregate([
                                    [
                                        '$match' => [
                                            $field->key => ['$exists' => true]

                                        ]
                                    ], [
                                        '$addFields' => [
                                            'type' . $field->key => ['$type' => '$' . $field->key]
                                        ]
                                    ],

                                ])->toArray();

                                echo  $field->key."<br/>";

                                foreach ($res as $key => $value) {
                                    if ($value['type' . $field->key] == 'array') {

                                        $set = [
                                            $field->key => [
                                                'from' => $value[$field->key][0],
                                                'to' => $value[$field->key][1],
                                            ],
                                            'oldFormat-' . $field->key => $value[$field->key]
                                        ];

                                        mDB::_collection($model->collection)->updateOne([
                                            "_id" => $value->_id
                                        ], [
                                            '$set' => $set
                                        ]);

                                        //    dd($value->_id);
                                        //   dd($set);

                                    }

                                    echo  $value->_id." ";

                                }
                            }
                        }
                    }
                }
            });
        });
    }
}
