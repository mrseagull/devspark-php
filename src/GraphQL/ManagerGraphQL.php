<?php

namespace DevSpark\GraphQL;

use DevSpark\Engine\Auth;
use BaraBaaS\Schema\CodeGen;
use DevSpark\GraphQL\Manager\Types;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\Error;
use GraphQL\Error\FormattedError;
use GraphQL\GraphQL;
use GraphQL\Type\Schema;



class ManagerGraphQL
{


    public static function init($input, $codegen = false)
    {
        try {



           

        
            $query = $input['query'] ?? null;

            // Получение переменных запроса
            $variables = isset($input['variables']) ? $input['variables'] : null;

            $schema = new Schema([
                'query'    => Types::query(),
                'mutation' => Types::mutation(),
            ]);

            if($codegen){
          
                    echo  view("barabaas::codegen", ['types' => CodeGen::getTSType($schema), 'requests' => CodeGen::getRequests($schema)]);
                    exit;
            }

            $rootResolver = [];

            $myErrorFormatter = function (Error $error) use ($query, $variables) {


                if ($query) {
                    $msg = urlencode("Error GRAPH QL BaraBaaS:  variable:" . json_encode($variables, JSON_UNESCAPED_UNICODE) . " input:" . json_encode($query, JSON_UNESCAPED_UNICODE) . " -  output :" . json_encode($error, JSON_UNESCAPED_UNICODE));
                    @file_get_contents("https://loger.apiloc.ru/chat/" . env("DORIS") . "/send?msg=" . $msg);
                }



                return FormattedError::createFromException($error);
            };

            $myErrorHandler = function (array $errors, callable $formatter) {

                return array_map($formatter, $errors);
            };

            $debug  = DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE;
            $result = GraphQL::executeQuery($schema, $query, $rootResolver, null, $variables, null, null)
                ->setErrorFormatter($myErrorFormatter)
                ->setErrorsHandler($myErrorHandler)->toArray($debug);

            return response()->json($result, 200);
        } catch (\Exception $e) {

            $result = [
                'error' => [
                    'message' => $e->getMessage(),
                ],
            ];
            return response()->json($result, 422);
        }
    }
}
