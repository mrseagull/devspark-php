<?php

namespace DevSpark\GraphQL\Manager;


use DevSpark\Engine\Auth;
use BaraBaaS\mDB;
use DevSpark\Engine\Core;
use DevSpark\Utils\Response;
use DevSpark\Engine\Config;
use BaraBaaS\Utils;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use BaraBaaS\Chat;

class QueryType extends ObjectType
{




    public function __construct()
    {


        $fields = [

            'unreadCnt' => [
                "type" => Type::int(),
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        return  rand(1, 10);
                    }

                    Auth::accessManager(true);

                    $curentManaget = Auth::getCurrentAuthenticatedManager();

                    return mDB::collection("chats")->aggregate([
                        [
                            '$match' => [
                                'unread.' . $curentManaget->_id => ['$gt' => 0]
                            ]
                        ],
                        [
                            '$group' => [
                                "_id" => 'null',
                                "count" => [
                                    '$sum' => 1
                                ]
                            ]
                        ]
                    ])->toArray()[0]['count'] ?? 0;
                }
            ],
            'chats' => [
                "type" => Type::listOf(Types::ChatType()),

                'resolve' => function ($root, $args) {




                    if (Auth::isDemo()) {
                        return  [];
                    }

                    Auth::accessManager(true);

                    $curentManaget = Auth::getCurrentAuthenticatedManager();

                    $aggregate = [
                        ['$match' => ["members" => $curentManaget->_id]],

                        ['$lookup' => ['from' => 'messages', 'localField' => '_id', 'foreignField' => 'chat', 'as' => 'messages']],

                        ['$lookup' => ['from' => 'users', 'localField' => 'members', 'foreignField' => '_id', 'as' => 'users']],
                        ['$lookup' => ['from' => 'managers', 'localField' => 'members', 'foreignField' => '_id', 'as' => 'managers']]
                    ];

                    $managerCollesctions = [];
                    foreach (Auth::$additionalManagers as $manager) {
                        $managerCollesctions[] = '$' . $manager['collection'];
                        $aggregate[] =   ['$lookup' => ['from' => $manager['collection'], 'localField' => 'members', 'foreignField' => '_id', 'as' => $manager['collection']]];
                    }

                    $aggregate[] = ['$addFields' => [
                        'membersData' => ['$concatArrays' => ['$users', '$managers', ...$managerCollesctions]],
                        'lastMessage' => ['$last' => '$messages'],
                    ]];

                    $aggregate[] = ['$addFields' => [
                        'updated_at' => '$lastMessage.created_at'
                    ]];



                    $aggregate[] = ['$sort' => [
                        "open" => -1,
                        'lastMessage.created_at' => -1,

                    ]];
                    $aggregate[] = ['$limit' => 100];

                    return mDB::collection("chats")->aggregate($aggregate);
                }

            ],
            'messages' => [
                "type" => Type::listOf(Types::ChatMessageType()),
                'args'        => [
                    "chat"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                ],
                'resolve' => function ($root, $args) {


                    if (Auth::isDemo()) {
                        return  [];
                    }


                    Auth::accessManager(true);




                    $curentManaget = Auth::getCurrentAuthenticatedManager();

                    $aggregate = [
                        ['$match' => ["chat" => mDB::id($args['chat'])]],
                        ['$sort' => [
                            'created_at' => -1
                        ]],
                        ['$limit' => 100],
                        ['$lookup' => ['from' => 'users', 'localField' => 'from', 'foreignField' => '_id', 'as' => 'users']],

                        ['$addFields' => ['users.type' => 'users']],

                        ['$lookup' => ['from' => 'managers', 'localField' => 'from', 'foreignField' => '_id', 'as' => 'managers']],
                        ['$addFields' => ['managers.type' => 'managers']],
                    ];

                    $managerCollesctions = [];
                    foreach (Auth::$additionalManagers as $manager) {
                        $managerCollesctions[] = '$' . $manager['collection'];
                        $aggregate[] =   ['$lookup' => ['from' => $manager['collection'], 'localField' => 'from', 'foreignField' => '_id', 'as' => $manager['collection']]];
                        $aggregate[] =    ['$addFields' => [$manager['collection'] . '.type' => $manager['collection']]];
                    }

                    $aggregate[] = ['$addFields' => [
                        'from' => ['$concatArrays' => ['$users', '$managers', ...$managerCollesctions]],
                    ]];


                    $aggregate[] = ['$unwind' => [
                        'path' => '$from'
                    ]];



                    $data = array_reverse(mDB::collection("messages")->aggregate($aggregate)->toArray());





                    foreach ($data as $index => $item) {

                        $before = $data[$index - 1] ?? null;
                        $next = $data[$index + 1] ?? null;

                        if (($before['from']["_id"] ?? null) != ($item['from']['_id'] ?? null)) {
                            $data[$index]['first'] = true;
                        }

                        if ($data[$index]['from']["_id"] != $curentManaget->_id)
                            $data[$index]['type'] = "received";
                        else
                            $data[$index]['type'] = "sent";

                        if (($next['from']["_id"] ?? null) != ($item['from']['_id'] ?? null)) {
                            $data[$index]['last'] = true;
                            $data[$index]['tail'] = true;
                        }
                    }

                    Chat::cleanUnread($args['chat'],  $curentManaget->_id);

                    return $data;
                }

            ],



            'paymentLink' => [

                "type" => Type::string(),
                'args'        => [
                    "amount"    => [
                        "type"        => Type::nonNull(Type::Int()),
                    ],
                    "payer" => [
                        "type" => Type::string(),
                    ],
                    "invoice" => [
                        "type" => Type::boolean(),
                        "defaultValue" => false
                    ]
                ],

                'resolve' => function ($root, $args) {


                    if (Auth::isDemo()) {
                        RequestUtils::response(['demo' => "В демо-режиме вы не можете сделать пополнение баланса."], 422);
                    }

                    Auth::accessManager(true);

                    $curentManaget = Auth::getCurrentAuthenticatedManager();


                    if (!$args['amount']) {
                        RequestUtils::response(['amount' => "Сумма пополнения должна быть больше 0."], 422);
                    }


                    $params = array(
                        'sum'            => $args['amount'],
                        'payer'          => $args['payer'] ?? "",
                        'callback'       => 'https://' . $_SERVER['HTTP_HOST'] . '/manager/payment/callback',
                        'fields[user]'   => (string)$curentManaget->_id,
                        'fields[key]'   => (string) $curentManaget->collection,
                        'service' => "qiwi",
                        'fields[amount]' => (string) $args['amount'],
                        'product'        => "Пополнение счета по договору оферты сервиса " . Config::$title . ", кабинета  №" . $curentManaget->_id . " (Без НДС)"

                    );


                    if ($args['invoice'] != true) {

                        if ($args['amount'] > 15000) {
                            RequestUtils::response(['amount' => "Доступна оплата не более 15000 рублей за один платеж."], 422);
                        }
                    }

                    $ch = curl_init($args['invoice'] ?  'https://payment.apptor.tech/invoice' : 'https://payment.apptor.tech/form');
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    $html = curl_exec($ch);
                    curl_close($ch);

                    $result = json_decode($html, true);

                    return $result['data']['link_pay'] ?? null;
                }

            ],

            'dataToExport' => [


                "type" => Type::string(),
                'args'        => [
                    "data"    => [
                        "type"        => Type::nonNull(Type::listOf(Type::listOf(Type::string())))
                    ],
                    "format" => [
                        "type" => Type::nonNull(Types::ExportFormatType()),
                        "description" => "Формат экспорта"
                    ]
                ],

                'resolve' => function ($root, $args) {


                    if (Auth::isDemo()) {
                        RequestUtils::response(['demo' => "Экспорт данных в демо режиме не доступен."], 422);
                    }

                    Auth::accessManager(true);

                    $data = $args['data'];

                    if ($args['format'] == 'xlsx') {



                        $col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

                        $spreadsheet = new Spreadsheet();

                        $sheet = $spreadsheet->getActiveSheet();
                        $sheet->getDefaultColumnDimension()->setWidth(24);

                        foreach ($data as $index => $vals) {
                            foreach ($vals as $indexVal => $val) {
                                $sheet->setCellValue($col[$indexVal] . ($index + 1), $val);
                            }
                        }

                        $writer = new Xlsx($spreadsheet);
                        $filename = md5(json_encode($data) . time()) . '.xlsx';
                        $writer->save(base_path() . '/storage/files/xlsx/' . $filename);



                        $url = Utils::saveToS3(base_path() . '/storage/files/xlsx/' . $filename, $filename, "exports-xlsx");
                        unlink(base_path() . '/storage/files/xlsx/' . $filename);
                        return $url;
                    }

                    if ($args['format'] == 'csv') {



                        $filename = md5(json_encode($data) . time()) . '.csv';

                        $fp = fopen(base_path() . '/storage/csv/' . $filename, 'w');

                        foreach ($data as $fields) {
                            fputcsv($fp, $fields);
                        }

                        fclose($fp);



                        $url = Utils::saveToS3(base_path() . '/storage/csv/' . $filename, $filename, "exports-csv");
                        unlink(base_path() . '/storage/csv/' . $filename);
                        return $url;
                    }
                }

            ],

            'badges' => [


                'type'        =>  Type::listOf(Types::BadgeType()),

                'resolve'     => function ($root, $args) {


                    if (Auth::isDemo()) {
                        return [];
                    }


                    Auth::accessManager(true);





                    $result = [];

                    foreach (Objects::$objects as $object) {
                        if ($object->badgeFunction && is_callable($object->badgeFunction)) {

                            $result[] = ['count' => $object->badgeFunction(), "key" => $object->key];
                        }
                    }

                    return $result;
                }
            ],

            'structure' => [

                'type'        =>  Types::StructureType(),

                'resolve'     => function ($root, $args) {
                    return Config::getJsonSchema();
                },


            ],
            'columns' => [
                "type" => Type::listOf(Types::ColumnType()),
                'args'        => [
                    "key"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Ключ объекта',
                    ]
                ],

                'resolve' => function ($root, $args) {

                    $object = Objects::getObject($args['key']);

                    $result = [];

                    $intable = $object->getInTable();


                    if ($intable) {


                        //Сначала идут указаные поля
                        foreach ($intable  as $key) {

                            if (isset($object->fields[$key])) {

                                $field = $object->fields[$key];

                                $key = implode('-', explode('.', $field->key));
                                $result[] = [
                                    'title' => $field->title,
                                    'dataIndex' => $field->key,
                                    'visible' =>  true,
                                    'key' =>  $key,
                                    "width" => $field->width,
                                    'className' => "column_key_" . $key,
                                ];
                            }
                        }
                    }

                    /*    foreach ($object->fields as $field) {


                        if (!in_array($field->key, $object->intable)) {
                            $key = implode('-', explode('.', $field->key));
                            $result[] = [
                                'title' => $field->title,
                                'dataIndex' => $field->key,
                                'visible' =>  false,
                                'key' =>  $key,
                                'className' => "column_key_" . $key,
                            ];
                        }
                    }*/



                    return $result;
                }


            ],



            'theme' => [
                "type" => Types::ThemeType(),
                'resolve' => function ($root, $args) {


                    return [
                        'title'    => Config::$title,
                        "subtitle" => Config::$subtitle,
                        "color"    => Config::$color,
                        "terms"    => false,
                        "cover"    => Config::$cover,
                        "logo"     => Config::$logo,
                        'balance' => Config::$balance,
                        "emailReg" => Config::$emailReg,
                        "socReg"   => Config::$socReg,
                        "emailAuth" => Config::$emailAuth,
                        "socAuth"   => Config::$socAuth,
                    ];
                }
            ],

            'profile' => [

                "type" => Types::ProfileType(),
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        return [

                            "_id" => "demo_user",
                            "photo" => Config::$logo,
                            "name" => "Demo",
                            "surname" => "User",
                            "email" => "demo@example.com",
                            "social" => [],
                            'apikey' => "DEMOKEY",
                            "balance" => 7000

                        ];
                    }

                    Auth::accessManager(true);
                    $currentManager = Auth::getCurrentAuthenticatedManager();

                    if (isset(Auth::$additionalManagers[$currentManager->key])) {

                        $currentManager['email'] =  $currentManager[Auth::$additionalManagers[$currentManager->key]['login']];
                    }
                    return $currentManager;
                }
            ],


            'payments' => [
                "type" => Type::listOf(Types::PaymentType()),
                "description" => "Оплаты",
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        return [];
                    }

                    Auth::accessManager(true);

                    $currentManager = Auth::getCurrentAuthenticatedManager();


                    $paymentCollection = $currentManager['collection'] . "_payments";

                    if (Auth::isDemo()) {
                        return  [];
                    } else {

                        return mDB::collection($paymentCollection)->find([
                            $currentManager['key'] => $currentManager->_id
                        ], [
                            'sort'  => [
                                "_id" => -1,
                            ],
                            'limit' => 100

                        ])->toArray();
                    }
                }
            ],


            'paymentsExport' => [
                "type" => Type::string(),
                "description" => "Экспорт оплат",
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        RequestUtils::response(['demo' => "Экспорт данных в демо режиме не доступен."], 422);
                    }



                    Auth::accessManager(true);

                    $currentManager = Auth::getCurrentAuthenticatedManager();


                    $paymentCollection = $currentManager['collection'] . "_payments";

                    $result =  mDB::collection($paymentCollection)->find([
                        $currentManager['key'] => $currentManager->_id
                    ], [
                        '$sort'  => [
                            "_id" => -1,
                        ],
                    ])->toArray();

                    $exportData   = [];
                    $exportData[] = ['Дата', "Тип транзакции", "Назначение", "Сумма"];
                    foreach ($result as $item) {
                        $exportData[] = [
                            gmdate("d.m.Y", $item->created_at),
                            $item->amount > 0 ? "Поступление" : "Списание",
                            $item->description ?? "",
                            $item->amount,
                        ];
                    }

                    $col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

                    $spreadsheet = new Spreadsheet();

                    $sheet = $spreadsheet->getActiveSheet();
                    $sheet->getDefaultColumnDimension()->setWidth(24);

                    foreach ($exportData as $index => $vals) {
                        foreach ($vals as $indexVal => $val) {
                            $sheet->setCellValue($col[$indexVal] . ($index + 1), $val);
                        }
                    }

                    $writer = new Xlsx($spreadsheet);
                    $filename = md5(json_encode($exportData) . time()) . '.xlsx';
                    $writer->save(base_path() . '/storage/files/xlsx/' . $filename);



                    $url = Utils::saveToS3(base_path() . '/storage/files/xlsx/' . $filename, $filename, "exports-xlsx");
                    unlink(base_path() . '/storage/files/xlsx/' . $filename);
                    return $url;
                }
            ],



            'export' => [
                "type" => Type::string(),
                'args'        => [
                    "key"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Ключ объекта',
                    ],
                    "format" => [
                        "type" => Type::nonNull(Types::ExportFormatType()),
                        "description" => "Формат экспорта"
                    ],
                    'sort' => [
                        'type' => Types::SortType(),
                    ],
                    'filter' => [
                        "type" => Type::string(),
                    ],
                    'search' => [
                        "type" => Type::string()
                    ],
                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        RequestUtils::response(['demo' => "Экспорт данных в демо режиме не доступен."], 422);
                    }


                    Auth::accessManager(true);


                    $object = Objects::getObject($args['key']);

                    $sort = [];

                    if (isset($args['sort'])) {
                        $sort = [
                            $args['sort']['field'] => $args['sort']['order'] ==  "ascend" ? 1 : -1
                        ];
                    }

                    $filter = [];

                    if (isset($args['filter'])) {
                        $filter = json_decode($args['filter'], true);
                    }

                    $result = $object->export([
                        "format" => $args['format'],
                        "sort"    => $sort,
                        "filter"  => $filter,
                        "search"  => $args['search'] ?? null,
                        "reverse" => true,
                    ]);

                    return  $result;
                }


            ],



            'table' => [
                "type" => Types::TableDataType(),
                'args'        => [
                    "key"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Ключ объекта',
                    ],
                    'limit' => [
                        'type' => Type::int(),
                        "defaultValue" => 20
                    ],
                    'skip' => [
                        'type' => Type::int(),
                        "defaultValue" => 0
                    ],
                    'sort' => [
                        'type' => Types::SortType(),
                    ],
                    'filter' => [
                        "type" => Type::string(),
                    ],
                    'search' => [
                        "type" => Type::string()
                    ],

                ],

                'resolve' => function ($root, $args) {

                    if (!Auth::isDemo()) {
                        Auth::accessManager(true);
                    }

                    $object = Objects::getObject($args['key']);

                    $sort = [];

                    if (isset($args['sort'])) {
                        $sort = [
                            $args['sort']['field'] => $args['sort']['order'] ==  "ascend" ? 1 : -1
                        ];
                    }

                    $filter = [];

                    if (isset($args['filter'])) {
                        $filter = json_decode($args['filter'], true);
                    }




                    if (Auth::isDemo()) {
                        $result =  $object->getDemo(10);
                    } else {

                        $result =  $object->find([
                            "limit"   => $args['limit'],
                            "skip"    => $args['skip'],
                            "sort"    => $sort,
                            "filter"  => $filter,
                            "search"  => $args['search'] ?? null,
                            "reverse" => true,
                            "fields" => $object->getInTable(),
                            "deep" => 2,
                        ]);
                    }





                    $tableData = [];
                    $row = 0;
                    foreach ($result as $key => $values) {

                        $index = 0;

                        $values = (array) $values;

                        $tableData[$row]['values']["_id"] = [
                            "key" => "_id",
                            "value" => $values["_id"],
                            "input" => null,
                        ];

                        $index = 1;

                        foreach ($object->fields as $field) {






                            $tableData[$row]['values'][$field->key] = [
                                "key" => $field->key,
                                "tpl" => $field->getTpl($values, $field->key),
                                "value" => $field->getExportValue($values),
                                "input" => $field->input,
                            ];
                        }


                        $tableData[$row]['view'] =   $object->bladeTpl ? (string) view($object->bladeTpl, (array)$values) : null;

                        $index++;
                        $row++;
                    }




                    return [
                        'data' => $tableData,
                        "limit"  => $args['limit'],
                        "skip"   => $args['skip'],
                    ];
                }


            ],


            'expandableFilter' => [
                "type" => Type::string(),
                'args'        => [
                    "_id" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "key" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "from"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],

                    "localField"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],

                    "foreignField"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],


                ],

                'resolve' => function ($root, $args) {


                    if (Auth::isDemo()) {
                        return json_encode(["demo" => "mode"]);
                    }

                    Auth::accessManager(true);

                    $parentObject = Objects::getObject($args['key']);





                    $parentItem =  mDB::collection($parentObject->collection)->findOne([
                        "_id" => mDB::id($args['_id'])
                    ]);




                    $localFieldValue =  isset($parentItem[$args['localField']]) ?  $parentItem[$args['localField']] : null;

                    if (!$localFieldValue) {

                        return null;
                    }



                    $object = Objects::getObject($args['from']);



                    if (get_class($localFieldValue) == 'MongoDB\BSON\ObjectId') {
                        $filter = [
                            $args['foreignField'] => (string) $localFieldValue
                        ];
                    } else if ((get_class($localFieldValue) == "MongoDB\Model\BSONArray") || isset($localFieldValue[0])) {

                        $ids = [];
                        foreach ($localFieldValue as $val) $ids[] = (string) $val;
                        $filter = [
                            $args['foreignField'] => ['$in' => $ids]
                        ];
                    } else {
                        $filter = [
                            $args['foreignField'] => $localFieldValue
                        ];
                    }


                    return json_encode($filter);
                }


            ],



            'stat' => [

                "type" => Types::StatResultType(),
                'args'        => [
                    "object" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "key"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Ключ объекта',
                    ],
                    'block' => [
                        "type"        => Type::nonNull(Type::string()),
                    ],

                    'filter' => [
                        "type" => Type::string(),
                    ],


                ],

                'resolve' => function ($root, $args) {

                    $object =  Objects::getObject($args['object']);

                    $stat = $object->getStat($args['key']);

                    $block = $stat->getBlock($args['block']);

                    if (Auth::isDemo()) {
                        $data = $block->getDemo();

                        if ($block->type == 'table') {
                            return ['table' => json_encode($data)];
                        } else {
                            return ['block' => $data];
                        }
                    }

                    $filter =  null;
                    if (isset($args['filter'])) {
                        $filter = RequestUtils::prepareFilterIds(json_decode($args['filter'], true));
                    }

                    $hash = md5(Auth::$manager_id . Auth::getCurrentAuthenticatedManager()->_id . $args['object'] . $args['key'] . $args['block'] . ($args['filter'] ?? "") . json_encode(Config::$globalSplitParams ?? []));

                    $hashData = mDB::collection("cacheStat")->findOne([
                        "hash" => $hash,
                        "created_at" => ['$gt' => time() - (60 * 2)]
                    ]);

                //    if (isset($hashData->data)) {
                //        $data = $hashData->data;
                  //  } else {
                        $data = $block->calcData($filter);

                        mDB::collection("cacheStat")->insertOne([
                            "hash" => $hash,
                            "object" => $args['object'],
                            "key" => $args['key'],
                            "block" => $args['block'],
                            'filter' => $filter,
                            'data' => $data
                        ]);
                 //   }

                    if ($block->type == 'table') {
                        return [
                            'table' => json_encode($data)
                        ];
                    } else {
                        return [
                            'block' => $data
                        ];
                    }
                }
            ],


            'total' => [

                "type" => Type::int(),
                'args'        => [
                    "key"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Ключ объекта',
                    ],

                    'filter' => [
                        "type" => Type::string(),
                    ],
                    'search' => [
                        "type" => Type::string()
                    ],


                ],

                'resolve' => function ($root, $args) {



                    if (Auth::isDemo()) {
                        return rand(30,100);
                    }

                    Auth::accessManager(true);


                    $object = Objects::getObject($args['key']);


                    $filter = [];

                    if (isset($args['filter'])) {
                        $filter = json_decode($args['filter'], true);
                    }


                    return $object->total([
                        "filter"  => $filter,
                        "search"  => $args['search'] ?? null,
                    ]);
                }



            ],


            'data' => [
                "type" => Type::string(),

                'args'        => [
                    "_id" => [
                        "type"        => Type::string(),

                    ],
                    "key"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Ключ объекта',
                    ]
                ],

                'resolve' => function ($root, $args) {

                    if (!Auth::isDemo()) {
                        Auth::accessManager(true);
                    }



                    $object = Objects::getObject($args['key']);

                    if (isset($args['_id'])) {


                        if (Auth::isDemo()) {
                            $result =  $object->getDemo(1)[0];
                        } else {


                            if ($object->single) {


                                $result = $object->find([
                                    "manager" => Auth::$manager_id
                                ])[0] ?? [
                                    "none" => "none"
                                ];
                            } else {
                                $result = $object->find([
                                    "_id" => mDB::id($args['_id']),
                                ])[0] ?? null;
                            }
                        }
                        if ($result)
                            foreach ($object->getFieldsKey() as $fieldKey) {

                                $result[$fieldKey] =  Utils::valueFromPath($result, $fieldKey);
                            }



                        return json_encode($result);
                    } else {

                        $result = $object->getFieldsValues();

                        return json_encode($result);
                    }
                }

            ],



            'select' => [

                "type" => Types::ResultSelectType(),
                'args'        => [
                    "object"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "display" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    'filter' => [
                        "type" => Type::string(),
                    ],
                    'limit' => [
                        'type' => Type::int(),
                        "defaultValue" => 9999
                    ],
                    'skip' => [
                        'type' => Type::int(),
                        "defaultValue" => 0
                    ],
                    'search' => [
                        "type" => Type::string()
                    ],

                ],

                'resolve' => function ($root, $args) {


                    if (!Auth::isDemo()) {
                        Auth::accessManager(true);
                    }


                    $subject  = Objects::getObject($args['object']);



                    $coverField = null;
                    foreach ($subject->fields as $key => $fieldSubject) {
                        if ($fieldSubject->input == "image" || $fieldSubject->input == "imagelink") {
                            $coverField = $fieldSubject->key;
                            break;
                        }
                    }


                    $findFields = array_diff(explode(" ", $args['display']), ['']);
                    if ($coverField)  $findFields[] = $coverField;
                    $findFields[] = 'search_string';



                    $filter = [];

                    if (isset($args['filter'])) {
                        $filter = json_decode($args['filter'], true);
                    }


                    if (Auth::isDemo()) {
                        $result =  $subject->getDemo(10);
                    } else {

                        $result =  $subject->find([
                            "search" => $args['search'] ?? null,
                            "limit"   => $args['limit'],
                            'filter' => $filter,
                            "skip"    =>  $args['skip'],
                            "fields" =>  $findFields,
                            "deep" => 1
                        ]);
                    }


                    return [
                        "data" => json_encode($result),
                        "limit"  => $args['limit'],
                        "coverField" => $coverField,
                        "skip"   => $args['skip'],
                    ];
                }
            ],

            'objects' =>  [
                'type'        =>   Type::listOf(Types::ObjectItemType()),
                'args'        => [
                    "key"    => [
                        "type"        => Type::string(),
                        "description" => 'Получить одну объект по key',
                    ]
                ],
                'resolve'     => function ($root, $args) {





                    if (isset($args['key']))
                        return [Objects::getObject($args['key'])->getJson()];
                    else
                        return Objects::getJsonObjects();
                },
            ],

            'position' => [
                "type" => Types::PositionType(),
                'args'        => [
                    "key"    => [
                        "type"        => Type::string(),
                        "description" => 'Получить одну объект по key',
                    ]
                ],
                'resolve'     => function ($root, $args) {

                    return Utils::getIpPosition();
                }
            ],


        ];


        $config = [
            'name'   => "Query",
            'fields' => $fields
        ];

        parent::__construct($config);
    }
}