<?php

namespace DevSpark\GraphQL\Manager;

use BaraBaaS\Chat;
use BaraBaaS\mDB;
use DevSpark\Engine\Core;
use DevSpark\Utils\Response;


use DevSpark\Engine\Auth;
use DevSpark\Engine\Config;
use DevSpark\Engine\Utils;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MutationType extends ObjectType
{
    public function __construct()
    {



        $fields = [


            'message' => [
                "type" => Type::boolean(),
                'args'        => [
                    "chat"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "text" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }


                    Auth::accessManager(true);

                    $curentManager = Auth::getCurrentAuthenticatedManager();

                    Chat::send($args['chat'], $args['text'], $curentManager->_id);
                    return true;
                }

            ],

            'closeChat' => [
                "type" => Type::boolean(),
                'args'        => [
                    "chat"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);


                    Chat::supportClose($args['chat']);
                    return true;
                }
            ],

            'deleteMessage' => [
                "type" => Type::boolean(),
                'args'        => [
                    "message"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);


                    Chat::deleteMessage($args['message']);
                    return true;
                }
            ],

            'createChat' => [
                "type" => Type::boolean(),
                'args'        => [
                    "members"    => [
                        "type"        => Type::nonNull(Type::listOf(Type::string())),
                    ],
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);

                    $curentManager = Auth::getCurrentAuthenticatedManager();

                    Chat::create([...$args['members'], $curentManager->_id]);
                    return true;
                }

            ],


            'buttonAction' => [
                "type" => Type::boolean(),
                "description" => "Быстрое действие",
                'args'        => [
                    "object"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "ids" => [
                        "type"        => Type::nonNull(Type::listOf(Type::string())),
                    ],
                    "fields" => [
                        "type" => Type::string()
                    ],
                    "action" => [
                        "type"        => Type::nonNull(Type::string()),
                    ]
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);




                    $fields = [];
                    if (isset($args['fields'])) {
                        $fields = json_decode($args['fields'], true);
                    }

                    if (isset(Objects::getObject($args['object'])->buttons[$args['action']]->action)) {


                        $ids = [];

                        foreach ($args['ids'] as $id) {
                            $ids[] = mDB::id($id);
                        }

                        Objects::getObject($args['object'])->buttons[$args['action']]->action($ids, $fields);
                        return true;
                    } else {

                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "Функция не доступна для вашего аккаунта"
                        ]);
                    }
                }

            ],

            'changePassword' => [

                "type" => Type::boolean(),
                'args'        => [
                    "password"    => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Текущий пароль',
                    ],
                    "newpassword" => [
                        "type"        => Type::nonNull(Type::string()),
                        "description" => 'Новый пароль',
                    ]
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);

                    if (Auth::$manager->password != Auth::getPassword($args['password'])) {
                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "Вы ввели неверный текущий пароль"
                        ]);
                    }

                    mDB::collection("managers")->updateOne([
                        "_id" => Auth::$manager->_id,
                    ], [
                        '$set' => [
                            "password" => Auth::getPassword($args['newpassword']),
                        ],
                    ]);

                    return true;
                }

            ],


            'updateProfile' => [

                "type" => Type::boolean(),
                "description" => "Обновить профиль",
                'args'        => [
                    "photo"    => [
                        "type"        => Type::string(),
                    ],
                    "name" => [
                        "type"        => Type::string(),
                    ],
                    "surname" => [
                        "type"        => Type::string(),
                    ]
                ],
                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);

                    $set = [];
                    if (isset($args['photo'])) {
                        $set['photo'] = $args['photo'];
                    }

                    if (isset($args['name'])) {
                        $set['name'] = $args['name'];
                    }

                    if (isset($args['surname'])) {
                        $set['surname'] = $args['surname'];
                    }

                    if (count($set) > 0) {


                        $currentManager = Auth::getCurrentAuthenticatedManager();
                        mDB::collection($currentManager->collection)->updateOne([
                            "_id" => $currentManager->_id
                        ], [
                            '$set' =>  $set
                        ]);
                    }

                    return true;
                }

            ],


            'recoveryPassword' => [

                "type" => Type::boolean(),
                'args'        => [
                    "code"    => [
                        "type"        => Type::string(),
                    ],
                    "password" => [
                        "type"        => Type::string(),
                        "description" => 'Новый пароль',
                    ],
                    "email" => [
                        "type"        => Type::nonNull(Type::string()),
                    ]
                ],

                'resolve' => function ($root, $args) {


                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }



                    if (Utils::has($args, ['password', 'code'])) {
                        $manager = mDB::collection("managers")->findOne([
                            "email"      => $args['email'],
                            "email_code" => (int) $args['code'],
                        ]);

                        if (!$manager) {

                            throw new \GraphQL\Error\Error(extensions: [
                                "type" => "VALID",
                                "message" => "Вы указали неверный код."
                            ]);
                        }

                        mDB::collection("managers")->updateOne(
                            ["_id" => $manager->_id],
                            [
                                '$set' => [
                                    'email_code' => '', "password" => Auth::getPassword($args['password']),
                                ],
                            ]
                        );

                        return true;
                    } else {

                        $manager = mDB::collection("managers")->findOne([
                            "email" => $args['email'],
                        ]);

                        if (!$manager) {

                            throw new \GraphQL\Error\Error(extensions: [
                                "type" => "VALID",
                                "message" => "На почту нет зарегистрированного аккаунта."
                            ]);
                        }

                        $email_code = random_int(111111, 999999);

                        mDB::collection("managers")->updateOne([
                            "_id" => $manager->_id,
                        ], [
                            '$set' => ['email_code' => $email_code],
                        ]);

                    Utils::send($args['email'],  "Восстановление пароля в " . Config::$title, "Ваш код - " . $email_code);


                        return true;
                    }
                }

            ],


            "registration" => [
                "type" => Type::boolean(),
                'args'        => [

                    "email" => [
                        "type"        => Type::nonNull(Type::string()),
                    ]
                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    if (!Config::$emailReg) {

                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "Регистрация не доступна"
                        ]);
                    }

                    $args['email'] = trim($args['email']);


                    $manager = mDB::collection("managers")->findOne(["email" => $args['email']]);

                    if ($manager) {

                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "На указанный email уже создан аккаунт."
                        ]);
                    }

                    $password = Utils::randomPassword();

                    mDB::collection("managers")->insertOne([
                        "password"   => Auth::getPassword($password),
                        "email"      =>  $args['email']
                    ]);

                    Utils::send($args['email'],  "Регистрация в " . Config::$title, "Ваш пароль - " . $password);

                    return true;
                }

            ],

            'authorization' => [
                "type" => Type::string(),
                'args'        => [

                    "email" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "password" => [
                        "type"        => Type::nonNull(Type::string()),
                    ]
                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        return "demotoken";
                    }

                    if (!Config::$emailAuth) {

                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "Авторизация недоступна."
                        ]);
                    }

                    $args['email'] = trim($args['email']);
                    $args['password'] = trim($args['password']);




                    $manager = mDB::collection("managers")->findOne(["password" => Auth::getPassword($args['password']), "email" => $args['email']]);


                    /* "name" => [
                        "collection" => "collection in mongodb",
                        "login" => "login field",
                        "password" => "password field",
                    ],*/

                    if (!$manager) {
                        foreach (Auth::$additionalManagers as $key => $additionalManager) {




                            $manager = mDB::collection($additionalManager['collection'])->findOne(
                                [
                                    $additionalManager['password'] => ['$in' => [Auth::getPassword($args['password']), $args['password']]],
                                    $additionalManager['login'] => $args['email']
                                ]
                            );
                            if ($manager) break;
                        }
                    }

                    if (!$manager) {

                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "Неверная пара логин и пароль."
                        ]);
                    } else {
                        return  Auth::getManagerToken($manager->_id);
                    }
                }

            ],


            'soc' => [

                "type" => Type::string(),
                'args'        => [

                    "key" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],

                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        return "demotoken";
                    }

                    $arrContextOptions = array(
                        "ssl" => array(
                            "verify_peer"      => false,
                            "verify_peer_name" => false,
                        ),
                    );

                    $dataAuth = file_get_contents('https://auth4app.com/hash?key=' . $args['key'], false, stream_context_create($arrContextOptions));

                    $data = json_decode($dataAuth, true);

                    if ($data['type'] == 'error') {

                        throw new \GraphQL\Error\Error(extensions: [
                            "type" => "VALID",
                            "message" => "Ошибка авторизации."
                        ]);
                    }

                    $userSoc = $data['data'];

                    $manager = mDB::collection("managers")->findOne([
                        "social.id" => $userSoc['id'],
                    ]);

                    if ($manager) {

                        if (!Config::$socAuth) {

                            throw new \GraphQL\Error\Error(extensions: [
                                "type" => "VALID",
                                "message" => "Авторизация недоступна."
                            ]);
                        }

                        return  Auth::getManagerToken($manager->_id);
                    } else {

                        if (!Config::$socReg) {


                            throw new \GraphQL\Error\Error(extensions: [
                                "type" => "VALID",
                                "message" => "Регистрация не доступна."
                            ]);
                        }

                        $manager = mDB::collection("managers")->insertOne([
                            'name'    => isset($userSoc['name']) ? $userSoc['name'] : null,
                            'surname' => isset($userSoc['surname']) ? $userSoc['surname'] : null,
                            'email'   => isset($userSoc['mail']) ? $userSoc['mail'] : null,
                            "photo"   => isset($userSoc['photo']) ? $userSoc['photo'] : null,
                            "social"  => [$userSoc],
                        ]);

                        return  Auth::getManagerToken($manager->getInsertedId());
                    }
                }


            ],



            'set' => [

                "type" => Type::string(),
                'args'        => [

                    "_id" => [
                        "type"        => Type::string(),
                    ],
                    "key" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "values" => [
                        "type"        => Type::nonNull(Type::string()),
                    ]
                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);


                    $values = json_decode($args['values'], true);

                    $object = Objects::getObject($args['key']);



                    if ($object->single) {

                        $before = mDB::_collection($object->collection)->findOne([
                            "manager" => Auth::$manager_id
                        ]);

                        if (!$before) {
                            $result =  $object->insert($values);
                        } else {
                            $result =  $object->update($values, $before->_id);
                        }
                    } else if (!empty($args['_id'])) {
                        $result =  $object->update($values, $args['_id']);
                    } else {
                        $result =  $object->insert($values);
                    }



                    return json_encode($result);
                }
            ],


            'groupSet' => [

                "type" => Type::boolean(),
                'args'        => [
                    "object"    => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "ids" => [
                        "type"        => Type::nonNull(Type::listOf(Type::string())),
                    ],
                    "values" => [
                        "type"        => Type::nonNull(Type::string()),
                    ]
                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);


                    $value = json_decode($args['values'], true);

                    $key = array_keys($value)[0];
                    $value = array_values($value)[0];


                    Objects::getObject($args['object'])->setValueForMany($key, $value, $args['ids']);

                    return true;
                }
            ],

            'delete' => [
                "type" => Type::boolean(),
                'args'        => [

                    "_id" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "key" => [
                        "type"        => Type::nonNull(Type::string()),
                    ],
                    "reason" => [
                        "type" => Type::nonNull(Type::string())
                    ]

                ],

                'resolve' => function ($root, $args) {

                    if (Auth::isDemo()) {
                        Response::response(['demo' => "В демо режиме не доступно."], 422);
                    }

                    Auth::accessManager(true);



                    Objects::getObject($args['key'])->delete($args['_id'], $args['reason']);


                    return true;
                }
            ]

        ];
        /*  foreach (Objects::$objects as $object) {
            $fields[$object->key] = $object->getGraphQLMutation();
        }*/


        $config = [
            'fields' =>  $fields
        ];
        parent::__construct($config);
    }
}