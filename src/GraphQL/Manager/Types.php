<?php

namespace DevSpark\GraphQL\Manager;

use DevSpark\Engine\Auth;
use BaraBaaS\mDB;
use BaraBaaS\Schema\FieldTypes\DefaultField;
use BaraBaaS\Schema\Icons;
use DevSpark\Engine\Core;
use Exception;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;

class Types
{
    private static $query;
    private static $mutation;

    static $objects;
    static $enums;


    public static function ResultDateType()
    {


        return  isset(self::$objects['ResultDateType']) ? self::$objects['ResultDateType'] : (self::$objects['ResultDateType']  = new ObjectType([
            'description' => 'Результат запроса',
            'name'        => "ResultDateType",
            'fields'      => function () {

                return [
                    'data'   => Type::string(),
                    'limit'  => Type::int(),
                    'offset' => Type::int(),
                    'total'  => Type::int(),
                ];
            },
        ]));
    }


    public static function ChatMemberType()
    {


        return  isset(self::$objects['ChatMemberType']) ? self::$objects['ChatMemberType'] : (self::$objects['ChatMemberType']  = new ObjectType([
            'description' => 'Участник чата',
            'name'        => "ChatMemberType",
            'fields'      => function () {

                return [
                    '_id'   => Type::string(),
                    'name' => Type::string(),
                    'surname' => Type::string(),
                    'phone' => Type::string(),
                    'email' => Type::string(),
                    "photo" => Type::string(),
                    "type" => Type::string(),
                    'title' =>  [
                        'type' => Type::string(),
                        'resolve'     => function ($root, $args) {
                            $tmp = [];

                            $tmp[] = $root['title'] ?? null;

                            $tmp[] = $root['name'] ?? null;
                            $tmp[] = $root['surname'] ?? null;

                            $tmp = array_diff($tmp, ['', null]);

                            if (count($tmp) > 1)  return implode(" ", $tmp);

                            $tmp[] = $root['phone'] ?? null;
                            $tmp[] = $root['email'] ?? null;
                            $tmp = array_diff($tmp, ['', null]);


                            return implode(" ", $tmp);
                        },
                    ],
                ];
            },
        ]));
    }


    public static function ChatMessageType()
    {


        return  isset(self::$objects['ChatMessageType']) ? self::$objects['ChatMessageType'] : (self::$objects['ChatMessageType']  = new ObjectType([
            'description' => 'Сообщение',
            'name'        => "ChatMessageType",
            'fields'      => function () {

                return [
                    '_id'   => Type::string(),
                    'from' => Types::ChatMemberType(),
                    'text' => [
                        'type' => Type::string(),
                        'resolve'     => function ($root, $args) {
                            return strip_tags($root['text'] ?? "");
                        },
                    ],
                    'created_at' => Type::float(),
                    'updated_at'  => Type::int(),
                    'first' =>  [
                        'type' => Type::boolean(),
                        'resolve'     => function ($root, $args) {
                            return $root['first'] ?? false;
                        },
                    ],
                    'last' =>  [
                        'type' => Type::boolean(),
                        'resolve'     => function ($root, $args) {
                            return $root['last'] ?? false;
                        },
                    ],
                    'tail' =>  [
                        'type' => Type::boolean(),
                        'resolve'     => function ($root, $args) {
                            return $root['tail'] ?? false;
                        },
                    ],
                    'type' => [
                        "type" => Type::string(),
                    ]

                ];
            },
        ]));
    }



    public static function ChatType()
    {


        return  isset(self::$objects['ChatType']) ? self::$objects['ChatType'] : (self::$objects['ChatType']  = new ObjectType([
            'description' => 'Чат',
            'name'        => "ChatType",
            'fields'      => function () {

                return [
                    '_id'   => Type::string(),
                    'title' => [
                        "type" => Type::string(),

                    ],
                    'members'  => [
                        'type' => Type::listOf(Types::ChatMemberType()),
                        'resolve'     => function ($root, $args) {
                            return $root['membersData'];
                        },
                    ],
                    'created_at' => Type::float(),
                    'updated_at'  => Type::int(),
                    'unread' => [
                        "type" => Type::int(),
                        'resolve'     => function ($root, $args) {
                            $curentManaget = Auth::getCurrentAuthenticatedManager();
                            return $root['unread'][(string)$curentManaget->_id] ?? 0;
                        },
                    ],
                    'open' => Type::boolean(),
                    'lastMessage' => Types::ChatMessageType()
                ];
            },
        ]));
    }




    public static function StatResultType()
    {

        return  isset(self::$objects['StatResultType']) ? self::$objects['StatResultType'] : (self::$objects['StatResultType'] = new ObjectType([
            'name'        => 'StatResultType',
            'description' => 'Результат для блока аналитики',
            'fields'      => [
                "table" => Type::string(),
                "block" =>  Type::listOf(Types::StatBlockResultType()),
            ],


        ]));
    }

    public static function StatBlockResultType()
    {

        return  isset(self::$objects['StatBlockResultType']) ? self::$objects['StatBlockResultType'] : (self::$objects['StatBlockResultType'] = new ObjectType([
            'name'        => 'StatBlockResultType',
            'description' => 'Одна запись результат для блока аналитики',
            'fields'      => [
                "label" => Type::string(),
                "value" =>  Type::float()



            ],


        ]));
    }

    public static function RowItem()
    {

        $types = [];
        $resolveType = [];
        foreach (Objects::$objects as $object) {
            $types[] = $object->getGraphQLType();
            $resolveType[$object->getGraphQLTypeName()] =  $object->getGraphQLType();
        }

        return  isset(self::$objects['RowItem']) ? self::$objects['RowItem'] : (self::$objects['RowItem']  = new UnionType([
            'name' => 'RowItem',
            'types' => $types,
            'resolveType' => function ($root) use ($resolveType): ObjectType {

                return $resolveType[$root->type];
            },
        ]));
    }



    public static function SortType()
    {
        return  isset(self::$objects['SortType']) ? self::$objects['SortType'] : (self::$objects['SortType'] = new InputObjectType([
            'name'        => 'SortType',
            'description' => 'Сортировка',
            'fields'      => [
                "field" => Type::nonNull(Type::string()),
                "order" =>  Type::nonNull(Types::SortTableOrder())
            ],


        ]));
    }

    public static function SortTableOrder()
    {

        return  isset(self::$objects['SortTableOrder']) ? self::$objects['SortTableOrder'] : (self::$objects['SortTableOrder'] = new EnumType([
            'name'        => "SortTableOrder",
            'description' => "Направление сортировки",
            'values'      => [
                "ascend"  => ["value" => "ascend", "description" => "По возрастанию (ascending)"],
                "descend" => ["value" => "descend", "description" => "По убыванию (descending)"],
            ],
        ]));
    }

    public static function ExportFormatType()
    {
        return  isset(self::$objects['ExportFormatType']) ? self::$objects['ExportFormatType'] : (self::$objects['ExportFormatType'] = new EnumType([
            'name'        => "ExportFormatType",
            'description' => "Формат для экспорта",
            'values'      => ["csv", "xlsx"],
        ]));
    }


    public static function TableDataItemType()
    {


        return  isset(self::$objects['TableDataItemType']) ? self::$objects['TableDataItemType'] : (self::$objects['TableDataItemType'] = new ObjectType([
            'name'        => 'TableDataItemType',
            'description' => 'Строка данных для вывода',
            'fields'      => [
                'input' => Type::string(),
                "key" => Type::string(),
                "value" => Type::string(),
                'tpl' => Type::string(),
            ],


        ]));
    }


    public static function TableDataType()
    {


        return  isset(self::$objects['TableDataType']) ? self::$objects['TableDataType'] : (self::$objects['TableDataType'] = new ObjectType([
            'name'        => 'TableDataType',
            'description' => 'Данные для вывода в таблице',
            'fields'      => [

                "data" => Type::listOf(Types::TableDataRowType())
            ],


        ]));
    }


    public static function TableDataRowType()
    {


        return  isset(self::$objects['TableDataRowType']) ? self::$objects['TableDataRowType'] : (self::$objects['TableDataRowType'] = new ObjectType([
            'name'        => 'TableDataRowType',
            'description' => 'Данные для вывода в таблице',
            'fields'      => [
                "view" => Type::string(),
                "values" => Type::listOf(Types::TableDataItemType())
            ],


        ]));
    }


    public static function ColumnType()
    {


        return  isset(self::$objects['ColumnType']) ? self::$objects['ColumnType'] : (self::$objects['ColumnType'] = new ObjectType([
            'name'        => 'ColumnType',
            'description' => 'Колонка таблицы',
            'fields'      => [

                "title" =>  Type::string(),
                "dataIndex" => Type::string(),
                "key" => Type::string(),
                "visible" => Type::boolean(),
                "width" => Type::float(),
                "className" => Type::string(),
                "sortDirections" => [
                    "type" => Type::listOf(Type::string()),
                    'resolve'     => function ($root, $args) {
                        return ["descend", "ascend"];
                    },
                ]
            ],


        ]));
    }


    public static function SocialType()
    {
        return  isset(self::$objects['SocialType']) ? self::$objects['SocialType'] : (self::$objects['SocialType'] = new ObjectType([
            'name'        => 'SocialType',
            'description' => 'Соц сеть',
            'fields'      => [

                "soc" =>  Type::string(),


            ],


        ]));
    }




    public static function ResultSelectType()
    {
        return  isset(self::$objects['ResultSelectType']) ? self::$objects['ResultSelectType'] : (self::$objects['ResultSelectType']  = new ObjectType([
            'description' => 'Результат запроса select',
            'name'        => "ResultSelectType",
            'fields'      => function () {

                return [
                    'data'   => Type::string(),
                    'limit'  => Type::int(),
                    'offset' => Type::int(),
                    'coverField'  => Type::string()
                ];
            },
        ]));
    }


    public static function ProfileType()
    {
        return  isset(self::$objects['ProfileType']) ? self::$objects['ProfileType'] : (self::$objects['ProfileType'] = new ObjectType([
            'name'        => 'ProfileType',
            'description' => 'Профиль',
            'fields'      => [
                "_id" => Type::string(),

                'balance' => Type::float(),
                "photo" => Type::string(),
                "name" =>  Type::string(),
                "surname"    =>  Type::string(),
                "email"    => Type::string(),
                "social" => Type::listOf(Types::SocialType()),
                "apikey" => [
                    "type" => Type::string(),

                    'resolve' => function ($root, $args) {

                        if (Auth::$manager) {
                            if (isset($root['apikey'])) return $root['apikey'];


                            $apikey =  hash('sha1', (string) Auth::$manager->_id . mt_rand());
                            mDB::collection("managers")->updateOne([
                                "_id"        => Auth::$manager->_id,
                                "deleted_at" => ['$exists' => false],
                            ], [
                                '$set' => [
                                    'apikey' =>  $apikey
                                ],
                            ]);

                            return  $apikey;
                        }
                    }


                ]

            ],


        ]));
    }


    public static function ResultActionType()
    {
        return  isset(self::$objects['ResultActionType']) ? self::$objects['ResultActionType'] : (self::$objects['ResultActionType'] = new ObjectType([
            'name'        => 'ResultActionType',
            'description' => 'Результат быстрого действия',
            'fields'      => [


                'message' => Type::string(),
                'success' => Type::boolean()

            ],


        ]));
    }

    public static function PaymentType()
    {
        return  isset(self::$objects['PaymentType']) ? self::$objects['PaymentType'] : (self::$objects['PaymentType'] = new ObjectType([
            'name'        => 'PaymentType',
            'description' => 'Оплата',
            'fields'      => [

                "amount" => Type::float(),
                "description" => Type::string(),
                "created_at" => Type::float()


            ],


        ]));
    }


    public static function ThemeType()
    {
        return  isset(self::$objects['ThemeType']) ? self::$objects['ThemeType'] : (self::$objects['ThemeType'] = new ObjectType([
            'name'        => 'ThemeType',
            'description' => 'Настройка темы',
            'fields'      => [


                'title' => Type::string(),
                "subtitle" =>  Type::string(),
                "color"    =>  [
                    "type" => Type::string(),

                    'resolve' => function ($root, $args) {

                        if ($root['color'] == '#000') {
                            //Theme

                            $colors = [
                                "#D32F2F",
                                "#C2185B",
                                "#7B1FA2",
                                "#512DA8",
                                "#303F9F",
                                "#0097A7",
                                "#388E3C",
                                "#FFA000",
                                "#E64A19",
                                "#455A64",
                            ];

                            return $colors[rand(0, count($colors) - 1)];
                        } else {
                            return $root['color'];
                        }
                    }
                ],


                "terms"    => Type::string(),
                "cover"    => [
                    "type" => Type::string(),

                    'resolve' => function ($root, $args) {

                        if (!$root['cover']) {


                            return "https://images.pexels.com/photos/994605/pexels-photo-994605.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2";
                        } else {
                            return $root['cover'];
                        }
                    }
                ],




                "logo"     => Type::string(),
                'balance' => Type::boolean(),
                "emailReg" => Type::boolean(),
                "socReg"   => Type::boolean(),
                "emailAuth" => Type::boolean(),
                "socAuth"   => Type::boolean(),



            ],


        ]));
    }



    public static function SelectItemType()
    {
        return  isset(self::$objects['SelectItemType']) ? self::$objects['SelectItemType'] : (self::$objects['SelectItemType'] = new ObjectType([
            'name'        => 'SelectItemType',
            'description' => 'Доступны значение для поля select/mselect',
            'fields'      => [
                "key" => Type::string(),
                "value" => Type::string(),

            ],


        ]));
    }



    public static function AccessType()
    {
        return  isset(self::$objects['AccessType']) ? self::$objects['AccessType'] : (self::$objects['AccessType'] = new ObjectType([
            'name'        => 'AccessType',
            'description' => 'Доступны',
            'fields'      => [
                "update" => Type::boolean(),
                "add" => Type::boolean(),
                "delete" => Type::boolean(),

            ]

        ]));
    }



    public static function TipType()
    {


        return   isset(self::$objects['TipType']) ? self::$objects['TipType'] : (self::$objects['TipType'] = new ObjectType([
            'name'        => 'TipType',
            "description" => "Подсказка объекта",
            'fields'      => [
                "icon" => Type::String(),
                "description" => Type::String(),
            ]

        ]));
    }

    public static function ResizeImageType()
    {
        return   isset(self::$objects['ResizeImageType']) ? self::$objects['ResizeImageType'] : (self::$objects['ResizeImageType'] = new ObjectType([
            'name'        => 'ResizeImageType',
            "description" => "Настройки ресайза изображения",
            'fields'      => [
                "h" => Type::int(),
                "w" => Type::int(),
                "preview" => [
                    "description" => "Коэфицент изменения размера фотографии при выводе в панели администрирования, где 1 - это исходное значение",
                    "type" => Type::float(),
                    "defaultValue" => 1
                ],
            ]

        ]));
    }




    public static function TabType()
    {



        return   isset(self::$objects['TabType']) ? self::$objects['TabType'] : (self::$objects['TabType'] = new ObjectType([
            'name'        => 'TabType',
            "description" => "Таб вывода предварительного просмотра",
            'fields'      => [
                "title" => [
                    "type" => Type::string(),
                    "description" => "Название группы"
                ],
                "key" => Type::string(),
                "filter" => [
                    "type" => Type::string(),
                    "description" => "Фильтрация",
                    'resolve'     => function ($root, $args) {
                        if (isset($root['filter']) && $root['filter']) {
                            return json_encode($root['filter']);
                        } else {
                            return null;
                        }
                    },
                ]
            ]


        ]));
    }

    public static function ExpandableType()
    {
        return   isset(self::$objects['ExpandableType']) ? self::$objects['ExpandableType'] : (self::$objects['ExpandableType'] = new ObjectType([
            'name'        => 'ExpandableType',
            "description" => "Вывод дополнительных данных в предварительном просмотре",
            'fields'      => [
                "from" => Type::string(),
                "localField" => Type::string(),
                "foreignField" =>  Type::string(),
            ]

        ]));
    }

    public static function ActionType()
    {



        return   isset(self::$objects['ActionType']) ? self::$objects['ActionType'] : (self::$objects['ActionType'] = new ObjectType([
            'name'        => 'ActionType',
            "description" => "Быстрое действие",
            'fields'      => [
                "key" => [
                    "type" => Type::string(),
                    "description" => "Уникальный ключ команды"
                ],
                "title" => [
                    "type" => Type::string(),
                ],
                "name" => [
                    "type" => Type::string(),
                ],
                "fields" => [
                    "type" => Type::listOf(Types::FieldType()),
                    "description" => "Список полей"
                ]
            ]

        ]));
    }

    public static function BadgeType()
    {
        return   isset(self::$objects['BadgeType']) ? self::$objects['BadgeType'] : (self::$objects['BadgeType'] = new ObjectType([
            'name'        => 'BadgeType',
            'fields'      => [
                "count" => [
                    "type" => Type::float(),
                ],
                "key" => [
                    "type" => Type::string(),
                ],

            ]

        ]));
    }


    public static function PositionType()
    {
        return   isset(self::$objects['PositionType']) ? self::$objects['PositionType'] : (self::$objects['PositionType'] = new ObjectType([
            'name'        => 'PositionType',
            'fields'      => [
                "latitude" => [
                    "type" => Type::float(),
                ],
                "longitude" => [
                    "type" => Type::float(),
                ],

            ]

        ]));
    }

    public static function GroupType()
    {



        return   isset(self::$objects['GroupType']) ? self::$objects['GroupType'] : (self::$objects['GroupType'] = new ObjectType([
            'name'        => 'GroupType',
            "description" => "Группа  полей",
            'fields'      => [
                "key" => Type::string(),
                "title" => [
                    "type" => Type::string(),
                    "description" => "Название группы"
                ],
                "description" => [
                    "type" => Type::string(),
                ],
                "fields" => [
                    "type" => Type::listOf(Types::FieldType()),
                    "description" => "Список полей"
                ],
                "from" => Type::string(),
                "localField" => Type::string(),
                "foreignField" =>  Type::string(),
            ]

        ]));
    }


    public static function GroupObjectType()
    {



        return   isset(self::$objects['GroupObjectType']) ? self::$objects['GroupObjectType'] : (self::$objects['GroupObjectType'] = new ObjectType([
            'name'        => 'GroupObjectType',
            "description" => "Группа  объектов",
            'fields'      => [
                "title" => [
                    "type" => Type::string(),
                    "description" => "Название группы"
                ],
                "objects" => [
                    "type" => Type::listOf(Types::ObjectItemType()),
                    "description" => "Список объектов"
                ]
            ]

        ]));
    }

    public static function FieldType()
    {
        if (isset(self::$objects['FieldType'])) {
            return self::$objects['FieldType'];
        } else {
            return self::$objects['FieldType'] = new ObjectType([
                'description' => 'Поле объекта',
                'name'        => 'FieldType',
                'fields'      => function () {
                    return [
                        'key' => Type::string(),
                        'title' => Type::string(),
                        'before' => [
                            "type" => Type::string(),
                            "description" => "HTML вставка перед полем ввода"
                        ],
                        'after' => [
                            "type" => Type::string(),
                            "description" => "HTML вставка перед полем ввода"
                        ],
                        'placeholder' => [
                            "type" => Type::string(),
                            "description" => "Плейсхолдер для поля ввода"
                        ],
                        'required' => [
                            "type" => Type::boolean(),
                            "description" => "Обязательное поле"
                        ],
                        'description' => Type::string(),
                        'default' => Type::string(),
                        'onlyshow' => Type::boolean(),
                        'fieldDisplay' => [
                            "description" => "Фильтр отображения поля",
                            "type" => Type::string(),
                            'resolve'     => function ($root, $args) {
                                if (isset($root['fieldDisplay']) && $root['fieldDisplay']) {
                                    return json_encode($root['fieldDisplay']);
                                } else {
                                    return null;
                                }
                            },
                        ],
                        "style" => [
                            "type" => Type::string(),
                            "description" => "CSS к значению поля",
                            'resolve'     => function ($root, $args) {
                                if (isset($root['style']) && $root['style']) {
                                    return json_encode($root['style']);
                                } else {
                                    return null;
                                }
                            },
                        ],
                        'col' => [
                            "type" => Type::int(),
                            "description" => "Ширина поля",
                            "defaultValue" => 24
                        ],
                        "input" => [
                            "type" => Type::string(),
                            "description" => "Тип поля"
                        ],
                        "originalInput" => [
                            "type" => Type::string(),
                            "description" => "Оригинальный тип поля для обработик фильтра"
                        ],
                        'accept' => [
                            "type" => Type::string(),
                            "description" => "Доступные типы файлов для загрузки"
                        ],
                        "max" => [
                            "type" => Type::float(),
                            "description" => "Максимальное значение (включительно)"
                        ],
                        "min" => [
                            "type" => Type::float(),
                            "description" => "Минимальное значение (включительно)"
                        ],
                        'resize' => [
                            "type" => Types::ResizeImageType(),
                            "description" => "Ресайз изображения"
                        ],
                        'addTitle' => [
                            "type" => Type::string(),
                            "description" => "Название кнопки для добавления данных (для поля MField)",
                        ],
                        'vertical' => [
                            "type" => Type::boolean(),
                            "description" => "Вертикальное расположение полей (для поля MField)",
                        ],
                        'mfields' => [
                            "type" => Type::listOf(Types::FieldType()),
                            "description" => "Поля для типа поля mfields"
                        ],
                        "width" => [
                            "description" => "Ширина колонки",
                            "type" => Type::int()
                        ],
                        "items" => [
                            "type" => Type::listOf(Types::SelectItemType()),
                            "description" => "Возможные значения полей select, mselect",
                            'resolve'     => function ($root, $args) {
                                if (isset($root['items'])) {
                                    $result = [];
                                    foreach ($root['items'] as $key => $value) {

                                        $result[] = [
                                            'key' => $key,
                                            'value' => $value
                                        ];
                                    }


                                    return $result;
                                } else {
                                    return null;
                                }
                            },
                        ],
                        'display' => [
                            "type" => Type::string(),
                            "description" => "Ключи для вывода данных для типа поля pointer,relation"
                        ],
                        'object' => [
                            "type" => Type::string(),
                            "description" => "Связанный объект получения данных для поля pointer,relation"
                        ],
                        'speller' => [
                            "type" => Type::boolean(),
                            "description" => "Проверка правописания"
                        ],
                        'fastEdit' => [
                            "type" => Type::boolean(),
                            "description" => "Быстрое редактирование"
                        ]


                    ];
                },
            ]);
        }
    }



    public static function StatEnumType()
    {

        return  isset(self::$objects['StatEnumType']) ? self::$objects['StatEnumType'] : (self::$objects['StatEnumType'] = new EnumType([
            'name'        => "StatEnumType",
            'description' => "Виды блоков статистики",
            'values'      => [
                "dashboard", "line", "table"
            ],
        ]));
    }

    public static function StatEnumDisplayType()
    {

        return  isset(self::$objects['StatEnumDisplayType']) ? self::$objects['StatEnumDisplayType'] : (self::$objects['StatEnumDisplayType'] = new EnumType([
            'name'        => "StatEnumDisplayType",
            'description' => "Стандартный вариант отображения для типа line",
            'values'      => [
                "linear", "columns", "area", "pie", "table"
            ],
        ]));
    }


    public static function StatColumnItem()
    {



        return   isset(self::$objects['StatColumnItem']) ? self::$objects['StatColumnItem'] : (self::$objects['StatColumnItem'] = new ObjectType([
            'name'        => 'StatColumnItem',
            "description" => "Колонка статистики таблица",
            'fields'      => [
                "key" => Type::string(),
                "dataIndex" => Type::string(),
                "title" => Type::string()

            ]

        ]));
    }

    public static function StatBlockType()
    {

        return   isset(self::$objects['StatBlockType']) ? self::$objects['StatBlockType'] : (self::$objects['StatBlockType'] = new ObjectType([
            'name'        => 'StatBlockType',
            "description" => "Блок статистики",
            'fields'      => [

                "key" => Type::string(),
                "title" => Type::string(),
                "type" => Types::StatEnumType(),
                "description" => Type::string(),
                "columns" => Type::listOf(Types::StatColumnItem()),
                "col" => Type::int(),
                "display" => Types::StatEnumDisplayType()

            ]

        ]));
    }

    public static function StatItemType()
    {



        return   isset(self::$objects['StatItemType']) ? self::$objects['StatItemType'] : (self::$objects['StatItemType'] = new ObjectType([
            'name'        => 'StatItemType',
            "description" => "Блок статистики",
            'fields'      => [

                "tip"     => Types::TipType(),
                "title"   => Type::string(),
                "key"     => Type::string(),
                "filters"    => [
                    "description" => "Поля фильтрации",
                    "type" => Type::listOf(Types::FieldType())
                ],
                "blocks"  => Type::listOf(Types::StatBlockType())


            ]

        ]));
    }

    public static function StructureType()
    {

        return   isset(self::$objects['StructureType']) ? self::$objects['StructureType'] : (self::$objects['StructureType'] = new ObjectType([
            'name'        => 'StructureType',
            "description" => "Структура",
            'fields'      => [
                "support" => Type::boolean(),
                "groups" => [
                    "type" => Type::listOf(Types::GroupObjectType()),
                ],
                'profile' => Type::listOf(Types::FieldType()),
                'globalFilter' => Type::listOf(Types::FieldType()),
                'objects' => Type::listOf(Types::ObjectItemType()),
                //                'analytics'=
            ]

        ]));
    }



    public static function MessageType()
    {

        return   isset(self::$objects['MessageType']) ? self::$objects['MessageType'] : (self::$objects['MessageType'] = new ObjectType([
            'name'        => 'MessageType',
            "description" => "Сообщение",
            'fields'      => [
                "type" => [
                    "type" => Type::string(),
                    "description" => "Message type: sent (default) or received",
                    "defaultValue" => "sent"
                ],
                "text" => [
                    "type" => Type::string(),
                    "description" => "Message text",
                ],
                "avatar" => [
                    "type" => Type::string(),
                    "description" => "Message user's avatar URL",
                ],
                "name" => [
                    "type" => Type::string(),
                    "description" => "Message user's name",
                ],
                "image" => [
                    "type" => Type::string(),
                    "description" => "Message image URL",
                ],
                "header" => [
                    "type" => Type::string(),
                    "description" => "Message header",
                ],
                "footer" => [
                    "type" => Type::string(),
                    "description" => "Message footer",
                ],
                "textHeader" => [
                    "type" => Type::string(),
                    "description" => "Message text header",
                ],
                "textFooter" => [
                    "type" => Type::string(),
                    "description" => "Message text footer",
                ],
                "first" => [
                    "type" => Type::boolean(),
                    "description" => "Defines that the message is first in the conversation",
                    "defaultValue" => false
                ],

                "last" => [
                    "type" => Type::boolean(),
                    "description" => "Defines that the message is last in the conversation",
                    "defaultValue" => false
                ],

                "tail" => [
                    "type" => Type::boolean(),
                    "description" => "Defines that the message has visual \"tail\". Usually last message in conversation",
                    "defaultValue" => false
                ],



            ]

        ]));
    }


    public static function ObjectItemType()
    {
        if (isset(self::$objects['ObjectItemType'])) {
            return self::$objects['ObjectItemType'];
        } else {
            return self::$objects['ObjectItemType'] = new ObjectType([
                'description' => 'Объект данных',
                'name'        => 'ObjectItemType',
                'fields'      => function () {
                    return [
                        "tip"        => [
                            "description" => "Подсказка объекта",
                            "type" => Types::TipType(),
                        ],
                        'icon' => [
                            "title" => "SVG Иконка объекта",
                            "type" => Type::String(),

                        ],
                        "cardMode" => Type::boolean(),
                        "access"     => Types::AccessType(),
                        "intable"    => [
                            "description" => "Поля в таблице предпросмотра",
                            "type" => Type::listOf(Type::string())
                        ],
                        "groups"     => [
                            "type" => Type::listOf(Types::GroupType()),
                            "description" => "Группы для полей объекта",
                            'resolve'     => function ($root, $args) {

                                return $root['groups'] ?? null;
                            },
                        ],
                        "tabs" => [
                            "description" => "Табы для вывода предварительного просмотра",
                            "type" => Type::listOf(Types::TabType())
                        ],


                        "stats" => Type::listOf(Types::StatItemType()),

                        "title"      => Type::string(),
                        "key"        => Type::string(),
                        "fields"     => [
                            "description" => "Поля объекта",
                            "type" => Type::listOf(Types::FieldType())
                        ],
                        "buttons"    => [
                            "description" => "Быстрые комнады для объекта",
                            "type" => Type::listOf(Types::ActionType())
                        ],
                        "modal"      => [
                            "title" => "Создание в модальном окне",
                            "type" => Type::boolean()
                        ],


                        "expandable" => [
                            "type" => Types::ExpandableType(),
                            "description" => "Дополнительные данные"
                        ],
                        //    "declension" => $declension,
                        "filters"    => [
                            "description" => "Поля фильтрации",
                            "type" => Type::listOf(Types::FieldType())
                        ],
                        'single' => [
                            "type" => Type::boolean(),
                            "description" => "Еденичная запись"
                        ],
                        "hide" => [
                            "type" => Type::boolean(),
                        ]

                    ];
                },
            ]);
        }
    }




    public static function query()
    {
        return self::$query ?: (self::$query = new QueryType);
    }

    public static function mutation()
    {
        return self::$mutation ?: (self::$mutation = new MutationType());
    }
}
