<?php

namespace BaraBaaS\GraphQL\User;

use BaraBaaS\AuthType;
use BaraBaaS\GraphQL\Manager\Types;
use BaraBaaS\Schema\CodeGen;
use DevSpark\Utils\Response;
use BaraBaaS\UserRequest;
use GraphQL\Error\DebugFlag;
use GraphQL\Error\Error;
use GraphQL\Error\FormattedError;
use GraphQL\GraphQL;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\InterfaceType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use GraphQL\Utils\SchemaPrinter;

class UserGraphQL
{



    public static function init($input, $schemaParams, $codegen = false)
    {
        try {


            //inject default mutation

            //Very important in firts step make query inject for create return types

            if (isset($schemaParams['query'])) {

                $schemaParams['query'] = InjectQuery::inject($schemaParams['query']);
            }



            if (isset($schemaParams['mutation'])) {

                $schemaParams['mutation'] = InjectMutatuion::inject($schemaParams['mutation']);
            }







        
            $query = $input['query'];


            $variables = isset($input['variables']) ? $input['variables'] : null;

            $schema = new Schema([
                'query'    => new QueryType($schemaParams['query']),
                'mutation' => new MutationType($schemaParams['mutation']),
            ]);

            if ($codegen) {;

                echo  view("barabaas::codegen", ['types' => CodeGen::getTSType($schema), 'requests' => CodeGen::getRequests($schema)]);
                exit;
            }


            $rootResolver = [];

            $myErrorFormatter = function (Error $error) use ($query, $variables) {


                if ($query) {
                    $msg = urlencode("Error GRAPH QL BaraBaaS:  variable:" . json_encode($variables, JSON_UNESCAPED_UNICODE) . " input:" . json_encode($query, JSON_UNESCAPED_UNICODE) . " -  output :" . json_encode($error, JSON_UNESCAPED_UNICODE));
                    @file_get_contents("https://loger.apiloc.ru/chat/" . env("DORIS") . "/send?msg=" . $msg);
                }



                return FormattedError::createFromException($error);
            };

            $myErrorHandler = function (array $errors, callable $formatter) {

                return array_map($formatter, $errors);
            };

            $debug  = DebugFlag::INCLUDE_DEBUG_MESSAGE | DebugFlag::INCLUDE_TRACE;
            $result = GraphQL::executeQuery($schema, $query, $rootResolver, null, $variables, null, null)
                ->setErrorFormatter($myErrorFormatter)
                ->setErrorsHandler($myErrorHandler)->toArray($debug);

            return response()->json($result, 200);
        } catch (\Exception $e) {

            $result = [
                'error' => [
                    'message' => $e->getMessage(),
                ],
            ];
            return response()->json($result, 422);
        }
    }
}


class QueryType extends ObjectType
{
    public function __construct($fields)
    {
        $config = [
            'name'   => "Query",
            'fields' => $fields
        ];
        parent::__construct($config);
    }
}


class MutationType extends ObjectType
{
    public function __construct($fields)
    {
        $config = [
            'name'   => "Mutation",
            'fields' => $fields
        ];
        parent::__construct($config);
    }
}