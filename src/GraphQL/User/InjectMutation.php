<?php

namespace BaraBaaS\GraphQL\User;

use DevSpark\Engine\Auth;
use BaraBaaS\AuthType;
use BaraBaaS\mDB;
use BaraBaaS\RegType;
use DevSpark\Engine\Core;
use BaraBaaS\UserRequest;
use GraphQL\Type\Definition\Type;

class InjectMutatuion
{

    public static function inject($fields)
    {



        if (array_key_exists('authSMS', $fields) && $fields['authSMS'] == null) {

            $fields['authSMS'] = UserRequest::getMutationAuth(AuthType::SMS);
        }

        if (array_key_exists('authCall', $fields) && $fields['authCall'] == null) {

            $fields['authCall'] = UserRequest::getMutationAuth(AuthType::Call);
        }


        if (array_key_exists('authEmail', $fields) && $fields['authEmail'] == null) {

            $fields['authEmail'] = UserRequest::getMutationAuth(AuthType::Email);
        }
        if (array_key_exists('authApple', $fields) && $fields['authApple'] == null) {

            $fields['authApple'] = UserRequest::getMutationAuth(AuthType::Apple);
        }



        if (array_key_exists('authSoc', $fields) && $fields['authSoc'] == null) {

            $fields['authSoc'] = UserRequest::getMutationAuth(AuthType::Soc);
        }


        if (array_key_exists('regEmail', $fields) && $fields['regEmail'] == null) {

            $fields['regEmail'] = UserRequest::getMutationReg(RegType::Email);
        }




        if (array_key_exists('profile', $fields) && ($fields['profile'] == null || gettype($fields['profile'][0] ?? null) == 'string')) {



            $includeFields = gettype($fields['profile'][0] ?? null) == 'string' ? $fields['profile'] : null;



            $fields['profile'] = [
                'type' => Objects::get("users")->getGraphQLType("ProfileType"),
                "description" => "Редактирование профиля",
                "args"        => [
                    "fields" => [
                        'type' => Type::nonNull(Objects::get("users")->getGraphQLInputFields("ProfileFieldsInput", $includeFields)),
                    ],

                ],

                'resolve'     => function ($root, $args) {

                    $collection = Objects::get("users")->collection;
                    mDB::collection($collection)->updateOne([
                        "_id" => Auth::$user->_id,
                    ], ['$set' => $args['fields']]);
                    return mDB::collection($collection)->findOne(["_id" => Auth::$user->_id]);
                },
            ];
        }




        return $fields;
    }
}