<?php

namespace BaraBaaS\GraphQL\User;

use DevSpark\Engine\Auth;
use BaraBaaS\AuthType;
use BaraBaaS\RegType;
use DevSpark\Engine\Core;
use BaraBaaS\UserRequest;

use PhpParser\Node\Expr\Cast\Object_;

class InjectQuery
{

    public static function inject($fields)
    {



        if (array_key_exists('profile', $fields) && ($fields['profile'] == null || gettype($fields['profile'][0] ?? null) == 'string')) {

            $showFields = gettype($fields['profile'][0] ?? null) == 'string' ? $fields['profile'] : null;

            $fields['profile'] = [
                'type' => Core::getModel("users")->getGraphQLType("ProfileType", $showFields),
                "description" => "Профиль",
                'resolve'     => function ($root, $args) {

                    Auth::accessUser(true);

                    return  Auth::$user;
                }
            ];
        }





        return $fields;
    }
}