<?php

namespace DevSpark\Controllers;

use GraphQL\Type\Definition\Type;

enum AuthType: string
{
    case SMS = "SMS";
    case Call = "Call";
    case Soc = "Soc";
    case Apple = "Apple";
    case Email = "Email";
}


enum RegType: string
{
    case Email = "Email";
}



class UserController
{
   

    public static $queries = [];


    public static function getMutationAuth(AuthType $type)
    {


        $mutation =    [
            'type'        => Type::string()

        ];


        $mutation['description'] = "Авторизация через " . $type->value;

        if ($type == AuthType::SMS || $type == AuthType::Call) {
            $mutation['args'] = [

                "phone" => [
                    "type" => Type::nonNull(Type::string()),
                    "description" => "Номер телефона"
                ],
                'code' => [
                    "type" => Type::string(),
                    "description" => "Полученный код, для второго шага авторизации"
                ],

            ];
        }

        if ($type == AuthType::Soc) {
            $mutation['args'] = [

                "key" => [
                    "type" => Type::nonNull(Type::string()),
                    "description" => "Ключ авторизации Auth4App"
                ],

            ];
        }


        if ($type == AuthType::Apple) {
            $mutation['args'] = [

                "scopes" => [
                    "type" => Type::nonNull(Type::string()),
                    "description" => "Результат авторизации JSON строкой"
                ],

            ];
        }



        if ($type == AuthType::Email) {
            $mutation['args'] = [

                "email" => [
                    "type" => Type::nonNull(Type::string()),
                ],
                "password" => [
                    "type" => Type::nonNull(Type::string()),
                ],

            ];
        }

        $mutation['resolve'] =  function ($root, $args) use ($type) {
            if ($type == AuthType::SMS) return self::AuthPhoneResolve($args, true);
            if ($type == AuthType::Soc) return self::AuthSocResolve($args, true);
            if ($type == AuthType::Apple) return self::AuthAppleResolve($args, true);
            if ($type == AuthType::Email) return self::AuthEmailResolve($args, true);
        };

        return $mutation;
    }


    public static function getMutationReg(RegType $type)
    {


        $mutation =    [
            'type'        => Type::string()

        ];


        $mutation['description'] = "Регистрация через " . $type->value;


        if ($type == RegType::Email) {
            $mutation['args'] = [

                "email" => [
                    "type" => Type::nonNull(Type::string()),
                ],
                "password" => [
                    "type" => Type::string(),
                ],

                "code" => [
                    "type" => Type::string(),
                ],
            ];
        }

        $mutation['resolve'] =  function ($root, $args) use ($type) {

            if ($type == RegType::Email) return self::RegEmailResolver($args, true);
        };

        return $mutation;
    }


    public static function AuthPhoneResolve($params, $isGraphQL = false)
    {

       
        Auth::accessApiKey($isGraphQL);

        RequestUtils::hasValueValidator(['phone'], $params);


        $params['phone'] = (int) preg_replace("/[^,.0-9]/", '', $params['phone']);



        if (isset($params['code'])) {

            $user = mDB::collection("users")->findOne([
                "phone" => (int) $params['phone'],
                "code" => (int) $params['code'],
                "manager" => Auth::$apikey->_id,
            ]);

            if ($user) return  Auth::getToken($user->_id);

            mDB::collection("users")->updateOne([
                "_id" => $user->_id,
            ], [
                '$unset' => [
                    'code' => 1
                ]
            ]);

            RequestUtils::response(['key' => 'Неверный SMS код'], 422);
        } else {


            $code = rand(1111, 9999);

            mDB::collection("users")->updateOne([
                "phone" => (int) $params['phone'],
                "manager" => Auth::$apikey->_id,
            ], [
                '$set' => [
                    "phone" => (int) $params['phone'],
                    "manager" => Auth::$apikey->_id,
                    "code" => $code,
                    "code_created_at" => time(),

                ]
            ], ['upsert' => true]);



            return $code;
        }
    }

    public static function AuthSocResolve($params, $isGraphQL = false)
    {


      
        Auth::accessApiKey($isGraphQL);

        RequestUtils::hasValueValidator(['key'], $params);


        $dataAuth = file_get_contents('https://auth4app.com/hash?key=' . $params['key']);

        $data = json_decode($dataAuth, true);

        if ($data['type'] == 'error') {
            RequestUtils::response(['key' => 'Ошибка авторизации'], 422);
        }

        $userSoc = $data['data'];

        $user = mDB::collection("users")->findOne([
            "social.id" => $userSoc['id'],
            "manager" => Auth::$apikey->_id,
        ]);

        if ($user) {
            return Auth::getToken($user->_id);
        } else {

            $user = mDB::collection("users")->insertOne([
                "manager" => Auth::$apikey->_id,
                'name' => $userSoc['name'] ?? null,
                'surname' => $userSoc['surname'] ?? null,
                'email' => $userSoc['mail'] ?? null,
                "photo" => $userSoc['photo'] ?? null,
                "social" => [$userSoc],
                "created_at" => time(),
            ]);

            return  Auth::getToken($user->getInsertedId());
        }
    }


    public static function  AuthAppleResolve($params, $isGraphQL = false)
    {

      
        Auth::accessApiKey($isGraphQL);

        RequestUtils::hasValueValidator(['scopes'], $params);

        $scopes = $params['scopes'];
        if (is_string($scopes)) {
            $scopes = json_decode($scopes, true);
        }

        $user = mDB::collection("users")->findOne([
            "social.id" => $scopes['user'],
            "manager" => Auth::$apikey->_id,
        ]);

        if ($user) {
            return Auth::getToken($user->_id);
        } else {
            $scopes['id'] = $scopes['user'];

            $user = mDB::collection("users")->insertOne([
                "manager" => Auth::$apikey->_id,
                'name' => $scopes['fullName']['givenName'] ?? null,
                'surname' => $scopes['fullName']['familyName'] ?? null,
                'email' => $scopes['email'] ?? null,
                "social" => [$scopes],
                "created_at" => time(),
            ]);

            return Auth::getToken($user->getInsertedId());
        }
    }

    public static function  AuthEmailResolve($params, $isGraphQL = false)
    {

       
        Auth::accessApiKey($isGraphQL);

        RequestUtils::hasValueValidator(['email', "password"], $params);

        $user = mDB::collection("users")->findOne(["manager" => Auth::$apikey->_id, "password" => Auth::getPassword($params['password']), "email" => $params['email']]);

        if ($user) {
            return  Auth::getToken($user->_id);
        } else {
            RequestUtils::response(['email' => 'Неверная пара email и пароль'], 422);
        }
    }


    public static function RegEmailResolver($params, $isGraphQL)
    {


       
        Auth::accessApiKey($isGraphQL);

        RequestUtils::hasValueValidator(['email'], $params);


        if (RequestUtils::hasValue(['password', 'code'], $params)) {


            $user = mDB::collection("users")->findOne([
                "email" => $params['email'],
                "manager" => Auth::$apikey->_id,
                "code" => (int) $params['code'],
            ]);

            if (!$user) {
                return RequestUtils::response(['code' => 'Вы указали неверный код.'], 422);
            }

            mDB::collection("users")->updateOne(
                [
                    "_id" => $user->_id,
                ],
                [
                    '$set' => [
                        "password" => Auth::getPassword($params['password']),
                    ],
                    '$unset' => [
                        'code' => 1
                    ]
                ]
            );

            return Auth::getToken($user->_id);
        } else {


            $code = 999999; // rand(111111, 999999);

            mDB::collection("users")->updateOne([
                "email" => $params['email'],
                "manager" => Auth::$apikey->_id,
            ], [
                '$set' => [
                    "email" => $params['email'],
                    "manager" => Auth::$apikey->_id,
                    "code" => $code,
                    "code_created_at" => time(),

                ]
            ], ['upsert' => true]);



            return $code;
        }
    }
}
