<?php

namespace BaraBaaS\Schema;

use BaraBaaS\Auth;
use BaraBaaS\mDB;

use BaraBaaS\Utils;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



trait ObjectQuery
{



    private $afterFindMiddleware; //$result, $this
    private $beforeFindMiddleware; //$params, $this

    private $afterInsertMiddleware; //$result, $this
    private $beforeInsertMiddleware; //$fields, $this

    private $afterUpdateMiddleware; //$result, $_id, $this
    private $beforeUpdateMiddleware; // $fields, $_id, $this




    public function setAfterInsertMiddleware($afterInsertMiddleware): ObjectItem
    {

        $this->afterInsertMiddleware = $afterInsertMiddleware;

        return $this;
    }

    public function setbeforeInsertMiddleware($beforeInsertMiddleware): ObjectItem
    {
        $this->beforeInsertMiddleware = $beforeInsertMiddleware;

        return $this;
    }

    public function setAfterFindMiddleware($afterFindMiddleware): ObjectItem
    {

        $this->afterFindMiddleware = $afterFindMiddleware;

        return $this;
    }

    public function setBeforeFindMiddleware($beforeFindMiddleware): ObjectItem
    {
        $this->beforeFindMiddleware = $beforeFindMiddleware;

        return $this;
    }

    public function setAfterUpdateMiddleware($afterUpdateMiddleware): ObjectItem
    {

        $this->afterUpdateMiddleware = $afterUpdateMiddleware;

        return $this;
    }

    public function setBeforeUpdateMiddleware($beforeUpdateMiddleware): ObjectItem
    {
        $this->beforeUpdateMiddleware = $beforeUpdateMiddleware;

        return $this;
    }


    public function __call($func, $args)
    {
        if (property_exists($this, $func) && is_callable($this->$func)) {
            return call_user_func_array($this->$func, $args);
        }
    }


    public function delete($_id, $reason)
    {

        if (!$this->access['delete']) {
            RequestUtils::response([
                "update" => "Вы не можете удалять записи в " . $this->title
            ], 422);
        }

        mDB::collection($this->collection)->updateOne([
            "_id" => mDB::id($_id)
        ], [
            '$set' => [
                'deleted_at' => time(),
                "deleted_reason" => $reason

            ]
        ]);
    }

    private function getLookUp($field, $prefix = "", $deep = 0)
    {
        $pipeline = [];

        $childrenPipeline = [];
        $project = [
            '_id' => true,
            'id' => true
        ];



        if ($deep > 1) {
            $chaildFields = Objects::getObjectByCollection($field->object)->fields;
            foreach ($chaildFields as  $chaildField) {

                $project[$chaildField->key] = true;
                if ($chaildField->input == 'pointer' || $chaildField->input == 'relation') {

                    $lookup = $this->getLookUp($chaildField, "", $deep - 1);
                    if ($lookup) {
                        $childrenPipeline = [...$childrenPipeline, ...$lookup];
                    }
                }
            }
        }




        if ($field->input == 'pointer') {


            $pipeline[] = [

                '$lookup' => [
                    'from' => $field->object,
                    'as' => $field->key,
                    'let' => [$field->key =>  '$' . $field->key],
                    'pipeline' => [
                        ['$project' => $project],
                        ['$match' => ['$expr' => ['$eq' => ['$_id', '$$' . $field->key]], 'deleted_at' => ['$exists' => false]]],
                        ['$limit' => 1], ...$childrenPipeline
                    ]
                ]
            ];

            $pipeline[] = [

                '$addFields' => [
                    $field->key => [
                        '$arrayElemAt' => ['$' . $field->key, 0]
                    ]
                ]
            ];
        }

        if ($field->input == 'relation') {


            $pipeline[] = [

                '$lookup' => [
                    'from' => $field->object,
                    'as' => $field->key,
                    'let' => [$field->key => ['$cond' => [['$isArray' => '$' . $field->key], '$' . $field->key, []]]],
                    'pipeline' => [
                        ['$project' => $project],
                        ['$match' => ['$expr' => ['$in' => ['$_id', '$$' . $field->key]],  'deleted_at' => ['$exists' => false]]],
                        ...$childrenPipeline
                    ]
                ]

            ];
        }



        return $pipeline;
    }

    public function find($params)
    {
        /*
        Params
        "fields"  => $request['fields'] ?? [],
        "limit"   => $request['limit'] ?? 20,
        'sample' => true/false,
        "skip"    => $request['skip'] ?? 0,
        "sort"    => $request['sort'] ?? [],
        "filter"  => $request['filter'] ?? [],
        "search"  => $request['search'] ?? null,
        "reverse" => $request['reverse'] ?? false,
        "_id"     => $request['_id'] ?? null,
        deep
         */

        if ($this->beforeFindMiddleware && is_callable($this->beforeFindMiddleware)) {

            $params = $this->beforeFindMiddleware($params, $this) ?? $params;
        }



        $pipeline = RequestUtils::prepareAggregateParams($this->key, $params);


        if (isset(Schema::$globalSplitParams) && count(Schema::$globalSplitParams) > 0) {

            $pipeline  = [
                [
                    '$match' => Schema::$globalSplitParams
                ],

                ...$pipeline
            ];
        }



        if ($params['deep'] ?? $this->deep > 0) {

            $deep = $params['deep'] ?? $this->deep;

            foreach ($this->fields as $key => $field) {


                if (!isset($params['fields']) || (in_array($key, $params['fields']) && count($params['fields']) > 0)) {

                    if ($field->input == 'pointer' || $field->input == 'relation') {

                        $lookup = $this->getLookUp($field, "", $deep);
                        if ($lookup)
                            $pipeline = [...$pipeline, ...$lookup];
                    }
                }
            }
        }


        $pipeline = RequestUtils::packingPipeline($pipeline);




        $result = RequestUtils::ObjectIdToString(mDB::collection($this->collection)->aggregate($pipeline)->toArray());




        //   $result = RequestUtils::relatedData(mDB::nArray($result), $params['deep'] ?? $this->deep);

        if ($this->afterFindMiddleware && is_callable($this->afterFindMiddleware)) {

            $result = $this->afterFindMiddleware($result, $this) ?? $result;
        }

        return $result;
    }

    public function total($params)
    {

        if ($this->beforeFindMiddleware && is_callable($this->beforeFindMiddleware)) {

            $params = $this->beforeFindMiddleware($params, $this) ?? $params;
        }

        $params['limit'] = 'all';

        $pipeline = RequestUtils::prepareAggregateParams($this->key, $params);

        if (isset(Schema::$globalSplitParams) && count(Schema::$globalSplitParams) > 0) {

            $pipeline  = [
                [
                    '$match' => Schema::$globalSplitParams
                ],

                ...$pipeline
            ];
        }

        $pipeline = RequestUtils::packingPipeline($pipeline);

        $pipeline[] = [
            '$group' => [
                "_id" => null,
                "count" => ['$sum' => 1]
            ]
        ];



        $total = mDB::collection($this->collection)->aggregate($pipeline)->toArray()[0]['count'] ?? 0;

        // mDB::collection($findParams['collection'])->countDocuments($findParams['filter'], ['projection' => ['_id']]);

        return $total;
    }

    public function update($values, $_id)
    {


        if (!$this->access['update']) {
            RequestUtils::response([
                "update" => "Вы не можете редактировать записи в " . $this->title
            ], 422);
        }


        $values = RequestUtils::validRelationships($values);

        $setFields = [];
        //Оставляем только доступные поля
        foreach ($this->fields as $field) {


            if (!$field->onlyshow) {
                if (isset($values[$field->key]) && $field->validWithFieldDisplay($values)) {
                    $setFields[$field->key] = $values[$field->key];
                } else {
                    $setFields[$field->key] = $field->getNull();
                }
            }
        }
        $values = $setFields;



        foreach ($this->fields as $field) {

            $value = $values[$field->key] ?? null;
            if ($field->input == 'html') {
                $value = strip_tags($value);
            }

            if (!$field->onlyshow && $field->validWithFieldDisplay($values) && $field->required && $field->isEmpty($value)) {

                $group = $this->getGroupWithField($field->key);

                RequestUtils::response([
                    $field->key => ($group ? "[" . $group->title . "]/" : "") . $field->title . " - обязательно для заполнения."
                ], 422);
            }
        }

        //Проверка обязательых полей


        if (isset($values['_id'])) {
            unset($values['_id']);
        }

        if (in_array($this->collection, Schema::$personalCollections)) {
            $values['manager'] = Auth::$manager_id;
        }

        if ($this->beforeUpdateMiddleware && is_callable($this->beforeUpdateMiddleware)) {
            $values = $this->beforeUpdateMiddleware($values, $_id, $this) ?? $values;
        }

        $this->changesLog($_id, $values);


        mDB::collection($this->collection)->updateOne([
            "_id" => mDB::getId($_id),
        ], ['$set' => $values]);


        $this->reCalcSearchFields($_id);

        $result = mDB::collection($this->collection)->findOne([
            "_id" => mDB::getId($_id),
        ]);

        $result = RequestUtils::relatedData(mDB::nArray([$result]), $this->deep)[0];

        if ($this->afterUpdateMiddleware && is_callable($this->afterUpdateMiddleware)) {
            $result = $this->afterUpdateMiddleware($result, $_id, $this) ?? $result;
        }

        return $result;
    }



    public function setValueForMany($key, $value, $ids)
    {


        $_ids = [];
        foreach ($ids as $id) {
            $_ids[] = mDB::id($id);
        }

        if (!$this->access['update']) {
            RequestUtils::response([
                "update" => "Вы не можете редактировать записи в " . $this->title
            ], 422);
        }


        $values = RequestUtils::validRelationships([
            $key => $value
        ]);

        $setFields = [];
        $field = $this->getField($key);


        if ($field->isEmpty($values[$field->key])) {

            RequestUtils::response([
                "update" => "Установите новое значение для " . $field->title
            ], 422);
        }

        if (!$field->onlyshow) {
            if (isset($values[$field->key]) && $field->validWithFieldDisplay($values)) {
                $setFields[$field->key] = $values[$field->key];
            } else {
                $setFields[$field->key] = $field->getNull();
            }
        }




        mDB::collection($this->collection)->updateMany([
            "_id" => ['$in' => $_ids],
            "manager" => Auth::$manager_id
        ], ['$set' => $setFields]);




        return true;
    }

    public function insert($values)
    {


        if (!$this->access['add']) {
            RequestUtils::response([
                "update" => "Вы не можете добавлять записи в " . $this->title
            ], 422);
        }


        $values = RequestUtils::validRelationships($values);

        $setFields = [];
        //Оставляем только доступные поля
        foreach ($this->fields as $field) {


            if (!$field->onlyshow) {
                if (isset($values[$field->key]) && $field->validWithFieldDisplay($values)) {
                    $setFields[$field->key] = $values[$field->key];
                } else {
                    $setFields[$field->key] = $field->getNull();
                }
            } else {
                $setFields[$field->key] = $field->getNull();
            }
        }
        $values = $setFields;



        foreach ($this->fields as $field) {

            $value = $values[$field->key] ?? null;
            if ($field->input == 'html') {
                $value = strip_tags($value);
            }

            if ($field->validWithFieldDisplay($values) && $field->required && $field->isEmpty($value)) {
                $group = $this->getGroupWithField($field->key);

                RequestUtils::response([
                    $field->key => ($group ? "[" . $group->title . "]/" : "") . $field->title . " - обязательно для заполнения."
                ], 422);
            }
        }



        $values['manager']    = Auth::$manager_id;


        $values = array_merge($values, Auth::getInsertFields());

        $values = array_merge($values, Schema::$globalSplitParams);

        if ($this->beforeInsertMiddleware && is_callable($this->beforeInsertMiddleware)) {

            $values = $this->beforeInsertMiddleware($values, $this) ?? $values;
        }

        $result = mDB::collection($this->collection)->insertOne($values);

        $values['_id'] = (string) $result->getInsertedId();

        $this->reCalcSearchFields($values['_id']);

        $result = RequestUtils::relatedData(mDB::nArray([$values]))[0];

        if ($this->afterInsertMiddleware && is_callable($this->afterInsertMiddleware)) {

            $result = $this->afterInsertMiddleware($result, $this) ?? $result;
        }

        return $result;
    }

    public function distinct($params)
    {
        if ($this->beforeFindMiddleware && is_callable($this->beforeFindMiddleware)) {

            $params = $this->beforeFindMiddleware($params, $this) ?? $params;
        }

        $findParams = RequestUtils::prepareFindParams($this->key, $params);
        $findParams['filter'] = array_merge($findParams['filter'], Schema::$globalSplitParams);

        $result = mDB::collection($findParams['collection'])->distinct($params['field'], $findParams['filter']);

        if (isset(Objects::$relationships[$this->key])) {

            $relationship = Objects::$relationships[$params['field']];

            $collection_rel = $relationship[0];

            $result = mDB::collection($collection_rel)->find([
                '_id' => ['$in' => $result],
            ]);
        }

        //$result = RequestUtils::relatedData($result, 2);
        $result = RequestUtils::ObjectIdToString($result);

        if ($this->afterFindMiddleware && is_callable($this->afterFindMiddleware)) {

            $result = $this->afterFindMiddleware($result, $this) ?? $result;
        }

        return  $result;
    }

    public function export($params)
    {
        /* PARAMS
        "sort"    => $request['sort'] ?? [],
        "filter"  => $request['filter'] ?? [],
        "search"  => $request['search'] ?? null,
        "reverse" => $request['reverse'] ?? false,
        "format"  => $request['format'] ?? 'xlsx',
         */
        $params['export'] = true;

        if ($this->beforeFindMiddleware && is_callable($this->beforeFindMiddleware)) {

            $params = $this->beforeFindMiddleware($params, $this) ?? $params;
        }


        $findParams = RequestUtils::prepareFindParams($this->key, $params);
        $findParams['filter'] = array_merge($findParams['filter'], Schema::$globalSplitParams);

        $total = mDB::collection($findParams['collection'])->countDocuments($findParams['filter'], ['projection' => ['_id']]);

        if ($total > 50000) {
            RequestUtils::response(['export' => 'Достигнут лимит кол-ва строк для экспорта, уменьшите диапазон данных с помощью фильтра.'], 422);
        }

        $result = mDB::collection($findParams['collection'])->find($findParams['filter'], $findParams['options']);

        $result = RequestUtils::relatedData(mDB::nArray($result), $this->deep);

        if ($this->afterFindMiddleware && is_callable($this->afterFindMiddleware)) {

            $result = $this->afterFindMiddleware($result, $this);
        }

        set_time_limit(0);

        date_default_timezone_set("Europe/Moscow");

        $exportData = [];
        //Создание шапки
        $index = 0;

        $keys = [];
        foreach ($this->fields as $field) {

            $keys[]                = $field->key;
            $exportData[0][$index] = $field->title;
            $index++;
        }

        $row = 1;
        foreach ($result as $key => $values) {

            $index = 0;

            foreach ($this->fields as $field) {

                $exportData[$row][$index] = $field->getExportValue($values);
                $index++;
            }
            $row++;
        }

        $filename = $this->title . "_" . md5(time() . $this->key . rand(111111111, 999999999)) . '.xlsx';

        if ($params['format'] == 'xlsx') {

            $col = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

            $spreadsheet = new Spreadsheet();

            $sheet = $spreadsheet->getActiveSheet();
            $sheet->getDefaultColumnDimension()->setWidth(24);
            $index = 0;
            foreach ($exportData as $index => $vals) {
                foreach ($vals as $indexVal => $val) {
                    $sheet->setCellValue($col[$indexVal] . ($index + 1), $val);
                }
            }

            $writer = new Xlsx($spreadsheet);

            Utils::createPath('/storage/files/xlsx/');

            $writer->save(base_path() . '/storage/files/xlsx/' . $filename);

            $url = Utils::saveToS3(base_path() . '/storage/files/xlsx/' . $filename, $filename, "exports-xlsx");
            unlink(base_path() . '/storage/files/xlsx/' . $filename);
            return $url;
        }

        if ($params['format'] == 'csv') {

            Utils::createPath('/storage/files/csv/');
            $fp = fopen(base_path() . '/storage/csv/' . $filename, 'w');

            foreach ($exportData as $fields) {
                fputcsv($fp, $fields);
            }

            fclose($fp);
            $url = Utils::saveToS3(base_path() . '/storage/files/csv/' . $filename, $filename, "exports-csv");
            unlink(base_path() . '/storage/files/csv/' . $filename);
            return $url;
        }

        return 'none';
    }

    private function changesLog($_id, $newData, $action = 'update', $titleAction = "Редактирование")
    {

        $was = mDB::collection($this->collection)->findOne([
            "_id" => mDB::id($_id),
        ]);

        $item = [
            "test"          => true,
            'was'           => [],
            'became'        => [],
            "created_at"    => time(),
            "action"        => $action,
            "titleAction"   => $titleAction,
            "collection"    => $this->collection,
            "itemId"        => mDB::id($_id),
            "actionManager" => Auth::$manager->_id ?? null,
            "manager"       => $was->manager ?? null,
        ];

        $changed = false;

        $changedFields = [];
        foreach ($newData as $key => $val) {
            if (stripos($key, '.') === false) {
                if (isset($was[$key])) {
                    if (json_encode($was[$key]) != json_encode($val)) {
                        $changed              = true;
                        $item['was'][$key]    = $was[$key];
                        $item['became'][$key] = $val;
                        $changedFields[]      = $this->fields[$key]->title;
                    }
                } else {
                    if (!isset($item['was'][$key]) && $val == null) {

                        //Нет записи в Монге и пришел null от клиента
                        //Возможно тут нужна еще логика

                    } else {
                        $changed = true;

                        $item['was'][$key]    = null;
                        $item['became'][$key] = $val;
                        $changedFields[]      = $this->fields[$key]->title;
                    }
                }
            }
        }
        $item['changedFields'] = implode(', ', $changedFields);

        if ($changed) {
            mDB::collection("changesLog")->insertOne($item);
        }
    }

    public function changes($_id)
    {

        $managers = mDB::collection("managers")->find([

            "_id" => ['$in' => mDB::collection("changesLog")->distinct("actionManager", [
                "manager"    => Auth::$manager_id,
                "itemId"     => mDB::id($_id),
                "collection" => $this->collection,
            ])],
        ]);

        $partners = mDB::collection("partners")->find([

            "_id" => ['$in' => mDB::collection("changesLog")->distinct("actionPartner", [
                "manager"    => Auth::$manager_id,
                "itemId"     => mDB::id($_id),
                "collection" => $this->collection,
            ])],
        ]);

        $result = mDB::collection("changesLog")->find([
            "itemId"     => mDB::id($_id),
            "collection" => $this->collection,
        ], [
            "sort"  => [
                "_id" => -1,

            ],
            "limit" => 100,
        ]);

        $result = RequestUtils::relatedData(mDB::nArray($result));

        foreach ($managers as $manager) {

            foreach ($result as &$value) {
                if (isset($value['actionManager']) && $value['actionManager'] == (string) $manager->_id) {
                    $value['actionManager'] = ($manager->name ?? '') . ' ' . ($manager->surname ?? '');
                }
            }
        }

        foreach ($partners as $partner) {

            foreach ($result as &$value) {
                if (isset($value['actionPartner']) && $value['actionPartner'] == (string) $partner->_id) {
                    $value['actionManager'] = ($partner->email ?? "") . ' ' . ($partner->title ?? '');
                }
            }
        }

        return $result;
    }



    public function reCalcSearchFields($_id)
    {

        $item = mDB::collection($this->collection)->findOne([
            "_id" => mDB::id($_id),
        ]);

        $search_string = [];

        foreach ($item as $key => $value) {

            if (isset($this->fields[$key])) {

                $search_string[] = $this->fields[$key]->getSearchValue($value);
            }
        }

        mDB::_collection($this->collection)->updateOne([
            "_id" => mDB::id($_id),
        ], [
            '$set' => [
                "search_string" => implode(" ", $search_string),
            ],
        ]);
    }
}