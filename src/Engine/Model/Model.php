<?php

namespace DevSpark\Engine\Model;

use DevSpark\Engine\MongoDBConnection;
use MongoDB\InsertOneResult;
use MongoDB\InsertManyResult;
use MongoDB\UpdateResult;
use MongoDB\DeleteResult;
use MongoDB\Collection;
use MongoDB\Model\BSONDocument;
use MongoDB\BSON\ObjectId;
use MongoDB\Driver\Cursor;

/**
 * Class Model
 *
 * Базовый класс для работы с моделями MongoDB.
 *
 * @package DevSpark
 */
class Model
{
    /**
     * Название коллекции.
     *
     * @var string
     */
    public static $collection;

    /**
     * Схема модели.
     *
     * @var array
     */
    static $schema = [];

    /**
     * Схема поля для веб-панели.
     *
     * @var array
     */
    static $fields = [];


     /**
     * Персональные записи
     *
     * @var array
     */
    static $personalFor = [];

    /**
     * Название модели.
     *
     * @var array
     */
    static $title = [];

    /**
     * Идентификатор документа.
     *
     * @var ObjectId|null
     */
    private ?ObjectId $_id = null;

    /**
     * Флаг мягкого удаления.
     *
     * @var bool
     */
    private $softDelete = false;

    /**
     * Данные модели.
     *
     * @var array
     */
    public $data;

    /**
     * Конструктор класса.
     *
     * @param array $attributes Атрибуты модели.
     *
     * @return Model
     */
    public function __construct(array $attributes = [])
    {
        $this->fill($attributes);
        return $this;
    }

    /**
     * Получение значения атрибута.
     *
     * @param string $name Название атрибута.
     *
     * @return mixed|null Значение атрибута или null, если атрибут не определен.
     */
    public function __get(string $name)
    {
        return $this->data[$name] ?? null;
    }

    /**
     * Установка значения атрибута.
     *
     * @param string $name  Название атрибута.
     * @param mixed  $value Значение атрибута.
     *
     * @return void
     */
    public function __set(string $name, $value): void
    {
        $this->data[$name] = $value;
    }

    /**
     * Заполнение модели данными.
     *
     * @param array $attributes Атрибуты модели.
     *
     * @return Model
     */
    public function fill(array $attributes): Model
    {
        foreach ($attributes as $key => $value) {
            $data[$key] = $value;
        }

        return $this;
    }

    /**
     * Сохранение модели.
     *
     * @return void
     */
    public function save(): ?ObjectId
    {
        $oldData = $this->toArray();
        $data = $this->toArray();
        if ($this->_id === null) {
            $this->_id = static::_insertOne($data);
            Logger::logChanges(static::$collection, $this->_id, [], $data);
            return $this->_id;
        } else {
            static::_updateOne(['_id' => $this->_id], ['$set' => $data]);
            Logger::logChanges(static::$collection, $this->_id, $oldData, $data);
            return $this->_id;
        }
    }

    


    /**
     * Ищет одну запись в коллекции, исключая мягко удаленные записи.
     *
     * @param array $filter Массив фильтров для поиска записи.
     *
     * @return Model|null Найденная запись или null, если запись не найдена.
     */
    public function find(array $filter): ?Model
    {
        // исключаем мягко удаленные записи при поиске
        $filter['deleted_at'] = ['$exists' => false];
        $document = MongoDBConnection::get()->selectCollection(static::$collection)->findOne($filter);
        if ($document === null) {
            return null;
        }
        return new static($document->getArrayCopy());
    }

    /**
     * Удаляет запись из коллекции, используя мягкое удаление или обычное удаление.
     *
     * @return void
     */
    public function delete(): void
    {
        // проверяем, используем ли мягкое удаление
        if ($this->softDelete) {
            $data = ['deleted_at' => time()];
            static::_updateOne(['_id' => $this->_id], ['$set' => $data]);
        } else {
            static::_deleteOne(['_id' => $this->_id]);
        }
    }

    /**
     * Создает новую запись в коллекции и сохраняет ее.
     *
     * @param array $attributes Массив атрибутов для создания записи.
     *
     * @return Model Возвращает созданную запись.
     */
    public function create(array $attributes): Model
    {
        $model = new static($attributes);
        $model->save();
        return $model;
    }

    /**
     * Возвращает массив атрибутов объекта модели без ID.
     *
     * @return array Массив атрибутов модели.
     */
    public function toArray(): array
    {
        $data = [];

        foreach ($this->data as $key => $value) {
            if ($key !== '_id') {
                $data[$key] = $value;
            }
        }
        return $data;
    }

    /**
     * Удаляет все записи из коллекции, используя мягкое удаление или обычное удаление.
     *
     * @param array $filter Массив фильтров для удаления записей.
     *
     * @return int Количество удаленных записей.
     */
    public static function destroy(array $filter): int
    {
        // проверяем, используем ли мягкое удаление
        if (static::$softDelete) {
            $data = ['deleted_at' => time()];
            return static::_updateMany($filter, ['$set' => $data]);
        } else {
            return static::_deleteMany($filter);
        }
    }

    /**
     * Устанавливает флаг мягкого удаления для всех моделей.
     *
     * @param bool $softDelete Флаг мягкого удаления.
     *
     * @return void
     */
    public static function withSoftDelete(bool $softDelete): void
    {
        static::$softDelete = $softDelete;
    }

    /**
     * Вставляет одну запись в коллекцию.
     *
     * @param array $document Массив полей для вставки.
     *
     * @return ObjectId Идентификатор вставленной записи.
     */
    public static function _insertOne(array $document): ?ObjectId
    {
        static::beforeInsert($document); // вызываем хук перед вставкой
        $result = MongoDBConnection::get()->selectCollection(static::$collection)->insertOne($document);
        static::afterInsert($result->getInsertedId()); // вызываем хук после вставки

        return $result->getInsertedId();
    }
    /**
     * Вставляет несколько документов в коллекцию.
     *
     * @param array $documents Массив документов, которые необходимо вставить.
     * @return array Массив идентификаторов вставленных документов.
     */
    public static function _insertMany(array $documents): array
    {
        $result = MongoDBConnection::get()->selectCollection(static::$collection)->insertMany($documents);

        return $result->getInsertedIds();
    }

    /**
     * Обновляет один документ в коллекции.
     *
     * @param array $filter Массив, содержащий критерии выборки документов для обновления.
     * @param array $update Массив новых значений.
     * @param array $options Опции обновления.
     * @return int Количество модифицированных документов.
     */
    public static function _updateOne(array $filter, array $update, array $options = []): int
    {
        static::beforeUpdate($filter, $update, $options); // вызываем хук перед обновлением
        $result = MongoDBConnection::get()->selectCollection(static::$collection)->updateOne($filter, $update, $options);
        static::afterUpdate($result->getModifiedCount()); // вызываем хук после обновления
        return $result->getModifiedCount();
    }

    /**
     * Обновляет несколько документов в коллекции.
     *
     * @param array $filter Массив, содержащий критерии выборки документов для обновления.
     * @param array $update Массив новых значений.
     * @param array $options Опции обновления.
     * @return int Количество модифицированных документов.
     */
    public static function _updateMany(array $filter, array $update, array $options = []): int
    {
        $result = MongoDBConnection::get()->selectCollection(static::$collection)->updateMany($filter, $update, $options);
        return $result->getModifiedCount();
    }

    /**
     * Удаляет один документ из коллекции.
     *
     * @param array $filter Массив, содержащий критерии выборки документов для удаления.
     * @return int Количество удаленных документов.
     */
    public static function _deleteOne(array $filter): int
    {
        static::beforeDelete($filter); // вызываем хук перед удалением

        $result = MongoDBConnection::get()->selectCollection(static::$collection)->deleteOne($filter);
        static::afterDelete($result->getDeletedCount()); // вызываем хук после удаления

        return $result->getDeletedCount();
    }

    /**
     * Удаляет несколько документов из коллекции.
     *
     * @param array $filter Массив, содержащий критерии выборки документов для удаления.
     * @return int Количество удаленных документов.
     */
    public static function _deleteMany(array $filter): int
    {
        $result = MongoDBConnection::get()->selectCollection(static::$collection)->deleteMany($filter);
        return $result->getDeletedCount();
    }

    /**
     * Находит и возвращает первый документ, удовлетворяющий заданным критериям.
     *
     * @param array $filter Массив, содержащий критерии выборки документов.
     * @param array $options Опции поиска.
     * @return ?BSONDocument Найденный документ или null, если документ не найден.
     */
    public static function _findOne(array $filter, array $options = []): ?BSONDocument
    {
        return MongoDBConnection::get()->selectCollection(static::$collection)->findOne($filter, $options);
    }


    /**
     * Выполняет поиск документов в коллекции MongoDB с использованием заданных фильтров и опций.
     * @param array $filter Массив, содержащий критерии поиска.
     * @param array $options Массив с дополнительными опциями для запроса.
     * @return Cursor Возвращает курсор, который можно использовать для итерации по найденным документам.
     */
    public static function _find(array $filter, array $options = []): Cursor
    {
        return MongoDBConnection::get()->selectCollection(static::$collection)->find($filter, $options);
    }


    /**
     * Метод, вызываемый перед добавлением нового документа в коллекцию
     *
     * @param array $document Документ для добавления (передается по ссылке и может быть изменен в хуке)
     * @return void
     */
    public static function beforeInsert(array &$document)
    {
        // empty hook, can be overridden in model
    }

    /**
     * Метод, вызываемый после успешного добавления нового документа в коллекцию
     *
     * @param string $insertedId Идентификатор добавленного документа
     * @return void
     */
    public static function afterInsert(string $insertedId)
    {
        // empty hook, can be overridden in model
    }

    /**
     * Метод, вызываемый перед обновлением документов в коллекции
     *
     * @param array $filter Критерий выборки документов, которые нужно обновить
     * @param array $update Данные для обновления (передаются по ссылке и могут быть изменены в хуке)
     * @param array $options Опции для обновления
     * @return void
     */
    public static function beforeUpdate(array $filter, array &$update, array $options = [])
    {
        // empty hook, can be overridden in model
    }

    /**
     * Метод, вызываемый после успешного обновления документов в коллекции
     *
     * @param int $modifiedCount Количество обновленных документов
     * @return void
     */
    public static function afterUpdate(int $modifiedCount)
    {
        // empty hook, can be overridden in model
    }

    /**
     * Метод, вызываемый перед удалением документов из коллекции
     *
     * @param array $filter Критерий выборки документов, которые нужно удалить
     * @return void
     */
    public static function beforeDelete(array $filter)
    {
        // empty hook, can be overridden in model
    }

    /**
     * Метод, вызываемый после успешного удаления документов из коллекции
     *
     * @param int $deletedCount Количество удаленных документов
     * @return void
     */
    public static function afterDelete(int $deletedCount)
    {
        // empty hook, can be overridden in model
    }
}
