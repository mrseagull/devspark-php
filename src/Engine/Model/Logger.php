<?php

namespace DevSpark\Engine\Model;

use DevSpark\Engine\MongoDBConnection;
use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Driver\Exception\Exception;
use MongoDB\Model\BSONDocument;

/**
 * Class Logger
 *
 * Класс для логирования изменений моделей MongoDB.
 *
 * @package DevSpark\Engine\Model
 */
class Logger
{
    /**
     * Название коллекции, в которой будут храниться логи.
     *
     * @var string
     */
    protected static $collection = 'modelsLogs';

    /**
     * Логирование изменений.
     *
     * @param string $modelName Название модели.
     * @param ObjectId $modelId Идентификатор модели.
     * @param array $oldValues Старые значения.
     * @param array $newValues Новые значения.
     *
     * @return void
     */
    public static function logChanges(string $modelName, ObjectId $modelId, array $oldValues, array $newValues): void
    {
        $collection = MongoDBConnection::get()->selectCollection(self::$collection);

        $changes = [];
        // сравниваем текущие значения атрибутов с сохраненными
        foreach ($newValues as $key => $value) {
            if ($key !== '_id' && isset($oldValues[$key]) &&  $value !== $oldValues[$key]) {
                $changes[$key] = [
                    'old' => $oldValues[$key],
                    'new' => $value
                ];
            }
        }

        if (count($changes) > 0) {

            // Добавляем информацию о логе в БД
            $log = [
                'model_name' => $modelName,
                'model_id' => $modelId,
                'changes' => $changes,
                'old_values' => $oldValues,
                'new_values' => $newValues,
                'created_at' => time()
            ];

            try {
                $collection->insertOne($log);
            } catch (Exception $e) {
                // Обработка ошибки записи лога в БД
                // ...
            }
        }
    }
}
