<?php

namespace DevSpark\Engine\Model;

use DevSpark\Engine\Core;
use DevSpark\Engine\Utils;
use DevSpark\Utils\Inflect;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


trait ModelGraphQL
{



    public function getGraphQLType($customKey = null, $showFields = null)
    {



        if ($customKey) $itemName = $customKey;
        else
            $itemName = Utils::onlyLetters(ucfirst(Inflect::singularize($this->key)) . 'Item');



        if (isset(Core::$GraphQLTypes[$itemName])) {
            return Core::$GraphQLTypes[$itemName];
        } else {

            $fields = [
                '_id' => [
                    "type"        => Type::ID(),
                    "description" => "Уникальный _id записи в " . $this->title,
                ],
            ];

            foreach ($this->fields as $field) {
                if ($showFields) {
                    if (in_array($field->key, $showFields))
                        $fields[$field->key] = $field->getGraphQLField($this);
                } else {
                    $fields[$field->key] = $field->getGraphQLField($this);
                }
            }



            foreach (Core::getAllModels() as $model) {
                foreach ($model->fields as $field) {

                    if ($field->input == 'pointer' && $field->object == $this->key) {

                        if ($showFields) {
                            if (in_array($field->key, $model->key))
                                $fields[$model->key] = $model->getGraphQLQuery(false, null, $field->key);
                        } else {
                            $fields[$model->key] = $model->getGraphQLQuery(false, null, $field->key);
                        }
                    }
                }
            }






            Core::$GraphQLTypes[$itemName] = new ObjectType([
                'description' => $this->title,
                'name'        => $itemName,
                'fields'      => $fields,
            ]);


            return Core::$GraphQLTypes[$itemName];
        }
    }



    public function getGraphQLDataType()
    {



        $itemName = Utils::onlyLetters(ucfirst($this->key));



        if (isset(Core::$GraphQLTypes[$itemName . "Data"])) {
            $dataType = Core::$GraphQLTypes[$itemName . "Data"];
        } else {

            $dataType = Core::$GraphQLTypes[$itemName . "Data"] = new ObjectType([
                'description' => 'Результат запроса',
                'name'        => $itemName . "Data",
                'fields'      => function () {

                    return [
                        'data'   => Type::listOf($this->getGraphQLType()),
                        'limit'  => Type::int(),
                        'offset' => Type::int(),
                        'total'  => Type::int(),
                    ];
                },
            ]);
        }

        return  $dataType;
    }

    public function getGraphQLTypeName()
    {
        return Utils::onlyLetters(ucfirst(Inflect::singularize($this->key)) . 'Item');
    }

    public function getGraphQLFilterInput()
    {

        $itemName = Utils::onlyLetters(ucfirst($this->key));

        if (isset(Core::$GraphQLTypes[$itemName . 'Filter'])) {
            return Core::$GraphQLTypes[$itemName . 'Filter'];
        } else {

            $fields = [
                '_ids' => [
                    'type'        => Type::listOf(Type::ID()),
                    'description' => 'Фильтр по полю ' . $this->title . ', cписок _id записей для фильтрации',
                ]
            ];

            foreach ($this->fields as $field) {

                $input = $field->getGraphQLFilterField($this);
                if ($input) {
                    $fields[$field->key] = $input;
                }
            }

            if (count($fields) > 0) {
                Core::$GraphQLTypes[$itemName . 'Filter'] = new InputObjectType([
                    'name'        => $itemName . 'Filter',
                    'description' => "Фильтры для записей " . $this->title,
                    'fields'      => $fields,
                ]);

                return Core::$GraphQLTypes[$itemName . 'Filter'];
            } else {
                return null;
            }
        }
    }

    public function getGraphQLSortFields()
    {

        $itemName = Utils::onlyLetters(ucfirst($this->key) . 'FieldsEnum');

        if (isset(Core::$GraphQLTypes[$itemName])) {
            $fieldsEnum = Core::$GraphQLTypes[$itemName];
        } else {

            $values = [];
            foreach ($this->fields as $key => $value) {
                $values[$key] = ['value' => $key, 'description' => $value->title];
            }

            $fieldsEnum = Core::$GraphQLTypes[$itemName] = new EnumType([
                'name'        => $itemName,
                'description' => "Поля " . $this->title,
                'values'      => $values,
            ]);
        }

        $itemName = 'SortDirectionEnum';

        if (isset(Core::$GraphQLTypes[$itemName])) {
            $fieldDirection = Core::$GraphQLTypes[$itemName];
        } else {

            $fieldDirection = Core::$GraphQLTypes[$itemName] = new EnumType([
                'name'        => $itemName,
                'description' => "Направление сортировки",
                'values'      => [
                    "ASC"  => ["value" => "ASC", "description" => "По возрастанию (ascending)"],
                    "DESC" => ["value" => "DESC", "description" => "По убыванию (descending)"],
                ],
            ]);
        }

        $itemName = Utils::onlyLetters(ucfirst($this->key) . 'SortInput');

        if (isset(Core::$GraphQLTypes[$itemName])) {
            return Core::$GraphQLTypes[$itemName];
        } else {

            return Core::$GraphQLTypes[$itemName] = new InputObjectType([
                'description' => 'Сортировка записей в ' . $this->title,
                'name'        => $itemName,
                'fields'      => [
                    'field'     => [
                        "type"        => Type::nonNull($fieldsEnum),
                        "description" => "Поле сортировка",
                    ],
                    'direction' => [
                        "type"        => Type::nonNull($fieldDirection),
                        "description" => "Направление сортировки",
                    ],
                ],

            ]);
        }
    }


    public function getGraphQLSingleQuery()
    {



        $_this = $this;

        $result = [
            'type'        =>  $this->getGraphQLType(),
            'description' => $this->title,
            'resolve'     => function ($root, $args) use ($_this) {


                $item = $_this->find([
                    "manager" => Auth::$manager_id
                ]);
                return  $item[0] ?? null;
            },
        ];




        return $result;
    }


    public function getGraphQLQuery($withoutData = false, $rootField = null, $refFieldKey = null, $fromBuilder = null)
    {

        $_this = $this;

        $itemName = Utils::onlyLetters(ucfirst($this->key));

        //Если это одиночная запись 
        if ($this->single) {
            return $this->getGraphQLSingleQuery();
        }


        if (!$withoutData) {
            $dataType = $this->getGraphQLDataType();
        }

       
        $description = isset($fromBuilder['description']) ? $fromBuilder['description'] : $this->title;

        $result = [
            'type'        => $withoutData ? Type::listOf($this->getGraphQLType()) : $dataType,
            'description' => $description,
            'args'        => [
                "_id"    => [
                    "type"        => Type::ID(),
                    "description" => 'Получить запись по _id',
                ],
                'limit'  => ["type" => Type::int(), 'defaultValue' => 10],
                'sample' => [
                    "type" => Type::boolean(),
                    "description" => "Случайные записи из выборки",
                    "defaultValue" => false
                ],
                'sort'   => [
                    "type"        => $this->getGraphQLSortFields(),
                    "description" => "Сортировка по полю",
                ],
                'offset' => ["type" => Type::int(), 'defaultValue' => 0],
                'all'    => [
                    "type"         => Type::boolean(),
                    'defaultValue' => false,
                    "description"  => "Вернуть все записи без пагинации (при большом кол-ве данных запрос может быть не выполнен)",
                ],
                'search' => ["type" => Type::string()],
            ],
            'resolve'     => function ($root, $args) use ($_this, $withoutData, $rootField, $refFieldKey,$fromBuilder) {
                $filter = [];

                //Если это вложенное поле $rootField
                $rootIds = [];

                if(isset($fromBuilder['match'])){
                    $filter[] = [
                        $fromBuilder['match']
                    ];
                }


                if ($rootField) {



                    if (isset($root[$rootField->key])) {


                        if (get_class($root[$rootField->key]) == 'MongoDB\BSON\ObjectId') {
                            $rootIds[] = $root[$rootField->key];
                        } else if ((get_class($root[$rootField->key]) == "MongoDB\Model\BSONArray")) {



                            foreach ($root[$rootField->key] as $rootItem) {
                                $rootIds[] =  $rootItem;
                            }
                        }
                    }


                    //Если это вложеное поле и нет _ids то возвращаем null
                    if (count($rootIds) == 0) {
                        return null;
                    }

                    $filter[] = [
                        '$match' => ['_id' => ['$in' => $rootIds]]
                    ];
                }

                $sort = [];
                if (isset($args['sort'])) {

                    $sort[$args['sort']['field']] = $args['sort']["direction"] == "DESC" ? -1 : 1;
                }

                if (isset($args['filter'])) {
                    foreach ($args['filter'] as $key => $value) {

                        if ($key == '_ids') {

                            $_ids = [];

                            foreach ($value as $val) {
                                $_ids[] = mDB::id($val);
                            }

                            if (count($_ids) > 0)

                                $filter[] = [
                                    '$match' => ['_id' => ['$in' => $_ids]]
                                ];
                        } else {

                            $match = $this->getField($key)->getGraphQLMatchFilter($value);

                            if ($match)
                                $filter[] = $match;
                        }
                    }
                }




                if ($refFieldKey) {

                    array_unshift($filter, [
                        '$match' => [
                            $refFieldKey => ['$in' => [$root['_id']]]
                        ]
                    ]);
                }


                $params = [];

                if (isset($args['_id'])) {


                    $params = [
                        "fields" => [],
                        "_id"    => mDB::id($args['_id']),
                        "limit"  => 1,
                    ];
                } else {




                    $params = [
                        "fields" => [],
                        "limit"  => $args['all'] ? 0 : $args['limit'],
                        'sample' => $args['sample'] ?? false,
                        "skip"   => $args['all'] ? 0 : $args['offset'],
                        "sort"   => $sort,
                        "filter" => $filter,
                        "search" => $args['search'] ?? null,
                        "deep"   => 0,
                    ];
                }

                $result = $_this->find($params);

                if ($withoutData) {
                    return $result;
                } else {
                    $total = isset($args['_id']) ? null : $this->total([
                        "fields" => ['_id'],
                        "filter" => $filter,
                        "search" => $args['search'] ?? null,
                        "deep"   => 0,
                    ]);

                    return [
                        'data'   => $result,
                        'limit'  => $args['all'] ? 0 : $args['limit'],
                        'offset' => $args['all'] ? 0 : $args['offset'],
                        'total'  => $total,
                    ];
                }
            },
        ];

        $filter = $this->getGraphQLFilterInput();
        if ($filter) {
            $result['args']['filter'] = $filter;
        }




        return $result;
    }

    public function getGraphQLInputFields($customKey = null, $includeFields = null)
    {

        if ($customKey) $itemName = $customKey;
        else
            $itemName = Utils::onlyLetters(ucfirst($this->key) . 'FieldsInput');

        if (isset(Core::$GraphQLTypes[$itemName])) {


            $fieldsInput = Core::$GraphQLTypes[$itemName];
        } else {

            $fields = [];

            foreach ($this->fields as $field) {

                $inputField = $field->getGraphQLInputField($this);
                if ($includeFields) {
                    if (in_array($field->key, $includeFields))
                        if ($inputField) {
                            $fields[$field->key] = $inputField;
                        }
                } else {
                    if ($inputField) {
                        $fields[$field->key] = $inputField;
                    }
                }
            };

            $fieldsInput = Core::$GraphQLTypes[$itemName] = new InputObjectType([
                'description' => 'Поля для записи в ' . $this->title,
                'name'        => $itemName,
                'fields'      => $fields,

            ]);
        }

        return $fieldsInput;
    }

    public function getGraphQLMutation()
    {



        $_this = $this;
        return [
            "description" => "Создать или обновить запись в " . $this->title,
            "type"        => $this->getGraphQLType(),
            "args"        => [
                "_id"    => [
                    'type'        => Type::string(),
                    "desctiption" => "Передайте  _id записи для обновления, в противном случае будет создана новая запись в " . $this->title,
                ],
                "fields" => [
                    'type' => Type::nonNull($this->getGraphQLInputFields()),
                ],

            ],
            'resolve'     => function ($root, $args) use ($_this) {

                return $_this->insert($args['fields']);
            },
        ];
    }
}