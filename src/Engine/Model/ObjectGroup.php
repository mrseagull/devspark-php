<?php

namespace BaraBaaS\Schema;


class ObjectGroup
{

    public $title;
    public $objectKeys = [];



    public function __construct($title, array $objectKeys = [])
    {


        $this->title      = $title;
        $this->objectKeys = $objectKeys;
        return $this;
    }


    public function addObject(String $objectKey): ObjectGroup
    {
        $this->objectKeys[] = $objectKey;
        return $this;
    }

    public function getObjectsKeys()
    {



        return $this->objectKeys;
    }

    public function getJson()
    {

        $objects = [];
        foreach ($this->objectKeys as $objectKey) {
            $object = Objects::getObject($objectKey);
            if ($object && !$object->hideObject)
                $objects[] = $object->getJson();
        }

        return [
            "title" => $this->title,
            "objects" => $objects
        ];
    }
}