<?php

use DevSpark\Engine\Fields\PointerField;
use DevSpark\Engine\Fields\PasswordField;
use DevSpark\Engine\Fields\RelationField;
use DevSpark\Engine\Fields\TextField;
use DevSpark\Engine\Fields\ColorField;
use DevSpark\Engine\Fields\NumberField;
use DevSpark\Engine\Fields\RangeNumberField;
use DevSpark\Engine\Fields\BooleanField;
use DevSpark\Engine\Fields\SelectField;
use DevSpark\Engine\Fields\MSelectField;
use DevSpark\Engine\Fields\ImageField;
use DevSpark\Engine\Fields\VideoField;
use DevSpark\Engine\Fields\FileField;
use DevSpark\Engine\Fields\QRField;
use DevSpark\Engine\Fields\IFrameField;
use DevSpark\Engine\Fields\ButtonField;
use DevSpark\Engine\Fields\MFieldsField;
use DevSpark\Engine\Fields\PhoneField;
use DevSpark\Engine\Fields\DateField;
use DevSpark\Engine\Fields\RangeDateField;
use DevSpark\Engine\Fields\TimeField;
use DevSpark\Engine\Fields\RateField;
use DevSpark\Engine\Fields\TextDateTimeField;
use DevSpark\Engine\Fields\GeoPointField;
use DevSpark\Engine\Fields\GradientField;
use DevSpark\Engine\Fields\AudioField;
use DevSpark\Engine\Fields\FloatField;
use DevSpark\Engine\Fields\IntField;
use DevSpark\Engine\Fields\RangeIntField;
use DevSpark\Engine\Fields\UrlField;

class Types
{


    public static function pointer(string $key, array $options = []): PointerField
    {
        return new PointerField($key, $options);
    }

    public static function password(string $key, array $options = []): PasswordField
    {
        return new PasswordField($key, $options);
    }

    public static function relation(string $key, array $options = []): RelationField
    {
        return new RelationField($key, $options);
    }

    public static function text(string $key, array $options = []): TextField
    {
        return new TextField($key, $options);
    }

    public static function longtext(string $key, array $options = []): TextField
    {
        return new TextField($key, $options);
    }

    public static function html(string $key, array $options = []): TextField
    {
        return new TextField($key, $options);
    }

    public static function color(string $key, array $options = []): ColorField
    {
        return new ColorField($key, $options);
    }

    public static function int(string $key, array $options = []): IntField
    {
        return new IntField($key, $options);
    }

    public static function float(string $key, array $options = []): FloatField
    {
        return new FloatField($key, $options);
    }

    public static function rangeint(string $key, array $options = []): RangeIntField
    {
        return new RangeIntField($key, $options);
    }

    public static function boolean(string $key, array $options = []): BooleanField
    {
        return new BooleanField($key, $options);
    }

    public static function select(string $key, array $options = []): SelectField
    {
        return new SelectField($key, $options);
    }

    public static function mselect(string $key, array $options = []): MSelectField
    {
        return new MSelectField($key, $options);
    }

    public static function image(string $key, array $options = []): ImageField
    {
        return new ImageField($key, $options);
    }

    public static function video(string $key, array $options = []): VideoField
    {
        return new VideoField($key, $options);
    }

    public static function imagelink(string $key, array $options = []): ImageField
    {
        return new ImageField($key, $options);
    }

    public static function file(string $key, array $options = []): FileField
    {
        return new FileField($key, $options);
    }

    public static function filelink(string $key, array $options = []): FileField
    {
        return new FileField($key, $options);
    }

    public static function qr(string $key, array $options = []): QRField
    {
        return new QRField($key, $options);
    }

    public static function button(string $key, array $options = []): ButtonField
    {
        return new ButtonField($key, $options);
    }

    public static function mfields(string $key, array $options = []): MFieldsField
    {
        return new MFieldsField($key, $options);
    }

    public static function phone(string $key, array $options = []): PhoneField
    {
        return new PhoneField($key, $options);
    }

    public static function date(string $key, array $options = []): DateField
    {
        return new DateField($key, $options);
    }

    public static function datetime(string $key, array $options = []): DateField
    {
        return new DateField($key, $options);
    }

    public static function rangedatetime(string $key, array $options = []): RangeDateField
    {
        return new RangeDateField($key, $options);
    }


    public static function rangedate(string $key, array $options = []): RangeDateField
    {
        return new RangeDateField($key, $options);
    }

    public static function time(string $key, array $options = []): TimeField
    {
        return new TimeField($key, $options);
    }

    public static function rate(string $key, array $options = []): RateField
    {
        return new RateField($key, $options);
    }

    public static function textdatetime(string $key, array $options = []): TextDateTimeField
    {
        return new TextDateTimeField($key, $options);
    }

    public static function geopoint(string $key, array $options = []): GeoPointField
    {
        return new GeoPointField($key, $options);
    }

    public static function gradient(string $key, array $options = []): GradientField
    {
        return new GradientField($key, $options);
    }

    public static function audio(string $key, array $options = []): AudioField
    {
        return new AudioField($key, $options);
    }

    public static function url(string $key, array $options = []): UrlField
    {
        return new UrlField($key, $options);
    }
}
