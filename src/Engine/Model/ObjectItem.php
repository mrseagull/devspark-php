<?php

namespace BaraBaaS\Schema;


use BaraBaaS\Schema\FieldTypes\DefaultField;

use BaraBaaS\Schema\FieldTypes\PointerField;

use BaraBaaS\Schema\FieldTypes\RelationField;

use morphos\Russian\NounPluralization;

use Error;

class ObjectItem
{

    use ObjectGraphQL;
    use ObjectQuery;

    public $title;
    public $key;
    public $fields = [];

    public $single = false; //Еденичная запись

    public $icon = 'square';
    public $declension; //Склонения

    public $deep = 2;

    public $hideObject = false;

    public $buttons;

    public $stats;

    public $bladeTpl; // Blade шаблон для вывода карточкой

    public $access = [
        "update" => false,
        "add"    => false,
        "delete" => false,
    ];


    private $job; //$_id

    public $badgeFunction; //Расчет бейджей

    public $collection; //Коллекция запроса

    public $tip = null; //Подсказка

    public $modal = false; //Создание в модальном окне

    public $not_data = "";
    public $groups = [];
    public $tabs;
    public $intable;

    public $groupMenu;

    public $expandable;

    public function __construct($key, $title = null)
    {
        $this->key        = $key;
        $this->collection = $key;
        $this->title      = $title ?? $key;

        return $this;
    }


    public function getDemo($count)
    {

        $result = [];

        for ($i = 0; $i < $count; $i++) {

            $item = [
                '_id' => "demo-" . rand(10000, 20000)
            ];

            foreach ($this->fields as $field) {

                $path = explode('.', $field->key);
                if (count($path) > 1) {
                    $item[$path[0]][$path[1]] = $field->getDemoValue();
                } else {
                    $item[$field->key] = $field->getDemoValue();
                }
            }

            $result[] = $item;
        }
        return $result;
    }



    public function setSingle($single)
    {
        $this->single = $single;
        return $this;
    }

    public function setModal($modal)
    {
        $this->modal = $modal;
        return $this;
    }

    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }





    public function getStat($key): StatItem | null
    {

        $statReturn = null;
        foreach ($this->stats as $stat) {
            if ($stat->key == $key) {
                $statReturn = $stat;
                break;
            }
        }


        return $statReturn;
    }

    public function setStats($stats): ObjectItem
    {
        $this->stats = $stats;
        return $this;
    }

    public function addStats(StatItem $stat): ObjectItem
    {
        $this->stats[] = $stat;
        return $this;
    }

    public function setBladeTpl($tpl)
    {
        $this->bladeTpl = $tpl;
        return $this;
    }



    public function setDeep($deep)
    {
        $this->deep = $deep;
        return $this;
    }

    public function setHide($hideObject)
    {
        $this->hideObject = $hideObject;
        return $this;
    }

    public function setGroupMenu($name_group)
    {
        $this->groupMenu = $name_group;
        return $this;
    }

    public function setCollection($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function setGroups(array $groups): ObjectItem
    {


        $this->groups = array_merge($this->groups, $groups);
        return $this;
    }

    public function addGroup(FieldGroup $group): ObjectItem
    {



        $this->groups[]  =  $group;
        return $this;
    }
    public function setTabs($tabs): ObjectItem
    {
        if (!$tabs) {
            $this->tabs = [];
            return $this;
        } else {
            foreach ($tabs as $index => &$tab) {
                if (!isset($tab['key'])) $tab['key'] = md5($this->key . "-" . $index);
            }

            $this->tabs = $tabs;
            return $this;
        }
    }

    public function setTip($emoji, $description): ObjectItem
    {
        $this->tip = [
            "icon"        => $emoji,
            "description" => $description,
        ];
        return $this;
    }

    public function removeTip(): ObjectItem
    {
        $this->tip = null;
        return $this;
    }

    public function setBadgeFunction($badgeFunction)
    {
        $this->badgeFunction = $badgeFunction;
        return $this;
    }

    public function setJob($job)
    {
        $this->badgeFunction = $job;
        return $this;
    }



    public function setTitle($title): ObjectItem
    {
        $this->title = $title;
        return $this;
    }

    public function setOnlyShowForAllFields(): ObjectItem
    {
        foreach ($this->fields as $field) {
            $field->onlyshow = true;
        }
        return $this;
    }

    public function setAccess($access): ObjectItem
    {
        $this->access = [

            'update' => $access['update'] ?? $this->access['update'],
            'add'    => $access['add'] ?? $this->access['add'],
            'delete' => $access['delete'] ?? $this->access['delete'],
        ];


        return $this;
    }

    public function addButton($button): ObjectItem
    {
        $this->buttons[$button->key] = $button;
        return $this;
    }

    //Add or Replace field
    public function addField($field): ObjectItem
    {

        $this->fields[$field->key] = $field;
        return $this;
    }

    //Add or Replace fields []
    public function addFields($fields, $intable = []): ObjectItem
    {
        foreach ($fields as $field) {
            $this->fields[$field->key] = $field;
        }

        $this->setInTable($intable);
        return $this;
    }

    public function getIcon(){
      return  Icons::getIcon($this->icon);
    }

    public function getField($key): DefaultField | PointerField | RelationField | null
    {

        return $this->fields[$key] ?? null;
    }

    //All keys fields in object
    public function getFieldsKey(): array
    {
        $keys = [];
        foreach ($this->fields as $key => $field) {
            $keys[] = $key;
        }
        return $keys;
    }

    //Get All fields with default or null values without onlyshow
    public function getFieldsValues($withoutOnlyShow = false): array
    {
        $values = [];
        foreach ($this->fields as $key => $field) {
            if (!$withoutOnlyShow && $field->onlyshow) {
                continue;
            }
            $values[$field->key] = $field->getNull();
        }
        return $values;
    }

    public function getFields(array $keys): array
    {
        $fields = [];

        foreach ($keys as $key) {
            if (isset($this->fields[$key])) {
                $fields[] = $this->fields[$key];
            }
        }

        return $fields;
    }

    public function removeFields($keys): ObjectItem
    {
        foreach ($keys as $key) {
            if (isset($this->fields[$key])) {
                unset($this->fields[$key]);
            }
        }

        return $this;
    }

    public function getInTable(): array
    {

        if ($this->intable && count($this->intable) > 0) {
            return $this->intable;
        } else {

            return  $this->getFieldsKey();
        }
    }

    public function setInTable($intable): ObjectItem
    {
        if (count($intable) > 0) {
            $this->intable = [];
            foreach ($intable as $value) {
                if (in_array($value, array_keys($this->fields))) {
                    $this->intable[] = $value;
                }
            }
        }
        return $this;
    }

    public function addInTable($intableField): ObjectItem
    {

        if (in_array($intableField, array_keys($this->fields))) {
            $this->intable[] = $intableField;
        }
        return $this;
    }

    public function updateField($field): ObjectItem
    {

        $this->fields[$field->key] = $field;
        return $this;
    }

    public function setExpandable($params): ObjectItem
    {

        if (!$params['from'] || !$params['localField'] || !$params['foreignField']) {

            new Error("For expandable don't found all need keys");
        }



        $this->expandable = $params;
        return $this;
    }




    public function getJson()
    {

        $buttons = [];

        if ($this->buttons) {
            foreach ($this->buttons as $button) {
                $buttons[$button->key] = $button->getJson();
            }
        }

        if (count($buttons) == 0) {
            $buttons = null;
        }

        if (!$this->declension) {

            if ($this->title != $this->key) {
                $declension = NounPluralization::getCases($this->title);
            }
        } else {
            $declension = $this->declension;
        }

        $groups = [];

        $used_keys = [];
        foreach ($this->groups as $group) {
            $groups[] = $group->getJson($this);
            $used_keys = array_merge($used_keys, $group->getFieldsKeys());
        }




        $other_fields = [];

        foreach ($this->getFieldsKey() as $fieldKey) {
            if (!in_array($fieldKey, $used_keys)) {
                $other_fields[] = $fieldKey;
            }
        }



        if (count($other_fields) > 0) {
            $groups = [(new FieldGroup("main", [
                "title" => "Общее",
                "fields" => $other_fields
            ]))->getJson($this), ...$groups];
        }

        $stats = null;
        if ($this->stats && count($this->stats) > 0) {


            foreach ($this->stats as $key => $stat) {
                if (!$stat->hideStat) {
                    $stats[] = $stat->getJson();
                }
            }
        }




        $result = [
            "tip"        => $this->tip,
            "icon" =>    Icons::getIcon($this->icon),
            "access"     => $this->access,
            "intable"    => $this->intable,
            "groups"     => $groups,
            "title"      => $this->title,
            "key"        => $this->key,
            "fields"     => [],
            "buttons"    => $buttons,
            "modal"      => $this->modal,
            "not_data"   => $this->not_data,
            "tabs"       => $this->tabs,
            "cardMode" => $this->bladeTpl ? true : false,
            "expandable" => $this->expandable,
            "declension" => $declension,
            "filters"    => [],
            "stats" => $stats,
            "single" => $this->single,
            "hideObject" => $this->hideObject,
        ];

        $filedsUsedInTabs = [];
        if (isset($this->tabs)) {
            foreach ($this->tabs as $tab) {
                if (isset($tab['filter'])) {
                    $filedsUsedInTabs = array_merge($filedsUsedInTabs, array_keys($tab['filter']));
                }
            }
        }


        foreach ($this->fields as $field) {





            $result['fields'][$field->key] = $field->getJson();

            if ($this->access['update'] == false && $this->access['add'] == false) {
                $result['fields'][$field->key]['onlyshow'] = true;
            }




            if (!in_array($field->key, $filedsUsedInTabs)) {
                $filterField = $field->getFilterField();
                if ($filterField) {
                    $result['filters'][$field->key] = array_merge(["originalInput" => $field->input], $filterField->getJson());
                }
            }
        }

        return $result;
    }

    public function getFilterFields()
    {




        $filedsUsedInTabs = [];
        if (isset($this->tabs)) {
            foreach ($this->tabs as $tab) {
                if (isset($tab['filter'])) {
                    $filedsUsedInTabs = array_merge($filedsUsedInTabs, array_keys($tab['filter']));
                }
            }
        }

        $filterFields = [];

        foreach ($this->fields as $field) {




            if (!in_array($field->key, $filedsUsedInTabs)) {
                $filterField = $field->getFilterField();
                if ($filterField) {
                    $filterFields[] = $filterField;
                }
            }
        }

        return  $filterFields;
    }


    public function getGroups()
    {




        $used_keys = [];
        foreach ($this->groups as $group) {
            $used_keys = array_merge($used_keys, $group->getFieldsKeys());
        }


        $other_fields = [];

        foreach ($this->getFieldsKey() as $fieldKey) {
            if (!in_array($fieldKey, $used_keys)) {
                $other_fields[] = $fieldKey;
            }
        }



        if (count($other_fields) > 0) {
            return [(new FieldGroup("main", [
                "title" => "Общее",
                "fields" => $other_fields
            ])), ...$this->groups];
        } else {
            return $this->groups;
        }
    }

    public function getGroupWithField($fieldKey): FieldGroup | null
    {


        $groupReturn = null;
        foreach ($this->groups as $group) {
            if (in_array($fieldKey, $group->fieldsKeys)) {
                $groupReturn = $group;
                break;
            }
        }
        return $groupReturn;
    }
}
