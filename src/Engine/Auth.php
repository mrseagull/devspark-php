<?php

namespace DevSpark\Engine;

use DevSpark\Utils\JWTEncoder;

/**
 * Class Auth
 * @package DevSpark\Engine
 *
 * Класс авторизации.
 */
class Auth
{
    /**
     * @var object|null Авторизованный пользователь.
     */
    private static $authenticatedUser = null;

    /**
     * @var string|null Модель авторизованного пользователя.
     */
    private static $authenticatedModel = null;

    /**
     * Авторизация пользователя по JWT токену.
     *
     * @return bool Результат авторизации.
     */
    public static function authenticate()
    {
        // Проверяем, есть ли JWT токен в заголовках или параметрах
        $token = null;
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $token = $_SERVER['HTTP_AUTHORIZATION'];
        } else if (isset($_REQUEST['token'])) {
            $token = $_REQUEST['token'];
        }

        if (!$token) {
            // Если токена нет, то авторизация не произведена
            return false;
        }

        // Декодируем JWT токен
        $secretKey = 'my_secret_key'; // секретный ключ для декодирования токена
        $decoded = JWTEncoder::decode($token);

        // Получаем _id авторизованного пользователя
        $userId = $decoded['sub'] ?? null;

        if (!$userId) return;

        // Проходим по всем моделям, чтобы найти ту, которая поддерживает авторизацию
        $modelsDir = 'Models';
        $models = scandir($modelsDir);
        foreach ($models as $modelFile) {
            if (!is_file($modelsDir . '/' . $modelFile)) {
                continue;
            }

            // Получаем имя класса модели
            $modelClass = pathinfo($modelFile, PATHINFO_FILENAME);
            $modelNamespace = 'Models\\' . $modelClass;

            // Проверяем, поддерживает ли модель авторизацию
            if (!property_exists($modelNamespace, 'isAuthRequired') || !$modelNamespace::$isAuthRequired) {
                continue;
            }

            // Ищем пользователя по _id в модели
            $user = $modelNamespace::findOne(array('_id' => $userId));
            if ($user) {
                // Если пользователя нашли, то сохраняем его в переменную класса и запоминаем модель
                self::$authenticatedUser = $user;
                self::$authenticatedModel = $modelNamespace;
                return true;
            }
        }

        // Если не нашли пользователя в ни одной модели, то авторизация не произведена
        return false;
    }

    /**
     * Получение авторизованного пользователя.
     *
     * @return object|null Авторизованный пользователь.
     */
    public static function getAuthenticatedUser()
    {
        return self::$authenticatedUser;
    }

    /**
     * Получение модели авторизованного пользователя.
     *
     * @return string|null Модель авторизованного пользователя.
     */
    public static function getAuthenticatedModel()
    {
        return self::$authenticatedModel;
    }


    public static function isDemo(){

        if (isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'demo.') === 0) {
            // Сайт был открыт на поддомене demo
            return true;
        }else{
            return false;
        }
    }

    public static function getPassword($password)
    {
        return md5($password);
    }

}
