<?php


namespace DevSpark\Engine\Chat;


use Exception;

class Chat
{

    public static function send($chat, $text, $from)
    {

        mDB::collection("messages")->insertOne([
            "chat" => mDB::id($chat),
            "text" => $text,
            "from" => mDB::id($from)
        ]);


        $chatData =  mDB::collection("chats")->findOne([
            "_id" => mDB::id($chat)
        ]);

        $unread =  $chatData->unread ?? [];

        $setUnread = [];
        foreach ($chatData->members as $memberId) {
            $setUnread[(string)$memberId] =  ($unread[(string)$memberId] ?? 0) + 1;
        }
        $setUnread[(string)$from] = 0;

        mDB::collection("chats")->updateOne([
            "_id" => mDB::id($chat)
        ], [
            '$set' => [
                "unread" => $setUnread,
            ]
        ]);
    }

    public static function deleteMessage($message_id)
    {

        mDB::collection("messages")->updateOne([
            "_id" => mDB::id($message_id)
        ], [
            '$set' => [
                "deleted_at" => time()
            ]
        ]);
    }


    public static function create($members, $title = null)
    {

        $members = array_unique(array_values($members));

        foreach ($members as &$member) {
            $member = mDB::id($member);
        }


        mDB::collection("chats")->insertOne([
            'title' => $title,
            'members' => $members,
        ]);
    }


    public static function supportCreate($members, $title = null)
    {

        $members = array_unique(array_values($members));

        foreach ($members as &$member) {
            $member = mDB::id($member);
        }


        $chat = mDB::collection("chats")->insertOne([
            'title' => $title,
            'members' => $members,
            "support" => true,
            "open" => false
        ]);
        return $chat->getInsertedId();
    }

    public static function addMember($chatid, $member)
    {

        mDB::collection("chats")->updateOne([
            "_id" => mDB::id($chatid),
        ], [
            '$addToSet' => [
                "members" => $member
            ]
        ]);
    }

    public static function removeMember($chatid, $member)
    {

        mDB::collection("chats")->updateOne([
            "_id" => mDB::id($chatid),
        ], [
            '$pull' => [
                "members" => $member
            ]
        ]);
    }

    public static function sendToSupportChat($member, $text)
    {

        $supportChatId = self::getSupportChat($member);


        self::send($supportChatId, $text, $member);

        mDB::collection("chats")->updateOne([
            "_id" => $supportChatId
        ], [
            '$set' => [
                "open" => true
            ]
        ]);
    }

    public static function getSupportChat($member)
    {
        $chat =  mDB::collection("chats")->findOne([
            "support" => true,
            "members" => ['$all' => [mDB::id($member), Auth::$manager_id]],
        ]);

        if (!$chat) {
            return self::supportCreate([$member, Auth::$manager_id]);
        } else {


            return $chat->_id;
        }
    }

    public static function cleanUnread($chat, $member)
    {

        mDB::collection("chats")->updateOne([
            "_id" => mDB::id($chat)
        ], [
            '$set' => [
                "unread." . ((string) $member) => 0
            ]
        ]);
    }

    public static function supportClose($chat)
    {


        mDB::collection("chats")->updateOne([
            "_id" => mDB::id($chat)
        ], [
            '$set' => [
                "open" => false
            ]
        ]);
    }

    public static function getMembers($ids)
    {

        foreach ($ids as &$id) {
            $id = mDB::id($id);
        }

        $result = [];

        $users =   mDB::collection("users")->find([
            "_id" => ['$in' => $ids]
        ]);

        $users =   mDB::collection("users")->find([
            "_id" => ['$in' => $ids]
        ]);
    }
}
