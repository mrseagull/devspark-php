<?php


namespace DevSpark\Engine\Knowledgebase;

use Aws\S3\S3Client;
use DevSpark\Engine\Core;
use DevSpark\Engine\Config;
use DevSpark\Utils\Icons;
use DOMDocument;
use DOMXPath;
use PHPMailer\PHPMailer\PHPMailer;


class Knowledgebase
{


    public static $url;

    public static $lang;


    public static function setLang($lang)
    {

        if (in_array($lang, ['ru', 'en', 'de'])) {

            self::$lang = $lang;
            return true;
        } else {

            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            if (in_array($lang, ['ru', 'en', 'de'])) {
                self::$lang = $lang;
            } else {
                self::$lang = 'ru';
            }


            return false;
        }
    }

    public static function updateObjectsArticles()
    {




        $models = Core::getAllModels();


        $countIndex = 0;
        foreach ($models as $model) {


            if ($model->hide) {

                mDB::collection("KB_Сategories")->deleteOne([
                    "key" => $model->key,
                ]);

                continue;
            }





            if ($model->single) {
             
                $groups = $model->getGroups();

                $groupCount = count($groups);
                $statsCount = count($model->stats ?? []);


                $description = $model->tip['description'] ?? null;

                if(!$description){


                    if($groupCount > 0 && $statsCount == 0){
                        $description = "Пошаговые инструкции этого раздела помогут быстро настроить систему с учетом 
                        конкретных потребностей и задач, которые она должна выполнять. Это позволит 
                        системе работать максимально эффективно и безопасно, а также избежать проблем, 
                        связанных с ее функционированием.";
                    }


                    if($statsCount > 0 && $groupCount == 0){
                        $description = "Графики и таблицы позволяют наглядно представить данные, 
                        что помогает быстрее и легче понять информацию. Например, график может показать динамику 
                        изменения в течение периода, а таблица может быстро сравнить показатели.";
                    }



                    if($statsCount > 0 && $groupCount > 0){
                        $description = "Раздел совмещает аналитику данных и настройку. Пошаговые инструкции этого раздела помогут быстро настроить систему с учетом 
                        конкретных потребностей и задач, а графики и таблицы позволяют наглядно представить данные, 
                        что помогает быстрее и легче понять информацию. ";
                    }

                }

                mDB::collection("KB_Сategories")->updateOne(
                    [
                        "key" => $model->key,
                    ],
                    ['$set' => [
                        "key" => $model->key,
                        "title" => ['ru' => $model->title],
                        "description" => ['ru' =>
                        $description
                        ],
                        "icon" => Icons::getIcon($model->icon)
                    ]],
                    ['upsert' => true]
                );


            

                $groupKeys = [];

                foreach ($groups as $group) {
                    $groupKeys[] = $group->key;
                }

                if($model->stats)
                foreach ($model->stats as $stats) {
                    $groupKeys[] = $stats->key;
                }


                mDB::_collection("KB_Articles")->deleteMany([
                    "category" => $model->key,
                    "key" => ['$nin' => $groupKeys]
                ]);

                foreach ($groups as $group) {



                    $article = [
                        "title" => ['ru' => $model->title . " - " . $group->title],
                        "category" => $model->key,
                        "key" => $group->key,
                        "content" =>  ['ru' =>  (string) view('barabaas::knowledgebase.article.single', ['group' => $group, 'model' => $model])]
                    ];


               
                    $hash = md5($article['title']['ru'] . $article['content']['ru']);
                    $article['hash'] = $hash;

                    $before = mDB::collection("KB_Articles")->findOne(
                        [
                            "category" => $model->key,
                            "key" => $group->key,
                            'hash' => $hash
                        ]
                    );

                    if (!$before)
                        mDB::collection("KB_Articles")->updateOne(
                            [
                                "key" => $group->key,
                                "category" => $model->key,
                            ],
                            ['$set' => $article],
                            ['upsert' => true]
                        );
                }


              
                if($model->stats)
                foreach ($model->stats as $stats) {



                    $article = [
                        "title" => ['ru' => $model->title . " - " .  $stats->title],
                        "category" => $model->key,
                        "key" => $stats->key,
                        "content" =>  ['ru' =>  (string) view('barabaas::knowledgebase.article.single', ['stats' => $stats, 'object' => $model])]
                    ];


                  
                    $hash = md5($article['title']['ru'] . $article['content']['ru']);
                    $article['hash'] = $hash;

                    $before = mDB::collection("KB_Articles")->findOne(
                        [
                            "category" => $model->key,
                            "key" => $stats->key,
                            'hash' => $hash
                        ]
                    );

                    if (!$before)
                        mDB::collection("KB_Articles")->updateOne(
                            [
                                "key" => $stats->key,
                                "category" => $model->key,
                            ],
                            ['$set' => $article],
                            ['upsert' => true]
                        );
                }



                continue;
            }




            $description = $model->tip['description'] ?? null;

            mDB::collection("KB_Сategories")->updateOne(
                [
                    "key" => $model->key,
                ],
                ['$set' => [
                    "key" => $model->key,
                    "title" => ['ru' => $model->title],
                    "description" => ['ru' => $description ??  "В этой категории мы рассмотрим, как работать с разделом \"" . $model->title . "\" в административной панели " . Config::$title . ". 
                       В этом разделе вы можете получить полный доступ к деталям записей."
                        . ($model->access['add'] ? "Вы сможете просматривать, редактировать и удалять данные, а также создавать новые записи." : "")],
                    "icon" => Icons::getIcon($model->icon)
                ]],
                ['upsert' => true]
            );


            $article = [
                "title" => ['ru' => $model->title . " - работа с данными"],
                "category" => $model->key,
                "key" => "table",
                "content" => ['ru' => (string) view('barabaas::knowledgebase.article.table', ['key' => $model->key, 'countIndex' => $countIndex])]
            ];

            $hash = md5($article['title']['ru'] . $article['content']['ru']);
            $article['hash'] = $hash;

            $before = mDB::collection("KB_Articles")->findOne(
                [
                    "category" => $model->key,
                    "key" => "table",
                    'hash' => $hash
                ]
            );

            if (!$before)
                mDB::collection("KB_Articles")->updateOne(
                    [
                        "key" => "table",
                        "category" => $model->key,
                    ],
                    ['$set' => $article],
                    ['upsert' => true]
                );




            $article = [
                "title" => ['ru' => $model->title . ($model->access['update'] ? " - просмотр и редактирование" : " - просмотр")],
                "category" => $model->key,
                "key" => "view",
                "content" => ['ru' => (string) view('barabaas::knowledgebase.article.show', ['key' => $model->key, 'countIndex' => $countIndex])]
            ];


            $hash = md5($article['title']['ru'] . $article['content']['ru']);
            $article['hash'] = $hash;


            $before = mDB::collection("KB_Articles")->findOne(
                [
                    "category" => $model->key,
                    "key" => "view",
                    'hash' => $hash
                ]
            );

            if (!$before)
                mDB::collection("KB_Articles")->updateOne(
                    [
                        "category" => $model->key,
                        "key" => "view",
                    ],
                    ['$set' => $article],
                    ['upsert' => true]
                );



            if ($model->access['add']) {
                $article = [
                    "title" => ['ru' => $model->title . " - создание записи"],
                    "category" => $model->key,
                    "key" => "add",
                    "content" => ['ru' => (string) view('barabaas::knowledgebase.article.add', ['key' => $model->key, 'countIndex' => $countIndex])]
                ];


                $hash = md5($article['title']['ru'] . $article['content']['ru']);
                $article['hash'] = $hash;

                $before = mDB::collection("KB_Articles")->findOne(
                    [
                        "category" => $model->key,
                        "key" => "add",
                        'hash' => $hash
                    ]
                );

                if (!$before)
                    mDB::collection("KB_Articles")->updateOne(
                        [
                            "category" => $model->key,
                            "key" => "add",
                        ],
                        ['$set' => $article],
                        ['upsert' => true]
                    );
            } else {
                mDB::collection("KB_Articles")->deleteOne([
                    "category" => $model->key,
                    "key" => "add",
                ]);
            }
            $countIndex++;
        }
    }

    public static function updateStaticArticles()
    {






        mDB::collection("KB_Сategories")->updateOne(
            [
                "key" => "start"
            ],
            ['$set' => [
                "key" => "start",
                "title" => ['ru' => "Начало работы"],
                "description" => ['ru' => "Начните изучать " . Config::$title . ". Войдите в аккаунт и настройте его для удобной работы с системой."],
                "icon" => Icons::getIcon("dock-window")
            ]],
            ['upsert' => true]
        );


        if (Config::$socAuth || Config::$emailAuth) {


            $article = [
                "title" => ['ru' => "Как войти в админ-панель"],
                "category" => "start",
                "description" => ["ru" => "Админ. панель " . Config::$title . " можно быстро и легко установить в виде приложения (PWA) на свой смартфон. "],
                "key" => "login",
                "content" => ['ru' => (string) view('barabaas::knowledgebase.static.login')]
            ];

            mDB::collection("KB_Articles")->updateOne(
                [
                    "category" => "start",
                    "key" => "login",
                ],
                ['$set' => $article],
                ['upsert' => true]
            );
        }





        $article = [
            "title" => ['ru' => "Как установить админ. панель на телефон (PWA)"],
            "category" => "start",
            "key" => "pwa",
            "content" => ['ru' => (string) view('barabaas::knowledgebase.static.pwa')]
        ];

        mDB::collection("KB_Articles")->updateOne(
            [
                "category" => "start",
                "key" => "pwa",
            ],
            ['$set' => $article],
            ['upsert' => true]
        );
    }

    public static function update()
    {


        self::updateStaticArticles();
        self::updateObjectsArticles();


        echo 'done';
    }

    public static function getCategoryUrl($category)
    {
        return '/' . $category->key;
    }

    public static function getArticleUrl($article)
    {
        return '/' . $article->category . '/' . $article->key;
    }


    public static function getCategories()
    {
        $categories =  mDB::_collection("KB_Сategories")->find([]);

        $result = [];



        foreach ($categories as $category) {

            $category = self::setCategoryLangValues($category);

            $category->url = self::getCategoryUrl($category);
            $result[] = $category;
        }

        return  $result;
    }

    public static function getCategory($key)
    {
        $category =  mDB::_collection("KB_Сategories")->findOne(['key' => $key]);
        if ($category) {
            $category = self::setCategoryLangValues($category);
            $category->url = self::getCategoryUrl($category);
        }
        return  $category;
    }



    public static function getArticle($category, $key)
    {
        $article =  mDB::_collection("KB_Articles")->findOne([
            'category' => $category,
            'key' => $key
        ]);

        if ($article) {
            $article->prev = mDB::_collection("KB_Articles")->findOne([
                "_id" => ['$lt' => $article->_id]
            ]);
            if ($article->prev) {
                $article->prev = self::setArticleLangValues($article->prev);

                $article->prev->url = self::getArticleUrl($article->prev);
            }
            $article->next = mDB::_collection("KB_Articles")->findOne([
                "_id" => ['$gt' => $article->_id]
            ]);

            if ($article->next) {
                $article->next = self::setArticleLangValues($article->next);

                $article->next->url = self::getArticleUrl($article->next);
            }


            $article = self::setArticleLangValues($article);


            $article->url =  self::getArticleUrl($article);
        }
        return $article;
    }


    private static function setCategoryLangValues($category)
    {

        if (isset($category->title[self::$lang]) && isset($category->description[self::$lang])) {
            $category->title = $category->title[self::$lang] ?? null;
            $category->description = $category->description[self::$lang] ?? null;
        } else {

            $title =   self::translate($category->title['ru']);
            $description =   self::translate($category->description['ru']);
            mDB::_collection("KB_Сategories")->updateOne([
                "_id" => $category->_id
            ], [
                '$set' => [
                    "title." . self::$lang => $title,
                    "description." . self::$lang => $description,
                ]
            ]);
            $category->title = $title;
            $category->description = $description;
        }
        return $category;
    }



    private static function setArticleLangValues($article)
    {

        if (isset($article->title[self::$lang]) && isset($article->content[self::$lang])) {
            $article->title = $article->title[self::$lang] ?? null;
            $article->content = $article->content[self::$lang] ?? null;
        } else {

            $title =   self::translate($article->title['ru']);
            $content =   self::translate($article->content['ru']);

            mDB::_collection("KB_Articles")->updateOne([
                "_id" => $article->_id
            ], [
                '$set' => [
                    "title." . self::$lang => $title,
                    "content." . self::$lang => $content,
                ]
            ]);
            $article->title = $title;
            $article->content = $content;
        }
        return $article;
    }


    public static function Lang($text)
    {

        $hash = md5($text);

        $item = mDB::_collection("KB_Localization")->findOne([
            "hash" => $hash,
        ]);

        if (!$item) {
            mDB::_collection("KB_Localization")->insertOne([
                "hash" => $hash,
                "ru" => $text
            ]);

            $item = mDB::_collection("KB_Localization")->findOne([
                "hash" => $hash,
            ]);
        }


        if (isset($item[self::$lang])) {
            return $item[self::$lang];
        } else {

            $string =   self::translate($text);


            mDB::_collection("KB_Localization")->updateOne([
                "hash" => $hash,
            ], [
                '$set' => [
                    self::$lang => $string

                ]
            ]);
            return $string;
        }
    }

    private static function translate($text)
    {


        $payload = json_encode([
            "text" => $text,
            "targetLanguage" => self::$lang,
            "currentLanguage" => 'ru',
            "debug" => true
        ]);

        $ch = curl_init("https://translator.apptor.tech/api/translate");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($result) {

            $res = json_decode($result, true);

            return $res['text'];
        } else {
            return "NONE";
        }
    }

    public static function getAsset($file, $encoding)
    {

        $path = Core::$packagePath.'/storage/';

        if (file_exists(base_path($path . $file . '.' . $encoding))) {
            header("Content-type: image/jpeg");
            echo  file_get_contents(base_path($path . $file . '.' . $encoding));
            exit;
        }
    }
    public static function getArticles($key)
    {
        $articles =  mDB::_collection("KB_Articles")->find([
            'category' => $key
        ]);

        $result = [];
        foreach ($articles as $article) {
            $article = self::setArticleLangValues($article);
            $article->url =  self::getArticleUrl($article);
            $result[] = $article;
        }

        return $result;
    }


    public static function convertFilterToDescription($filter)
    {

        /*Данный код принимает фильтр запроса к MongoDB и преобразует его в словесное описание. Для этого используется рекурсивная функция convertFilterToDescription, которая перебирает все поля и значения фильтра и в зависимости от типа поля генерирует соответствующее словесное описание.

        Пример использования кода:

        ```
        $filter = [
        'name' => 'John',
        'age' => [ '$gte' => 18 ],
        '$or' => [
            [ 'city' => 'Moscow' ],
            [ 'city' => 'St. Petersburg' ]
        ]
        ];

        $description = convertFilterToDescription($filter);

        echo $description; // Выводит: name = John и age больше или равно 18 и (city = Moscow или city = St. Petersburg)

        ```*/
        $description = '';

        foreach ($filter as $field => $value) {
            if (is_array($value)) {
                $description .= self::convertFilterToDescription($value);
            } else {
                switch ($field) {
                    case '$and':
                        $description .= ' и ';
                        $description .= self::convertFilterToDescription($value);
                        break;
                    case '$or':
                        $description .= ' или ';
                        $description .= self::convertFilterToDescription($value);
                        break;
                    case '$eq':
                        $description .= $value;
                        break;
                    case '$ne':
                        $description .= 'не равно ' . $value;
                        break;
                    case '$gt':
                        $description .= 'больше чем ' . $value;
                        break;
                    case '$gte':
                        $description .= 'больше или равно ' . $value;
                        break;
                    case '$lt':
                        $description .= 'меньше чем ' . $value;
                        break;
                    case '$lte':
                        $description .= 'меньше или равно ' . $value;
                        break;
                    case '$in':
                        $description .= 'входит в ' . implode(', ', $value);
                        break;
                    case '$nin':
                        $description .= 'не входит в ' . implode(', ', $value);
                        break;
                    case '$exists':
                        $description .= 'существует';
                        break;
                    case '$regex':
                        $description .= 'содержит ' . $value;
                        break;
                    default:
                        $description .= $field . ' = ' . $value;
                        break;
                }
            }
        }

        return $description;
    }
}
