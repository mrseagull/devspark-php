<?php

namespace DevSpark\Engine;


use MongoDB\Client;
use MongoDB\Database;

class MongoDBConnection
{

    protected static $client;
    public static $database;

    public static $queryCount = 0;


    public static function connect()
    {

        $params = [];
        if (env('MONGODB_USERNAME')) {
            $params = [
                "username"         => env('MONGODB_USERNAME'),
                "authSource"       => env('MONGODB_AUTHSOURCE', "admin"),
                "password"         => env('MONGODB_PASSWORD'),
                "connectTimeoutMS" => 360000,
                "socketTimeoutMS"  => 360000,
                'poolSize'         => 1000,
                'ssl' => false,
            ];
        }



        $server = 'mongodb://' . env('MONGODB_HOST', "localhost") . ":" . env("MONGODB_PORT", 27017);

        $db  = env('MONGODB_DATABASE');


        self::$client = new Client($server, $params);

        self::$database = self::$client->$db;
    }


    


    public static function get(): Database
    {

        if (!self::$database) {
            self::connect();
        }

        return self::$database;
    }
}
