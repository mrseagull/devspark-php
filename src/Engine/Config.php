<?php


namespace DevSpark\Engine;


class Config
{

   
    public static $title    = "Без названия";
    public static $subtitle = "";
    public static $logo     = "https://random.imagecdn.app/300/300";
    public static $cover    = "https://random.imagecdn.app/1024/1024";
    public static $color    = "#000";

    //Register setting
    public static $emailReg = false;
    public static $socReg   = false;

    public static $emailAuth = false;
    public static $socAuth   = false;

    public static $balance = false;

   
    public static $support = false;
   
   
    public static $objectGroups = [];

   
    //Глабальный параметры для выборки и вставки
    public static $globalSplitParams = [];

   


}