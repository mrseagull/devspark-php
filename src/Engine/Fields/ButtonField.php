<?php

namespace DevSpark\Engine\Fields;

class ButtonField extends DefaultField
{
    public $input = "button";
    public $onlyshow = true;
    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }
}
