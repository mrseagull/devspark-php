<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;

class DateTimeField extends DefaultField
{
    public $input = "datetime";

    public $width = 100;

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return $val > 0 ?
            date("d.m.Y H:i:s", $val)
            : "";
    }

    public function getInputDescription(){
        return "Дата и время";
    }

    public function getDemoValue()
    {
        return  time() + rand(-604800, 604800);
    }

    public function getTpl($item, $key)
    {

        $val = $item[$this->key] ?? null;
        if ($val > 0) return (string) view('barabaas::' . $this->input, ["val" => $val]);
    }

    public function getFilterField()
    {

        return new RangeDateField($this->key, [
            "title" => $this->title,
        ]);
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return Type::Float();
    }

    public function getGraphQLMatchFilter($value = null)
    {
        return [
            '$match' => [$this->key => [
                '$gte' => $value['gte'],
                '$lte' => $value['lte'],
            ]]

        ];
    }

    public function getGraphQLFilterField($objectItem = null)
    {

        if (isset(Core::$GraphQLTypes['RangeDateInputType'])) {
            $RangeDateInputType = Core::$GraphQLTypes['RangeDateInputType'];
        } else {

            $RangeDateInputType = Core::$GraphQLTypes['RangeDateInputType'] = new InputObjectType([
                'description' => 'Диапазон дат',
                'name'        => 'RangeDateInputType',
                'fields'      => function () {
                    return [
                        'gte' => [
                            "type"        => Type::float(),
                            "description" => "Больше или равно - greater than or equal (логическое условие >=) ",
                        ],
                        'lte' => [
                            "type"        => Type::float(),
                            "description" => "Меньше или равно - less than or equal (логическое условие >=) ",
                        ],
                    ];
                },
            ]);
        }

        return [
            'type'        => $RangeDateInputType,
            'description' => 'Фильтр по полю ' . $this->title,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::Float()) : Type::Float(),
            'description' => $this->title . ' (Дата и время UNIX)',
        ];
    }
}
