<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class RangeDateTimeField extends DefaultField
{
    public $input = "rangedatetime";

    public function getExportValue($values)
    {


        $val = $values[$this->key] ?? null;

        return (isset($val['from']) && isset($val['to'])) ?
            date("d.m.Y H:i", $val['from']) . ' - ' . date("d.m.Y H:i", $val['to'])
            : "";
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }


    public function getInputDescription(){
       
        return "Диапазон дат и времени от и до включительно";
    }

    public function getDemoValue()
    {
        $date = time() + rand(-604800, 604800);
        return  [$date, $date + rand(-604800, 604800)];
    }


    public function getGraphQLType()
    {
        return Type::listOf(Type::float());
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::listOf(Type::float())) : Type::listOf(Type::float()),
            'description' => $this->title,
        ];
    }
}
