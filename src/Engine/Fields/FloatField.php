<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;

class FloatField extends DefaultField
{
    public $input = "float";

    public $max = null;
    public $min = null;

    public $width = 100;

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);

        $this->max = $params['max'] ?? null;
        $this->min = $params['min'] ?? null;
    }

    public function getInputDescription()
    {
        return  "Это поле отображается " . (!$this->onlyshow ? " и доступно для редактирования " : "") . " в числовом формате, 
        то есть здесь выводится конкретное число, 
        которое соответствует значению \"" . $this->title . "\".";
    }


    public function getDemoValue()
    {
        return rand(($this->max ?? 100) * 100, ($this->min ?? 200) * 100) / 100;
    }

    public function isEmpty($value)
    {
        return !is_numeric($value);
    }

    public function getFilterField()
    {

        return new RangeIntField($this->key, [
            "title" => $this->title,
        ]);
    }
    public function getJsonParams()
    {
        return [
            "max" => $this->max,
            "min" => $this->min,
        ];
    }

    public function getGraphQLType()
    {
        return Type::Float();
    }

    public function getGraphQLMatchFilter($value = null)
    {

        if (isset($value['or']) && count($value['or']) > 0) {

            $result = [];
            foreach ($value['or'] as $value) {

                $val = [];
                if ($value['gte']) $val['$gte'] =  $value['gte'];
                if ($value['lte']) $val['$lte'] =  $value['lte'];
                if (count($val) > 0)
                    $result[] =  [$this->key => $val];
            }
            if (count($result) > 0)
                return [
                    '$match' => [
                        '$or' => $result
                    ]
                ];
            else return null;
        } else {

            $val = [];
            if ($value['gte']) $val['$gte'] =  $value['gte'];
            if ($value['lte']) $val['$lte'] =  $value['lte'];
            if (count($val) > 0)
                return ['$match' => [$this->key => $val]];
            else return null;
        }
    }


    public function getGraphQLFilterField($objectItem = null)
    {

        if (isset(Core::$GraphQLTypes['RangeFloatType'])) {
            $RangeFloatType = Core::$GraphQLTypes['RangeFloatType'];
        } else {

            $RangeFloatTypeProps = Core::$GraphQLTypes['RangeFloatType'] = new InputObjectType([
                'description' => 'Диапазон чисел',
                'name'        => 'RangeFloatTypeProps',
                'fields'      => function () {
                    return [
                        'gte' => [
                            "type"        => Type::int(),
                            "description" => "Больше или равно - greater than or equal (логическое условие >=) ",
                        ],
                        'lte' => [
                            "type"        => Type::int(),
                            "description" => "Меньше или равно - less than or equal (логическое условие >=) ",
                        ],
                    ];
                },
            ]);


            $RangeFloatType = Core::$GraphQLTypes['RangeFloatType'] = new InputObjectType([
                'description' => 'Диапазон чисел',
                'name'        => 'RangeFloatType',
                'fields'      => function () use ($RangeFloatTypeProps) {
                    return [
                        'gte' => [
                            "type"        => Type::int(),
                            "description" => "Больше или равно - greater than or equal (логическое условие >=) ",
                        ],
                        'lte' => [
                            "type"        => Type::int(),
                            "description" => "Меньше или равно - less than or equal (логическое условие >=) ",
                        ],
                        'or' => [
                            "type" => Type::listOf($RangeFloatTypeProps),
                            "description" => "Для нескольких условий фильтра поля"
                        ]
                    ];
                },
            ]);
        }

        return [
            'type'        => $RangeFloatType,
            'description' => 'Фильтр по полю ' . $this->title,
        ];
    }


    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::float()) : Type::float(),
            'description' => $this->title,
        ];
    }
}
