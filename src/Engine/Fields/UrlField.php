<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class UrlField extends DefaultField
{
    public $input = "url";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return Type::string();
    }

    public function getDemoValue()
    {
        return 'https://demo.demo/demo';
    }

    public function getInputDescription(){
        return "Ссылка";
    }


    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}
