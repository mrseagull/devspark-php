<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class QRField extends DefaultField
{
    public $input = "qr";
    public $onlyshow = true;
    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }


    public function getInputDescription(){
       
        return "QR код";
    }

    public function getDemoValue()
    {
        return (\Faker\Factory::create('ru_RU'))->text();
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}
