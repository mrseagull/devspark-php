<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class IFrameFiled extends DefaultField
{
    public $input = "iframe";
    public $onlyshow = true;

    public function getExportValue($values)
    {

        return "";
    }




    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return null;
    }

    public function getGraphQLInputField($objectItem = null)
    {
        return null;
    }
}