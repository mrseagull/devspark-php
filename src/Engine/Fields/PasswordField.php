<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class PasswordField extends DefaultField
{
    public $input = "password";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getInputDescription(){
        return "Пароль";
    }

    public function getDemoValue()
    {
        return (\Faker\Factory::create('ru_RU'))->text();
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}
