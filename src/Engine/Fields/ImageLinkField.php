<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class ImageLinkField extends DefaultField
{
    public $input = "imagelink";

    public $resize = [];
    public $accept = "image/*";

    public $width = 80;

    public function getTpl($item, $key)
    {

        return (string) view('barabaas::' . $this->input, ["val" => $item[$key] ?? ""]);
    }

    public function getInputDescription(){
        return "Изображение";
    }

    public function getDemoValue()
    {

        $faker = (\Faker\Factory::create('ru_RU'));

        return  'https://picsum.photos/300/300/?blur=5&random=' . rand(1, 10000); // $faker->imageUrl($this->resize['w'] ?? 400, $this->resize['h'] ?? 400, $this->title, true);
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);

        $this->resize = $params['resize'] ?? [];
        $this->accept = $params['accept'] ?? $this->accept;
    }

    public function getJsonParams()
    {
        return [
            "resize" => $this->resize,
            "accept" => $this->accept,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title . ' (ссылка на изображение)',
        ];
    }
}