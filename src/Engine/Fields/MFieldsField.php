<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MFieldsField extends DefaultField
{

    public $objectItemKey = ""; //Поле для генерации GraphQL когда нужен родитель

    public $input = "mfields";

    public $mfields = [];

    public $vertical = false;

    public $addTitle = "Добавить";


    public function getInputDescription(){

        $fields = [];
        foreach($this->mfields as $item){
            $fields[] = $item->title.' - '.(strlen($item->description) > 1 ? $item->description : $item->getInputDescription());
        }
        return implode('<br/>', $fields);

    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);

        $this->vertical = $params['vertical'] ?? false;
        $this->mfields  = $params['mfields'];
        $this->addTitle = $params['addtitle'] ?? "Добавить";
    }

    public function getDemoValue()
    {
        $result = [];
        $count = rand(3, 10);
        for ($i = 0; $i < $count; $i++) {
            $item = [];
            foreach ($this->mfields as $field) {
                $item[$field->key] = $field->getDemoValue();
            }
            $result[] = $item;
        }

        return  $result;
    }


    public function getExportValue($values)
    {
        $result = [];

        $value = $values[$this->key] ?? null;
        if ($value) {

            foreach ($value as $row) {
                $row = (array) $row;



                $insertRow = [];

                foreach ($this->mfields as $mfield) {

                    $insertRow[] = $mfield->title . ': ' .  $mfield->getExportValue($row);
                }
                $result[] = implode('; ', $insertRow);
            }


            return implode(' | ', $result);
        }

        return "";
    }

    public function getJsonParams()
    {

        $mfields = [];
        foreach ($this->mfields as $field) {
            $mfields[$field->key] = $field->getJson();
        }
        return [
            "addTitle" => $this->addTitle,
            "mfields"  => $mfields,
            "vertical" => $this->vertical
        ];
    }

    public function getGraphQLType()
    {

        $itemName =  ucfirst($this->key);
        $typeName = $itemName . ucfirst($this->objectItemKey);
        $mfields  = $this->mfields;

        $_this = $this;
        if (isset(Core::$GraphQLTypes[$typeName])) {
            return Core::$GraphQLTypes[$typeName];
        } else {

            return Core::$GraphQLTypes[$typeName] = new ObjectType([
                'description' => $this->title . ' (' .
                    ($this->description ??
                        'Поле ' . $this->title . ' записи') . ')',
                'name'        => $typeName,
                'fields'      => function () use ($mfields, $_this) {

                    $fields = [];

                    foreach ($mfields as $field) {
                        $fields[$field->key] = $field->getGraphQLField($_this);
                    }


                    return $fields;
                },

            ]);
        }
    }

    public function getGraphQLField($objectItem = null)
    {

        $this->objectItemKey = $objectItem->key;

        return [
            'type'        => Type::listOf($this->getGraphQLType()),
            'description' => $this->title . ' (' .
                ($this->description ??
                    'Поле ' . $this->title . ' записи') . ')',
            'resolve'     => function ($root, $args) {
                return  isset($root[$this->key]) ? (array)$root[$this->key] : null;
            }

        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {

        if ($this->onlyshow) {
            return null;
        }

        $itemName = ucfirst($this->key);
        $typeName = $itemName . ucfirst($this->objectItemKey) . "Input";
        $mfields  = $this->mfields;

        $_this = $this;

        if (isset(Core::$GraphQLTypes[$typeName])) {
            $inputType = Core::$GraphQLTypes[$typeName];
        } else {

            $inputType = Core::$GraphQLTypes[$typeName] = new InputObjectType([
                'description' => $this->title,
                'name'        => $typeName,
                'fields'      => function () use ($mfields, $_this) {

                    $fields = [];

                    foreach ($mfields as $field) {
                        $fields[$field->key] = $this->required ? Type::nonNull($field->getGraphQLInputField($_this)) : $field->getGraphQLInputField($_this);
                    }
                    return $fields;
                },
            ]);
        }

        return [
            'type'        => $this->required ? Type::nonNull($inputType) : $inputType,
            'description' => $this->title,
        ];
    }
}