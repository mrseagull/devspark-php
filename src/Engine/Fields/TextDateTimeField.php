<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class TextDateTimeField extends DefaultField
{
    public $input = "textdatetime";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public $width = 100;


    public function getDemoValue()
    {
        return date("d.m.Y H:i:s", strtotime(rand(-10, 10) . ' day'));
    }

    public function getInputDescription(){
        return "Дата и время";
    }


    public function getTpl($item, $key)
    {

        $val = $item[$this->key] ?? null;
        if ($val)
            return (string) view('barabaas::' . $this->input, ["val" => $val ?? ""]);
    }


    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}