<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class DefaultField
{
    public $key;
    public $title;
    public $required     = false;
    public $description  = "";
    public $default      = null;
    public $onlyshow     = false;
    public $fieldDisplay = [];
    public $style        = null;
    public $col          = 24;
    public $input        = "none";
    public $placeholder = null;
    public $before = null;
    public $after  = null;
    public $speller = false;

    public $fastEdit = false;

    public $multilingual = [];




    public $width = 200;

    public $nullable = null;


    public function getDemoValue()
    {
        return null;
    }


    public function getInputDescription(){
        return null;
    }


    public function getTpl($item, $key)
    {
        return null;
    }

    public function getExportValue($values)
    {

        return Utils::valueFromPath($values, $this->key);
    }


    public function isEmpty($value)
    {

        return !$value;
    }

    public function setMultilingual($multilingual)
    {
        $this->multilingual = $multilingual;
        return $this;
    }


    public function setFastEdit($fastEdit)
    {
        $this->fastEdit = $fastEdit;
        return $this;
    }


    public function validWithFieldDisplay($values): bool
    {

        $valid = false;
        if ($this->fieldDisplay && count($this->fieldDisplay) > 0) {

            foreach ($this->fieldDisplay as $key => $filterValues) {
                if (isset($values[$key]) && in_array($values[$key], $filterValues)) {
                    $valid = true;
                }
            }
        } else {
            $valid = true;
        }

        return $valid;
    }

    public function getNull()
    {



        if ($this->onlyshow && $this->default) {
            return $this->default;
        } else  if ($this->required && $this->default) {
            return $this->default;
        } else {
            return $this->nullable;
        }
    }

    public function __construct($key, $params = [])
    {
        $this->key          = $key;
        $this->title        = $params['title'] ?? $key;
        $this->required   = $params['required'] ?? false;
        $this->description  = $params['description'] ?? "";
        $this->default      = $params['default'] ?? null;
        $this->onlyshow     = $params['onlyshow'] ?? false;
        $this->fieldDisplay = $params['fieldDisplay'] ?? null;
        $this->style        = $params['style'] ?? null;
        $this->before       = $params['before'] ?? null;
        $this->after        = $params['after'] ?? null;
        $this->col          = $params['col'] ?? 24;
        $this->placeholder = $params['placeholder'] ?? null;
        $this->multilingual = $params['multilingual'] ?? [];
        $this->speller = $params['speller'] ?? false;
        $this->fastEdit = $params['fastEdit'] ?? false;
    }

    public function getJson()
    {
        return array_merge([
            "key"          => $this->key,
            "input"        => $this->input,
            "title"        => $this->title,
            "required"     => $this->required,
            "description"  => $this->description,
            "default"      => $this->default,
            "onlyshow"     => $this->onlyshow,
            "fieldDisplay" => $this->fieldDisplay,
            "placeholder" => $this->placeholder,
            "style"        => $this->style,
            "before"       => $this->before,
            "after"        => $this->after,
            "col"          => $this->col,
            "width" => $this->width,
            "speller" => $this->speller,
            'multilingual' => $this->multilingual,
            "fastEdit" => $this->fastEdit,
        ], $this->getJsonParams());
    }


    public function getJsonParams()
    {
        return [];
    }

    public function getFilterField()
    {
        return null;
    }

    public function getSearchValue($value)
    {
        return "";
    }

    public function getGraphQLType()
    {
        return Type::string();
    }

    public function getGraphQLFilterField($objectItem = null)
    {
        return null;
    }

    public function getGraphQLMatchFilter($value = null)
    {
        return null;
    }

    public function getGraphQLField($objectItem = null)
    {
        return [
            'type'        => $this->getGraphQLType(),
            'description' => $this->title . ' (' .
                ($this->description ??
                    'Поле ' . $this->title . ' записи') . ')',
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        return null;
    }
}
