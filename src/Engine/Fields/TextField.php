<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class TextField extends DefaultField
{
    public $input = "text";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getSearchValue($value)
    {
        return $value ?? "";
    }

    public function getDemoValue()
    {
        return (\Faker\Factory::create('ru_RU'))->text(20);
    }


    public function getInputDescription(){
       
            return "Короткий текст";
      
    }


    public function getTpl($item, $key)
    {

        $val = Utils::valueFromPath($item, $key);


        return (string) view('barabaas::' . $this->input, ["val" => $val ?? ""]);
    }



    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}