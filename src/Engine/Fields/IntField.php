<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;

class IntField extends DefaultField
{
    public $input = "number";

    public $max = null;
    public $min = null;

    public $width = 100;

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
        $this->max = $params['max'] ?? null;
        $this->min = $params['min'] ?? null;
    }

    public function getInputDescription()
    {
        return  "Это поле отображается " . (!$this->onlyshow ? " и доступно для редактирования " : "") . " в числовом формате, 
        то есть здесь выводится конкретное число, 
        которое соответствует значению \"" . $this->title . "\".";
    }



    public function getDemoValue()
    {

        return rand($this->min ?? 1000,  $this->max ?? 4000);
    }

    public function isEmpty($value)
    {
        return !is_numeric($value);
    }



    public function getFilterField()
    {

        return new RangeIntField($this->key, [
            "title" => $this->title,
        ]);
    }
    public function getJsonParams()
    {
        return [
            "max" => $this->max,
            "min" => $this->min,
        ];
    }

    public function getSearchValue($value)
    {
        return $value ?? "";
    }

    public function getGraphQLType()
    {
        return Type::int();
    }


    public function getGraphQLMatchFilter($value = null)
    {

        if (isset($value['or']) && count($value['or']) > 0) {

            $result = [];
            foreach ($value['or'] as $value) {

                $val = [];
                if (isset($value['gte'])) $val['$gte'] =  $value['gte'];
                if (isset($value['lte'])) $val['$lte'] =  $value['lte'];
                if (count($val) > 0)
                    $result[] =  [$this->key => $val];
            }
            if (count($result) > 0)
                return [
                    '$match' => [
                        '$or' => $result
                    ]
                ];
            else return null;
        } else {

            $val = [];
            if (isset($value['gte'])) $val['$gte'] =  $value['gte'];
            if (isset($value['lte'])) $val['$lte'] =  $value['lte'];
            if (count($val) > 0)
                return ['$match' => [$this->key => $val]];
            else return null;
        }
    }

    public function getGraphQLFilterField($objectItem = null)
    {

        if (isset(Core::$GraphQLTypes['RangeIntType'])) {
            $RangeIntType = Core::$GraphQLTypes['RangeIntType'];
        } else {

            $RangeIntTypeProps = Core::$GraphQLTypes['RangeIntType'] = new InputObjectType([
                'description' => 'Диапазон чисел',
                'name'        => 'RangeIntTypeProps',
                'fields'      => function () {
                    return [
                        'gte' => [
                            "type"        => Type::int(),
                            "description" => "Больше или равно - greater than or equal (логическое условие >=) ",
                        ],
                        'lte' => [
                            "type"        => Type::int(),
                            "description" => "Меньше или равно - less than or equal (логическое условие >=) ",
                        ],
                    ];
                },
            ]);


            $RangeIntType = Core::$GraphQLTypes['RangeIntType'] = new InputObjectType([
                'description' => 'Диапазон чисел',
                'name'        => 'RangeIntType',
                'fields'      => function () use ($RangeIntTypeProps) {
                    return [
                        'gte' => [
                            "type"        => Type::int(),
                            "description" => "Больше или равно - greater than or equal (логическое условие >=) ",
                        ],
                        'lte' => [
                            "type"        => Type::int(),
                            "description" => "Меньше или равно - less than or equal (логическое условие >=) ",
                        ],
                        'or' => [
                            "type" => Type::listOf($RangeIntTypeProps),
                            "description" => "Для нескольких условий фильтра поля"
                        ]
                    ];
                },
            ]);
        }

        return [
            'type'        => $RangeIntType,
            'description' => 'Фильтр по полю ' . $this->title,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::int()) : Type::int(),
            'description' => $this->title,
        ];
    }
}
