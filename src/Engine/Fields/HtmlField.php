<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class HtmlField extends DefaultField
{
    public $input = "html";


    public function getInputDescription(){
        return "Текст";
    }

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? "";

        return strip_tags($val);
    }


    public function getDemoValue()
    {
        return (\Faker\Factory::create('ru_RU'))->text();
    }





    public function getTpl($item, $key)
    {

        return (string) view('barabaas::' . $this->input, ["val" => $item[$key] ?? ""]);
    }



    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return Type::string();
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}