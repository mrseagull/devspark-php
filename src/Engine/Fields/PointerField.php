<?php

namespace DevSpark\Engine\Fields;


use DevSpark\Engine\Core;
use DevSpark\Utils\MongoHelper;
use GraphQL\Type\Definition\Type;

class PointerField extends DefaultField
{
    public $input = "pointer";

    public $display = "";
    public $object  = "";

    public function getExportValue($values)
    {


        $val = $values[$this->key] ?? [];

        $val = (array)$val;

        $result = [];
        foreach (explode(" ", $this->display) as $field) {

            $field = explode(".", $field);

            if (count($field) == 3) {
                $result[] = isset($val[$field[0]][$field[1]][$field[2]]) ?  $val[$field[0]][$field[1]][$field[2]] : "";
            }

            if (count($field) == 2) {
                $result[] =  isset($val[$field[0]][$field[1]]) ?  $val[$field[0]][$field[1]] : "";
            }

            if (count($field) == 1) {
                $result[] = isset($val[$field[0]]) ?  $val[$field[0]] : "";
            }
        }

        return implode(" ", $result);
    }



    public function getInputDescription(){
        if($this->object)
        return "Запись из раздела \"".Core::get($this->object)->title."\"";
        else
        return "Запись из раздела";
    }


    public function getDemoValue()
    {

        $keys = explode(" ", $this->display);
        $item = [];
        foreach ($keys as $key) {
            $item[$key] = (\Faker\Factory::create('ru_RU'))->text(20);
        }
        return $item;
    }

    public function getFilterField()
    {

        return new RelationField($this->key, [
            "title"   => $this->title,
            "object"  => $this->object,
            "display" => $this->display,
        ]);
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
        $this->display = $params['display'];
        $this->object  = $params['object'];

        Core::addRelationship($key, $params['object'], "one");
    }

    public function getJsonParams()
    {

        return [
            "display" => $this->display,
            "object"  => $this->object,
        ];
    }

    public function getGraphQLType()
    {

        return Core::getModel($this->object)->getGraphQLType();
    }

    public function getGraphQLField($objectItem = null)
    {
        $_this = $this;
        return [
            'type'        => $this->getGraphQLType(),
            'description' => $this->title . ' (' .
                ($this->description ??
                    'Поле ' . $this->title . ' записи') . ')',

            'resolve'     => function ($root, $args) use ($_this) {

                if (isset($root[$_this->key])) {

                    $_id = null;
                    if (isset($root[$_this->key]["_id"])) {
                        $_id = $root[$_this->key]["_id"];
                    } else {
                        $_id = $root[$_this->key];
                    }

                    $params = [
                        "_id"   => $_id,
                        "limit" => 1,
                        "deep"  => 1,
                    ];

                    $result = Core::getModel($this->object)->find($params);

                    return $result[0] ?? null;
                } else {
                    return null;
                }
            },
        ];
    }





    public function getGraphQLMatchFilter($value = null)
    {

        $_ids = [];

        foreach ($value as $val) {
            $_ids[] = MongoHelper::id($val);
        }

        if (count($_ids) > 0)
            return [
                '$match' => [
                    $this->key =>
                    ['$in' => $_ids]
                ]
            ];
        else
            return null;
    }

    public function getGraphQLFilterField($objectItem = null)
    {
        return [
            'type'        => Type::listOf(Type::ID()),
            'description' => 'Фильтр по полю ' . $this->title . ', cписок _id записей для фильтрации',
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::ID()) : Type::ID(),
            'description' => $this->title . '  _id записи',
        ];
    }
}
