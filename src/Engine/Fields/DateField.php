<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\Type;

class DateField extends DefaultField
{
    public $input = "date";

    public $width = 100;
    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return $val > 0 ?
            date("d.m.Y", $val)
            : "";
    }

    public function getInputDescription(){
        return "Дата";
    }


    public function getDemoValue()
    {
        return  time() + rand(-604800, 604800);
    }

    public function getFilterField()
    {

        return new RangeDateField($this->key, [
            "title" => $this->title,
        ]);
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return Type::Float();
    }

    public function getGraphQLMatchFilter($value = null)
    {
        return [
            '$match' => [$this->key => [
                '$gte' => $value['gte'],
                '$lte' => $value['lte'],
            ]]

        ];
    }
    public function getGraphQLFilterField($objectItem = null)
    {

        if (isset(Core::$GraphQLTypes['RangeDateFilterInputType'])) {
            $RangeDateFilterInputType = Core::$GraphQLTypes['RangeDateFilterInputType'];
        } else {

            $RangeDateFilterInputType = Core::$GraphQLTypes['RangeDateFilterInputType'] = new InputObjectType([
                'description' => 'Диапазон дат',
                'name'        => 'RangeDateFilterInputType',
                'fields'      => function () {
                    return [
                        'gte' => [
                            "type"        => Type::float(),
                            "description" => "Больше или равно - greater than or equal (логическое условие >=) ",
                        ],
                        'lte' => [
                            "type"        => Type::float(),
                            "description" => "Меньше или равно - less than or equal (логическое условие >=) ",
                        ],
                    ];
                },
            ]);
        }

        return [
            'type'        => $RangeDateFilterInputType,
            'description' => 'Фильтр по полю ' . $this->title,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::Float()) : Type::Float(),
            'description' => $this->title . ' (Дата в UNIX)',
        ];
    }
}
