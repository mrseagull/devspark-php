<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class TimeField extends DefaultField
{
    public $input = "time";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getExportValue($values)
    {

        $time = $values[$this->key] ?? null;

        return $time ? $time[0] . ":" . $time[1] : "";
    }

    public function getDemoValue()
    {
        return [rand(10, 24), rand(0, 59)];
    }

    public function getInputDescription(){
        return "Время Часы:Mинуты";
    }

    public function getGraphQLType()
    {
        return Type::listOf(Type::int());
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::listOf(Type::int())) : Type::listOf(Type::int()),
            'description' => $this->title,
        ];
    }
}
