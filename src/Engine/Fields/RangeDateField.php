<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class RangeDateField extends DefaultField
{
    public $input = "rangedate";

    public function getExportValue($values)
    {


        $val = $values[$this->key] ?? null;

        return (isset($val['from']) && isset($val['to'])) ?
            date("d.m.Y", $val['from']) . ' - ' . date("d.m.Y", $val['to'])
            : "";
    }


    public function getInputDescription()
    {

        return "Диапазон дат от и до включительно";
    }


    public function getDemoValue()
    {
        $date = time() + rand(-604800, 604800);
        return  ["from" => $date, "to" => $date + rand(-604800, 604800)];
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {


        $key = $this->key;

        $name =  'RangeDateType';

        if (isset(Core::$GraphQLTypes[$name])) {
            return Core::$GraphQLTypes[$name];
        } else {

            return Core::$GraphQLTypes[$name] = new ObjectType([
                'description' => 'Диапазон дат',
                'name'        => $name,
                'fields'      => function () {
                    return [
                        'from'   => Type::float(),
                        'to' => Type::float(),
                    ];
                },
                
            ]);
        }
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::listOf(Type::float())) : Type::listOf(Type::float()),
            'description' => $this->title,
        ];
    }
}
