<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class AudioField extends DefaultField
{
    public $input = "audio";

    public function getExportValue($values)
    {
        return $values[$this->key] ?? "";
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }


    public function getInputDescription(){
        return "Аудио файл";
    }

    public $accept = "";

    public function getJsonParams()
    {
        return [
            "accept" => $this->accept,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title . ' (ссылка на аудио файл)',
        ];
    }
}