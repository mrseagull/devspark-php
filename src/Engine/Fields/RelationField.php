<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Utils\MongoHelper;
use DevSpark\Engine\Core;
use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class RelationField extends DefaultField
{
    public $input = "relation";

    public $display = "";
    public $object  = "";

    public function getExportValue($values)
    {

        $vals = $values[$this->key] ?? null;


        $data = [];
        if ($vals && count($vals) > 0) {
            foreach ($vals as $index => $val) {


                $result = [];

                foreach (explode(" ", $this->display) as $field) {

                    $field = explode(".", $field);

                    if (count($field) == 3) {
                        $result[] = $val[$field[0]][$field[1]] ?? "";
                    }

                    if (count($field) == 2) {
                        $result[] = $val[$field[0]][$field[1]] ?? "";
                    }

                    if (count($field) == 1) {
                        $result[] = $val[$field[0]] ?? "";
                    }
                }



                $data[] = implode(" ", $result);
            }


            return implode(", ", $data);
        }
    }


    public function getInputDescription(){
        if($this->object)
        return "Выбор одной или нескольких записей из раздела \"". Core::get($this->object)->title."\"";
        else
        return "Выбор одной или нескольких записей из раздела";
    }

    public function getDemoValue()
    {

        $keys = explode(" ", $this->display);


        $result = [];
        for ($i = 0; $i < 2; $i++) {
            $item = [];
            foreach ($keys as $key) {
                $item[$key] = (\Faker\Factory::create('ru_RU'))->text(20);
            }

            $result[] = $item;
        }

        return  $result;
    }

    public function getTpl($item, $key)
    {

        $vals = $this->getExportValue($item);

        $vals = explode(",", $vals);

        $vals = array_values($vals);
        $text = [];

        return (string) view('barabaas::' . $this->input, ["val" => $vals ?? ""]);
    }

    public function getFilterField()
    {

        return $this;
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
        $this->display = $params['display'];
        $this->object  = $params['object'];

        Core::addRelationship($key, $params['object'], "many");
    }

    public function getJsonParams()
    {

        return [
            "display" => $this->display,
            "object"  => $this->object,
        ];
    }

    public function getGraphQLType()
    {

        $object   = $this->object;

        return Type::listOf(Core::getModel($object)->getGraphQLType());
        /*
     
        $itemName = ucfirst($this->key);

        if (isset(Core::$GraphQLTypes[$itemName . 'Data'])) {
            return Core::$GraphQLTypes[$itemName . 'Data'];
        } else {

            return Core::$GraphQLTypes[$itemName . 'Data'] = new ObjectType([
                'description' => 'Результат запроса ',
                'name'        => $itemName . 'Data',
                'fields'      => function () use ($object) {
                    return [
                        'data'   => Type::listOf(Core::getModel($object)->getGraphQLType()),
                        'limit'  => Type::int(),
                        'offset' => Type::int(),
                        'total'  => Type::int(),
                    ];
                },
            ]);
        }*/
    }

    public function getGraphQLField($objectItem = null)
    {
        if (Core::getModel($this->object))
            return Core::getModel($this->object)->getGraphQLQuery(true, $this);
        else return Type::string();
    }




    public function getGraphQLMatchFilter($value = null)
    {

        $_ids = [];

        foreach ($value as $val) {
            $_ids[] = mDB::id($val);
        }

        if (count($_ids) > 0)
            return [
                '$match' => [
                    $this->key => [
                        '$in' => $_ids
                    ]
                ]
            ];
        else
            return null;
    }

    public function getGraphQLFilterField($objectItem = null)
    {
        return [
            'type'        => Type::listOf(Type::ID()),
            'description' => 'Фильтр по полю ' . $this->title . ', cписок _id записей для фильтрации',
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::listOf(Type::ID())) : Type::listOf(Type::ID()),
            'description' => $this->title . ' список _id записей',
        ];
    }
}
