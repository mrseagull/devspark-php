<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class FileField extends DefaultField
{
    public $input = "file";

    public $accept = "";

    public function getExportValue($values)
    {
        $val = $values[$this->key] ?? null;

        return $val['url'] ?? "";
    }

    public function getInputDescription(){
        return "Файл";
    }

    public function __construct($key, $params = [])
    {
        $this->accept = $params['accept'] ?? "";
        parent::__construct($key, $params);
    }


    public function getDemoValue()
    {
        return  [
            'url' =>  'https://demo.ru/demo.file'
        ];
    }

    public function getJsonParams()
    {
        return [
            "accept" => $this->accept,
        ];
    }

    public function getGraphQLType()
    {
        if (isset(Core::$GraphQLTypes['FileType'])) {
            return Core::$GraphQLTypes['FileType'];
        } else {

            return Core::$GraphQLTypes['FileType'] = new ObjectType([
                'description' => 'Файл',
                'name'        => 'FileType',
                'fields'      => function () {
                    return [

                        'url'  => Type::string(),
                        'type' => Type::string(),
                    ];
                },
            ]);
        }
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::ID()) : Type::ID(),
            'description' => $this->title . ' (_ID загруженного файла)',
        ];
    }
}
