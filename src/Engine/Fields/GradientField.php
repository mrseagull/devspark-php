<?php

namespace DevSpark\Engine\Fields;

class GradientField extends DefaultField
{
    public $input = "gradient";


    public function getInputDescription(){
        return "Настройки градиента";
    }

    public function getDemoValue()
    {
        return null; /*   [
            "palette" => [
                [
                    "color" => "rgb(80, 167, 227)",
                    "opacity" => 1,
                    "active" => false,
                    "id" => 1,
                    "offset" => 0.000
                ],
                [
                    "color" => "rgb(128, 181, 244)",
                    "active" => true,
                    "opacity" => 1,
                    "id" => 2,
                    "offset" => 0.425
                ],
                [
                    "color" => "rgb(13, 122, 197)",
                    "active" => false,
                    "opacity" => 1,
                    "id" => 3,
                    "offset" => 0.909
                ]
            ],
            "angle" => 315
        ];*/
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }
}
