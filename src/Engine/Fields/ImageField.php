<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;


class ImageField extends DefaultField
{
    public $input = "image";

    public $resize = [];
    public $accept = "image/*";

    public $width = 80;



    public function getInputDescription(){
        return "Изображение";
    }


    public function getDemoValue()
    {

        $faker = (\Faker\Factory::create('ru_RU'));

        $img = 'https://picsum.photos/300/300/?blur=5&random=' . rand(1, 10000); // $faker->imageUrl($this->resize['w'] ?? 400, $this->resize['h'] ?? 400, $this->title, false, "", true);
        return [


            'url' =>  $img,
            'url_medium' => $img,
            'url_small' => $img

        ];
    }



    public function getTpl($item, $key)
    {

        return (string) view('barabaas::' . $this->input, ["val" => $item[$key] ?? ""]);
    }

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return $val['url'] ?? "";
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);

        $this->resize = $params['resize'] ?? [];
        $this->accept = $params['accept'] ?? $this->accept;
    }

    public function getJsonParams()
    {
        return [
            "resize" => $this->resize,
            "accept" => $this->accept,
        ];
    }

    public function getGraphQLType()
    {
        if (isset(Core::$GraphQLTypes['ImageType'])) {
            return Core::$GraphQLTypes['ImageType'];
        } else {

            return Core::$GraphQLTypes['ImageType'] = new ObjectType([
                'description' => 'Фотография',
                'name'        => 'ImageType',
                'fields'      => function () {
                    return [
                        'url'        => Type::string(),
                        'url_medium' => Type::string(),
                        'url_small'  => Type::string(),
                        'blurhash' => [
                            "type" => Type::string(),
                            "description" => "Blur placeholder for https://github.com/woltapp/blurhash",
                        ],
                        'type'       => Type::string(),

                    ];
                },
            ]);
        }
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::ID()) : Type::ID(),
            'description' => $this->title . ' (_ID загруженного изображения)',
        ];
    }
}