<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class BooleanField extends DefaultField
{
    public $input = "boolean";


    public $width = 60;

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? false;

        return $val  ? "Да" : "Нет";
    }

    public function getInputDescription(){
        return "Значение Да/Нет";
    }

    public function getDemoValue()
    {
        return rand(0, 1) == 1;
    }


    public function getTpl($item, $key)
    {

        if (isset($item[$key]) && $item[$key])
            return '<div class="flex items-center justify-center" style="fill:rgb(22 163 74);">
        <svg  width="20" xmlns="http://www.w3.org/2000/svg" id="mdi-checkbox-marked" viewBox="0 0 24 24"><path d="M10,17L5,12L6.41,10.58L10,14.17L17.59,6.58L19,8M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z" /></svg>
        </div>';
        else
            return '<div class="flex items-center justify-center" style="fill:rgb(220 38 38);">
        <svg class="fill-red-500" width="20" xmlns="http://www.w3.org/2000/svg" id="mdi-close-box" viewBox="0 0 24 24"><path d="M19,3H16.3H7.7H5A2,2 0 0,0 3,5V7.7V16.4V19A2,2 0 0,0 5,21H7.7H16.4H19A2,2 0 0,0 21,19V16.3V7.7V5A2,2 0 0,0 19,3M15.6,17L12,13.4L8.4,17L7,15.6L10.6,12L7,8.4L8.4,7L12,10.6L15.6,7L17,8.4L13.4,12L17,15.6L15.6,17Z" /></svg>
        </div>';
    }


    public function isEmpty($value)
    {

        return gettype($value) != 'boolean';
    }


    public function getFilterField()
    {

        return new SelectField($this->key, [
            "title" => $this->title,
            "items" => [
                "1" => "Активно " . $this->title,
                "2" => "Неактивно " . $this->title,
            ],
        ]);
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return Type::boolean();
    }



    public function getGraphQLMatchFilter($value = null)
    {


        return [
            '$match' => [$this->key => $value]

        ];
    }

    public function getGraphQLFilterField($objectItem = null)
    {
        return [
            'type'        => $this->getGraphQLType(),
            'description' => 'Фильтр по полю ' . $this->title,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::boolean()) : Type::boolean(),
            'description' => $this->title,
        ];
    }
}
