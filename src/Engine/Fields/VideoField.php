<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class VideoField extends DefaultField
{
    public $input = "video";

    public $accept = "video/*";

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return $val['url'] ?? "";
    }


    public function getInputDescription(){
        return "Видео";
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);

        $this->accept = $params['accept'] ?? $this->accept;
    }

    public function getJsonParams()
    {
        return [
            "accept" => $this->accept,
        ];
    }
    public function getDemoValue()
    {

        $videos = [
            array(
                'description' => 'Big Buck Bunny tells',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
                'subtitle' => 'By Blender Foundation',
                'thumb' => 'images/BigBuckBunny.jpg',
                'title' => 'Big Buck Bunny'
            ),
            array(
                'description' => 'The first Blender Open Movie from 2006',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4',
                'subtitle' => 'By Blender Foundation',
                'thumb' => 'images/ElephantsDream.jpg',
                'title' => 'Elephant Dream'
            ),
            array(
                'description' => 'HBO GO now works with Chromecast',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4',
                'subtitle' => 'By Google',
                'thumb' => 'images/ForBiggerBlazes.jpg',
                'title' => 'For Bigger Blazes'
            ),
            array(
                'description' => 'Introducing Chromecast.',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4',
                'subtitle' => 'By Google',
                'thumb' => 'images/ForBiggerEscapes.jpg',
                'title' => 'For Bigger Escape'
            ),
            array(
                'description' => 'Introducing Chromecast.',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4',
                'subtitle' => 'By Google',
                'thumb' => 'images/ForBiggerFun.jpg',
                'title' => 'For Bigger Fun'
            ),
            array(
                'description' => '"Introducing Chromecast. "',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4',
                'subtitle' => 'By Google',
                'thumb' => 'images/ForBiggerJoyrides.jpg',
                'title' => 'For Bigger Joyrides'
            ),
            array(
                'description' => '"Introducing Chromecast. "',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4',
                'subtitle' => 'By Google',
                'thumb' => 'images/ForBiggerMeltdowns.jpg',
                'title' => 'For Bigger Meltdowns'
            ),
            array(
                'description' => '"Sintel is an independently produced "',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4',
                'subtitle' => 'By Blender Foundation',
                'thumb' => 'images/Sintel.jpg',
                'title' => 'Sintel'
            ),
            array(
                'description' => 'Smoking Tire takes the all-new Subaru',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/SubaruOutbackOnStreetAndDirt.mp4',
                'subtitle' => 'By Garage419',
                'thumb' => 'images/SubaruOutbackOnStreetAndDirt.jpg',
                'title' => 'Subaru Outback On Street And Dirt'
            ),
            array(
                'description' => '"Tears of Steel was realized with "',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4',
                'subtitle' => 'By Blender Foundation',
                'thumb' => 'images/TearsOfSteel.jpg',
                'title' => 'Tears of Steel'
            ),
            array(
                'description' => 'The Smoking Tire heads out',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4',
                'subtitle' => 'By Garage419',
                'thumb' => 'images/VolkswagenGTIReview.jpg',
                'title' => 'Volkswagen GTI Review'
            ),
            array(
                'description' => '"The Smoking Tire is going on the "',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4',
                'subtitle' => 'By Garage419',
                'thumb' => 'images/WeAreGoingOnBullrun.jpg',
                'title' => 'We Are Going On Bullrun'
            ),
            array(
                'description' => 'The Smoking Tire meets up with Chris a.',
                'sources' => 'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4',
                'subtitle' => 'By Garage419',
                'thumb' => 'images/WhatCarCanYouGetForAGrand.jpg',
                'title' => 'What care can you get for a grand?'
            )
        ];

        return [
            "url" => $videos[rand(0, count($videos) - 1)]['sources'],
            "cover" => null,
            "type" => "video/mp4",
        ];
    }




    public function getGraphQLType()
    {
        if (isset(Core::$GraphQLTypes['VideoType'])) {
            return Core::$GraphQLTypes['VideoType'];
        } else {

            return Core::$GraphQLTypes['VideoType'] = new ObjectType([
                'description' => 'Видео',
                'name'        => 'VideoType',
                'fields'      => function () {
                    return [
                        'url'   => Type::string(),
                        'cover' => Type::string(),
                        'type'  => Type::string(),
                        'width' => Type::float(),
                        'height' => Type::float(),
                    ];
                },
            ]);
        }
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::ID()) : Type::ID(),
            'description' => $this->title . ' (_ID загруженного видеофайла)',
        ];
    }
}