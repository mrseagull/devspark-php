<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class FileLinkField extends DefaultField
{
    public $input = "filelink";

    public function getExportValue($values)
    {
        $val = $values[$this->key] ?? null;

        return $val ?? "";
    }

    public function getDemoValue()
    {
        return  'https://demo.ru/demo.file';
    }

    public function getInputDescription(){
        return "Файл";
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public $accept = "";

    public function getJsonParams()
    {
        return [
            "accept" => $this->accept,
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title . ' (ссылка на файл)',
        ];
    }
}
