<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class RangeIntField extends DefaultField
{
    public $input = "rangenumber";

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return ($val && count($val) > 1) ? $val[0] . ' - ' . $val[1] : "";
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getDemoValue()
    {
        $num = rand(1, 100);
        return  [$num, $num + rand(1, 100)];
    }


    public function getInputDescription(){
       
        return "Диапазон чисел от и до включительно";
    }

    public function getGraphQLType()
    {
        return Type::listOf(Type::int());
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::listOf(Type::int())) : Type::listOf(Type::int()),
            'description' => $this->title,
        ];
    }
}
