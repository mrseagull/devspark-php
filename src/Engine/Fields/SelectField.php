<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class SelectField extends DefaultField
{
    public $input = "select";

    public $items = [];

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return $this->items[$val ?? null] ?? $this->items[$this->default ?? null] ?? "";
    }


    public function getInputDescription(){
        return "Значение из списка:<br/>".implode('<br/>',array_values($this->items));
    }

    public function getDemoValue()
    {
        return array_keys($this->items)[rand(0, count($this->items) - 1)];
    }

    public function getFilterField()
    {
        if (count($this->items) > 3) {
            return new MSelectField($this->key, [
                "title" => $this->title,
                "items" => $this->items
            ]);
        } else {
            return new SelectField($this->key, [
                "title" => $this->title,
                "items" => $this->items
            ]);
        }
    }

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
        $this->items = $params['items'] ?? [];
    }

    public function getJsonParams()
    {

        return [
            "items" => $this->items,
        ];
    }

    public function getGraphQLType()
    {

        $itemName = ucfirst($this->key);

        if (isset(Core::$GraphQLTypes[$itemName . 'Enum'])) {
            return Core::$GraphQLTypes[$itemName . 'Enum'];
        } else {

            $values = [];
            foreach ($this->items as $key => $value) {

                $values[$key] = ['value' => $key, 'description' => $value];
            }


            return Core::$GraphQLTypes[$itemName . 'Enum'] = new EnumType([
                'name'        => $itemName . 'Enum',
                'description' => "Варианты значений для поля " . $this->title,
                'values'      => $values,
            ]);
        }
    }

    public function getGraphQLField($objectItem = null)
    {
        return [
            'type'        => $this->getGraphQLType(),
            'description' => $this->title . ' (' .
                ($this->description ??
                    'Поле ' . $this->title . ' записи') . ')',

        ];
    }

    public function getGraphQLMatchFilter($value = null)
    {
        return [
            '$match' => [
                $this->key =>
                ['$in' => $value]
            ]
        ];
    }

    public function getGraphQLFilterField($objectItem = null)
    {
        return [
            'type'        => Type::listOf($this->getGraphQLType()),
            'description' => "Фильтр по полю " . $this->title . ', результат если хотябы одно из переданных значений входит в значения поля записи',
        ];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull($this->getGraphQLType()) : Type::listOf($this->getGraphQLType()),
            'description' => $this->title,
        ];
    }
}
