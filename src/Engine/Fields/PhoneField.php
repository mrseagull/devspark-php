<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class PhoneField extends DefaultField
{
    public $input = "phone";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }


    public function getInputDescription(){
        return "Номер телефона";
    }

    public function getSearchValue($value)
    {
        return $value ?? "";
    }


    public function getDemoValue()
    {
        return (\Faker\Factory::create('ru_RU'))->e164PhoneNumber();
    }


    public function getGraphQLType()
    {
        return Type::string();
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::float()) : Type::float(),
            'description' => $this->title,
        ];
    }
}
