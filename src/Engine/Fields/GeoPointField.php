<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Core;
use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class GeoPointField extends DefaultField
{
    public $input = "geopoint";

    public function getExportValue($values)
    {

        $val = $values[$this->key] ?? null;

        return $val['address'] ?? "";
    }


    public function getInputDescription(){
        return "Адрес";
    }

    public function getDemoValue()
    {

        $faker = (\Faker\Factory::create('ru_RU'));

        return  [

            'lat' => $faker->latitude(),
            'lng' => $faker->longitude(),
            'address' =>  $faker->address()

        ];
    }


    public function getTpl($item, $key)
    {

        return (string) view('barabaas::' . $this->input, ["val" => $item[$key] ?? ""]);
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getSearchValue($value)
    {
        if (isset($value['address'])) {
            return (string) $value['address'];
        } else {
            return "";
        }
    }

    public function getGraphQLType()
    {
        if (isset(Core::$GraphQLTypes['GeoPointType'])) {
            return Core::$GraphQLTypes['GeoPointType'];
        } else {

            return Core::$GraphQLTypes['GeoPointType'] = new ObjectType([
                'description' => 'Гео точка и адрес',
                'name'        => 'GeoPoint',
                'fields'      => function () {
                    return [
                        'lat'     => Type::float(),
                        'lng'     => Type::float(),
                        'address' => Type::string(),
                    ];
                },
            ]);
        }
    }

    public function getGraphQLInputField($objectItem = null)
    {

        if ($this->onlyshow) {
            return null;
        }

        if (isset(Core::$GraphQLTypes['GeoPointInputType'])) {
            $inputGeo = Core::$GraphQLTypes['GeoPointInputType'];
        } else {

            $inputGeo = Core::$GraphQLTypes['GeoPointInputType'] = new InputObjectType([
                'description' => 'Гео точка и адрес',
                'name'        => 'GeoPointInputType',
                'fields'      => function () {
                    return [
                        'lat'     => Type::nonNull(Type::float()),
                        'lng'     => Type::nonNull(Type::float()),
                        'address' => Type::nonNull(Type::string()),
                    ];
                },
            ]);
        }

        return [
            'type'        => $this->required ? Type::nonNull($inputGeo) : $inputGeo,
            'description' => $this->title,
        ];
    }
}