<?php

namespace DevSpark\Engine\Fields;

use GraphQL\Type\Definition\Type;

class ColorField extends DefaultField
{
    public $input = "color";

    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getInputDescription(){
        return "Цвет";
    }


    public function getDemoValue()
    {
        return  ['#f44336', '#e91e63', '#9c27b0', '#9c27b0', '#3f51b5', '#2196f3', '#03a9f4', '#009688', '#8bc34a'][rand(0, 8)];
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::string()) : Type::string(),
            'description' => $this->title,
        ];
    }
}
