<?php

namespace DevSpark\Engine\Fields;

use DevSpark\Engine\Utils;
use GraphQL\Type\Definition\Type;

class RateField extends DefaultField
{
    public $input = "rate";



    public $width = 60;


    public function getTpl($item, $key)
    {


        $val =  Utils::valueFromPath($item, $key);

        return (string) view('barabaas::' . $this->input, ["val" => $val ?? ""]);
    }


    public function getDemoValue()
    {
        return rand(4, 5);
    }


    public function getInputDescription(){
       
        return "Рейтинг";
    }


    public function __construct($key, $params = [])
    {
        parent::__construct($key, $params);
    }

    public function getGraphQLType()
    {
        return Type::int();
    }

    public function getGraphQLInputField($objectItem = null)
    {
        if ($this->onlyshow) {
            return null;
        }

        return [
            'type'        => $this->required ? Type::nonNull(Type::int()) : Type::int(),
            'description' => $this->title,
        ];
    }
}