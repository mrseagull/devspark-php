<?php

namespace DevSpark\Engine;

use DevSpark\Engine\Model\Model;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Core
{

    public static $models       = [];
    public static $relationships = [];


    public static $packagePath;

    public static $GraphQLTypes = [];

    public static function addRelationship($key, $modelKey, $type)
    {
        self::$relationships[$key] = [$modelKey, $type];
    }

    public static function setRelationship($relationships)
    {
        self::$relationships = $relationships;
    }

    public static function getAllModels(){
        self::initModels();
        return self::$models;
    }


    public static function hideAllObjects()
    {
        self::initModels();


        foreach (self::$models as $key => $model) {
            $model->setHide(true);
        }
    }

    private static function initModels()
    {
        if (!self::$models) {
            $modelsPath = app_path('Models'); // путь к каталогу с моделями
            $modelFiles = glob("$modelsPath/*.php"); // получаем все файлы моделей

            foreach ($modelFiles as $file) {
                $className = str_replace('.php', '', basename($file)); // получаем имя класса модели
                $modelClass = "App\\Models\\$className"; // формируем полное имя класса модели
                if (class_exists($modelClass) && self::$models[$modelClass::$key]) { // проверяем, что класс модели существует
                    self::$models[$modelClass::$key] = $modelClass; // добавляем ссылку на модель в массив
                }
            }
        }
    }

    public static function getModel($key): Model | null
    {

        self::initModels();

        return self::$models[$key] ?? null;
    }


    public static function get($key): Model | null
    {


        self::initModels();

        return self::$models[$key] ?? null;
    }


    public static function getIndexByKey($key)
    {
        self::initModels();

        return  array_search($key, array_keys(self::$models)) ?? 0;
    }


    public static function getModelByCollection($collection): Model | null
    {
        self::initModels();

        foreach (self::$models as $key => $model) {
            if ($model->collection === $collection) {
                return $model;
                break;
            }
        }

        return null;
    }

    public static function getModels($keys): array | null
    {

        self::initModels();

        $result = [];
        foreach ($keys as $key) {
            if (isset(self::$models[$key])) {
                $result[] = self::$models[$key];
            }
        }
        return $result;
    }

    public static function getObjectsNotKeys($keys): array | null
    {

        self::initModels();

        $result = [];

        foreach (self::$models as $key => $model) {
            if (!in_array($model::$key, $keys)) {
                $result[] = $model;
            }
        }

        return $result;
    }



    public static function getJsonSchema()
    {


        return [

            "support"      => Config::$support ?? false,

            "groups"       => [], //self::getJsonGroups(),
            "profile"      => [],




            "objects"      => self::getJsonObjects(),


        ];
    }


    public static function getJsonObjects($keys = []): array
    {
        $result = [];
        foreach (self::$models as $key => $model) {
            if (!$model->hide) {
                if (count($keys) > 0) {
                    if (in_array($key, $keys))
                        $result[$key] = $model->getJson();
                } else {

                    $result[$key] = $model->getJson();
                }
            }
        }
        return $result;
    }
}
