<?php

namespace DevSpark\Engine;

use DevSpark\Utils\MongoHelper;
use MongoDB\Client;
use MongoDB\Collection;



class mDB
{

    public static function collection($collection): Collection
    {

        return MongoDBConnection::get()->selectCollection($collection);
    }

    public static function id($val){
        return MongoHelper::id($val);
    }
}
