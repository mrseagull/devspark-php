<?php

namespace DevSpark\Utils;


class MongoHelper
{


    public static function id($val)
    {

        if ($val && gettype($val) == 'string') {
            return self::getId($val);
        } else {
            return $val;
        }
    }

    private static function getId($id = false)
    {
        if ($id) {
            $id  = (string) $id;

            if (preg_match('/^[0-9a-f]{24}$/i', $id) === 1) {
                return $id ? new \MongoDB\BSON\ObjectID($id) : new \MongoDB\BSON\ObjectID();
            } else {
                return false;
            }
        }
    }
   
}