<?php

namespace DevSpark\Utils;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use OpenSSLAsymmetricKey;

class JWTEncoder
{
    public static $ttl             = 525600; //Время жизни токена (в минутах)
    public static $refreshTtl      = 525600; //Время жизни refresh-токена (в минутах)
    public static $leeway          = 0; //Допустимый разброс времени при проверке валидности токена
    public static $timestamp       = null; //Переопределение текущего времени при проверке валидности токена
    public static $blacklistPrefix = "_JWTBL_"; //Префикс для черного списка токенов в кеше

    private static $publicKey;
    private static $privateKey;
    private static $algo;

    public static function encode(string $subject, array $customClaims = []): string
    {
        $payload = array_merge($customClaims, self::getDefaultClaims(), ['sub' => $subject]); //'sub' - уникальный ID субъекта токена (напр. ID пользователя)
        return JWT::encode($payload, self::getPrivateKey(), self::getAlgo());
    }

    /**
     * @throws InvalidArgumentException                 Provided key/key-array was empty or malformed
     * @throws DomainException                          Provided JWT is malformed
     * @throws UnexpectedValueException                 Provided JWT was invalid
     * @throws Firebase\JWT\SignatureInvalidException   Provided JWT was invalid because the signature verification failed
     * @throws Firebase\JWT\BeforeValidException        Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws Firebase\JWT\BeforeValidException        Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws Firebase\JWT\ExpiredException            Provided JWT has since expired, as defined by the 'exp' claim
     */
    public static function decode(string $jwt): array
    {
        JWT::$leeway    = self::$leeway;
        JWT::$timestamp = self::$timestamp;
        return (array) JWT::decode($jwt, new Key(self::getPublicKey(), self::getAlgo()));
    }

    public static function addToBlacklist(array $payload): void
    {
        if (!isset($payload['jti'])) {
            throw new \InvalidArgumentException("Can't blacklist JWT without unique ID");
        }

        $key = self::$blacklistPrefix . $payload['jti'];

        if (!isset($payload['exp'])) {
            Cache::forever($key, 'forever');
        }

        if (Cache::has($key)) {
            return;
        }

        Cache::add($key, 'temp', max($payload['exp'], $payload['iat']) - time());
    }

    public static function inBlacklist(array $payload): bool
    {
        if (!isset($payload['jti'])) {
            return false;
        } else {
            return Cache::has(self::$blacklistPrefix . $payload['jti']);
        }
    }

    public static function getExpireTime()
    {
        return Carbon::now('UTC')->addMinutes(self::$ttl)->getTimestamp();
    }

    public static function getRefreshExpireTime()
    {
        return Carbon::now('UTC')->addMinutes(self::$refreshTtl)->getTimestamp();
    }

    private static function getDefaultClaims(): array
    {
        $claims = [];
        $now    = Carbon::now('UTC');

        $claims['iss'] = request()->url(); //URI, где был выдан токен
        $claims['iat'] = $now->getTimestamp(); //Время создания токена
        $claims['nbf'] = $now->getTimestamp(); //Время, после которого токен станет доступен
        $claims['exp'] = $now->addMinutes(self::$ttl)->getTimestamp(); //Время, после которого токен считается невалидным
        $claims['jti'] = Str::random(); //Уникальный идентификатор токена

        return $claims;
    }

    private static function getPrivateKey(): OpenSSLAsymmetricKey
    {
        if (self::$privateKey == null) {
            return self::$privateKey = openssl_pkey_get_private(env('JWT_PRIVATE_KEY'), env('JWT_PASSPHRASE', ""));
        } else {
            return self::$privateKey;
        }
    }

    private static function getPublicKey(): OpenSSLAsymmetricKey
    {
        if (self::$publicKey == null) {
            return self::$publicKey = openssl_pkey_get_public(env('JWT_PUBLIC_KEY'));
        } else {
            return self::$publicKey;
        }
    }

    private static function getAlgo(): string
    {
        if (self::$algo == null) {
            return self::$algo = env('JWT_ALGO', 'RS256');
        } else {
            return self::$algo;
        }
    }
}