<?php

namespace DevSpark\Utils;


use Aws\S3\S3Client;
use DevSpark\Engine\Config;
use PHPMailer\PHPMailer\PHPMailer;

class Utils
{

    

    public static function valueFromPath($values, $pathInit)
    {



        if (is_string($pathInit)) $path = explode('.', $pathInit);
        else $path = [...$pathInit];

        if (isset($values[$path[0]])) {

            if (count($path) > 1) {
                $nowKey =  $path[0];
                unset($path[0]);
                $path = array_values($path);
                return self::valueFromPath($values[$nowKey], $path);
            } else {
                return $values[$path[0]];
            }
        } else {
            return null;
        }
    }



    public static function saveToS3($path, $file, $type = "files")
    {



        // Instantiate an Amazon S3 client.
        $s3 = new S3Client([
            'version'     => 'latest',
            'region'      => 'ru-1',
            'endpoint'    => env("S3_ENDPOINT"),
            'credentials' => [
                'key'    => env("S3_CREDENTIALS_KEY"),
                'secret' => env("S3_CREDENTIALS_SECRET")
            ],
        ]);

        $rootDir = self::onlyLetters($_SERVER['HTTP_HOST']);

        $result = $s3->putObject([
            'Bucket' => env('S3_BUCKET'),
            'Key'    =>   $rootDir . "/" . $type . '/' . $file,
            'Body'   => fopen($path, 'r'),
            'ACL'    => 'public-read',
        ]);
        if (isset($result['ObjectURL'])) {

            return $result['ObjectURL'];
        } else {
            return '';
        }
    }



    public static function getMimeType($file)
    {
        if (function_exists('finfo_open')) {
            $finfo    = finfo_open(FILEINFO_MIME_TYPE);
            $mimetype = finfo_file($finfo, $file);
            finfo_close($finfo);
        } else {
            $mimetype = mime_content_type($file);
        }
        if (empty($mimetype)) {
            $mimetype = 'application/octet-stream';
        }

        return $mimetype;
    }

    public static function createPath($url)
    {

        $urlItems = explode("/", $url);

        $urlItems = array_diff($urlItems, ['']);

        $dir = base_path() . '/';
        foreach ($urlItems as $item) {

            $dir = $dir . $item . '/';
            if (!is_dir($dir)) {
                mkdir($dir, 0755);
            }
        }
    }
    public static function send($mailTo, $subject, $body)
    {

        $mail = new PHPMailer(false);

        $mail->isSMTP();
        $mail->CharSet  = 'UTF-8';
        $mail->Hostname = env("MAIL_HOSTNAME");
        $mail->Host     = env("MAIL_HOST");
        $mail->SMTPAuth = true;
        $mail->Username = env("MAIL_USERNAME");
        $mail->Password = env("MAIL_PASSWORD");
        $mail->Port     = env("MAIL_PORT");
        $mail->IsHTML(true);

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer'       => false,
                'verify_peer_name'  => false,
                'allow_self_signed' => true,
            ),
        );

        $mail->setFrom('hello@onepush.ru', Config::$title);
        $mail->addAddress($mailTo);

        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body    = $body;

        $mail->send();
    }

    public static function has($data, $keys)
    {


        foreach ($keys as $key) {
            if (!isset($data[$key])) {
                return false;
                break;
            }
        }

        return true;
    }

    public static function onlyLetters($str)
    {
        $result =  preg_replace('/[^a-zа-я]/ui', '*', $str);
        $result = explode('*', $result);
        $result = array_diff($result, ['']);

        $text = [];
        foreach ($result as $index => $val) {
            if ($index > 0) $val = ucfirst($val);
            $text[] = $val;
        }
        return implode('', $text);
    }

    public static function getUserCityPosition()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        
        $SxGeo = new SxGeo();

        $position = $SxGeo->getCityFull($ip);

        return $position;
    }


    public static function getIpPosition(): array
    {



        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $SxGeo = new SxGeo();

        $position = $SxGeo->getCityFull($ip);

        return [
            'latitude' => $position['city']['lat'] ?? $position['country']['lat'] ??  40.311476,


            'longitude' => $position['city']['lon'] ?? $position['country']['lon'] ??  85.993190
        ];
    }


    public static function randomPassword()
    {
        $alphabet    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass        = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n      = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


   
  
}