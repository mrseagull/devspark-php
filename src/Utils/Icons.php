<?php

namespace DevSpark\Utils;

class Icons
{


    public static function getIcon($name)
    {
        $icon = null;
        foreach ([90, 80] as $limit) {

            if ($icon) break;


            $packagePath = realpath(dirname(__FILE__) . '/../../');


            $data =   scandir(base_path($packagePath) . '/storage/icons/');


            foreach ($data as $key) {
                if ($icon) break;
                if ($key != "." && $key != "..") {



                    similar_text($name, explode('.', $key)[0], $perc);

                    if ($perc > $limit) {
                        $icon =  file_get_contents(base_path($packagePath) . '/storage/icons/' . $key);
                        break;
                        //  return $icon;
                    }
                }
            }
        }

        return  $icon;
    }
}