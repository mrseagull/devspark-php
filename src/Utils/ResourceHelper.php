<?php

namespace DevSpark\Utils;


use Aws\S3\S3Client;
use DevSpark\Engine\Config;
use DevSpark\Engine\Core;
use PHPMailer\PHPMailer\PHPMailer;

class ResourceHelper
{

    


    public static function get($file, $encoding)
    {


        $hash =  \Composer\InstalledVersions::getReference("devspark/devspark-php") ?? "none";

        if ($file == 'sw' && $encoding == 'js') {
            header('Content-Type: application/javascript');
            echo 'self.addEventListener("install",(e=>{console.info("Installing service worker..."),e.waitUntil(self.skipWaiting())})),self.addEventListener("activate",(e=>{console.info("Service worker activated"),e.waitUntil(self.clients.claim())})),self.addEventListener("fetch",(function(e){}));';
            exit;
        }

        if ($file == 'manifest' && $encoding == 'json') {

            header('Content-Type: application/json; charset=utf-8');


            $manifest = [
                "name" => Config::$title,
                "short_name" =>  Config::$title,
                "description" => Config::$subtitle,
                "icons" => [
                    [
                        "src" =>  Config::$logo,
                        "sizes" => "192x192 512x512",
                        "type" => "image/png"
                    ]
                ],

                "screenshots" => [
                    [
                        "src" => "https://cp98834-apptor_storage.s3.timeweb.com/assets/pwa-s1.jpg",
                        "sizes" => "546x1041",
                        "type" => "image/jpeg",

                    ],
                    [
                        "src" => "https://cp98834-apptor_storage.s3.timeweb.com/assets/pwa-s2.jpg",
                        "sizes" => "546x1041",
                        "type" => "image/jpeg",

                    ],
                    [
                        "src" => "https://cp98834-apptor_storage.s3.timeweb.com/assets/pwa-s3.jpg",
                        "sizes" => "546x1041",
                        "type" => "image/jpeg",

                    ],

                ],


                "shortcuts" => [],
                "theme_color" => Config::$color,
                "background_color" => "#ffff",
                "display" => "fullscreen",
                "orientation" => "portrait",
                "start_url" => "https://" . $_SERVER['HTTP_HOST'],
                "scope" => ".",
                "dir" => "auto",
                "related_applications" => []
            ];

            echo json_encode($manifest);
            exit;
        }

        if ($file == 'wiget' || $file == 'widget') {

            header('Content-Type: text/javascript');
            echo '
           
                    (function () {
                        var em = document.createElement("script");
                        em.type = "text/javascript";
                        em.async = true;
                        em.setAttribute("url", "' . url() . '");
                        em.src =
                            "' . url() . '/admin/assets/main.js?hash=' . $hash . '";
                            
                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(em, s);
                        var st = document.createElement("link");
                        st.rel = "stylesheet";
                        st.href =
                        "' . url() . '/admin/assets/main.css?hash=' . $hash . '";
                        document.head.appendChild(st);

                     
                    })();
                ';
            exit;
        }

        $path = Core::$packagePath.'/build/';
       
        
       

        if ($file == 'main' && $encoding == 'css') {
            header('Content-Type: text/css');
            if (file_exists(($path . $file . '.' . $encoding)))
                echo  file_get_contents(($path . $file . '.' . $encoding));
            exit;
        }

        if ($file == 'main' && $encoding == 'js') {
            header('Content-Type: text/javascript');
            if (file_exists(($path . $file . '.' . $encoding)))
                echo  file_get_contents(($path . $file . '.' . $encoding));

            exit;
        }
    }


  
}