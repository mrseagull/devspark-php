<?php

namespace DevSpark\Utils;


use Error;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use Illuminate\Http\Request;
use maximal\audio\Waveform;
use \phpthumb;
use kornrunner\Blurhash\Blurhash;


class FileUpload
{



    public static function init($request, $type)
    {


        if (!isset($request['file'])) {
            Response::response([
                "file" => "Поле file обязательно для загрузки файла"
            ], 422);
        }



        switch ($type) {


            case "image":
                return self::image($request);
            case "screenshot":
                return self::screenshot($request);
            case "video":
                return self::video($request);
            case "document":
                return self::document($request);
            case "audio":
                return self::audio($request);

            default:
                new Error("Unknown type");
        }
    }


    public static function screenshot(Request $request)
    {

        $image = $request->file;



        $path        = '/storage/files/screenshots';

        Utils::createPath($path);

        $image->move(base_path() . $path, $image->getClientOriginalName());

        return  ['file' => $image->getClientOriginalName(),  'success'    => true];
    }


    public static function image(Request $request)
    {



        $image = $request->file;



        $path        = 'storage/files/images';

        if (is_string($image)) {

            $name        = md5(time() . rand(1111, 9999));


            Utils::createPath($path);

            $file = $request->file;

            $filename        = $name . '.png';
            $filename_medium = $name . '_medium.png';
            $filename_small  = $name . '_small.png';

            $photoBASE64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file));

            file_put_contents(base_path() . '/' . $path . '/' . $filename, $photoBASE64);
        } else {


            Utils::createPath($path);
            $path = base_path() . '/' . $path;

            $name        = md5($image->getClientOriginalName() . time());

            $filename        = $name . '.' . $image->getClientOriginalExtension() ?: 'png';
            $filename_medium = $name . '_medium.' . $image->getClientOriginalExtension() ?: 'png';
            $filename_small  = $name . '_small.' . $image->getClientOriginalExtension() ?: 'png';


            $image->move($path, $filename);
        }


        $path =  $path . '/' . $filename;

        $url   =  self::getResizeImage($path, $filename, $request->w ?? null, $request->h ?? null);

        $sizeOrigin = getimagesize($path);

        if ($sizeOrigin[0] > 1000) {
            $url_filename_medium =  self::getResizeImage($path, $filename_medium, 1000, 1000);
        } else {
            $url_filename_medium = $url;
        }

        if ($sizeOrigin[0] > 500) {
            $url_filename_small =  self::getResizeImage($path, $filename_small, 500, 500);
        } else {
            $url_filename_small = $url;
        }

        $imageForBlur = imagecreatefromstring(file_get_contents($url_filename_small));
        $width = imagesx($imageForBlur);
        $height = imagesy($imageForBlur);

        $step = 1;
        if ($width > 100) $step = round($width / 10);

        $pixels = [];
        for ($y = 0; $y < $height; $y = $y + $step) {
            $row = [];
            for ($x = 0; $x < $width; $x = $x + $step) {
                $index = imagecolorat($imageForBlur, $x, $y);
                $colors = imagecolorsforindex($imageForBlur, $index);

                $row[] = [$colors['red'], $colors['green'], $colors['blue']];
            }
            $pixels[] = $row;
        }

        $components_x = 4;
        $components_y = 3;
        $blurhash = Blurhash::encode($pixels, $components_x, $components_y);


        $fields = [
            'success'    => true,
            'user'       => Auth::$user ? Auth::$user->_id : null,
            'name'       => $image->getClientOriginalName(),
            'url'        => $url,
            'url_medium' => $url_filename_medium,
            'url_small'  => $url_filename_small,
            'source'     => "local",
            "blurhash" => $blurhash ?? null,
            'width'      =>  $sizeOrigin[0],
            'height'     =>  $sizeOrigin[1],
            "type"       => Utils::getMimeType($path),
            'created_at' => time(),
        ];



        $file          = mDB::collection("images")->insertOne($fields);
        $id            = $file->getInsertedId();
        $fields['_id'] = (string) $id;


        return $fields;
    }




    private static function getResizeImage($path, $saveFilename, $w, $h)
    {

        if ($w && $h) {
            $phpThumb = new phpthumb();
            $phpThumb->setSourceFilename($path);

            $phpThumb->setParameter("w", $w);
            $phpThumb->setParameter("h", $h);
            $phpThumb->setParameter("zc", 'C');

            if ($phpThumb->GenerateThumbnail()) {
                $tmp_path = $path . '-' . time();
                $phpThumb->RenderToFile($tmp_path);
                $result =  Utils::saveToS3($tmp_path, $saveFilename, "images");

                unlink($tmp_path);

                return   $result;
            } else {

                return  Utils::saveToS3($path, $saveFilename, "images");
            }
        } else {
            return  Utils::saveToS3($path, $saveFilename, "images");
        }
    }






    public static function document(Request $request)
    {



        $document = $request->file;

        $name        = md5($document->getClientOriginalName() . time());
        $server_path = base_path() . '/';
        $path        = 'storage/files/document';
        Utils::createPath($path);

        $filename = $name . '.' . $document->getClientOriginalExtension();

        $document->move($server_path . $path, $filename);

        $url = Utils::saveToS3(base_path() . '/' . $path . '/' . $filename, $filename, "files");

        $fields = [
            'success'    => true,
            'user'       => Auth::$user ? Auth::$user->_id : null,
            'name'       => $document->getClientOriginalName(),
            'url'        => $url,
            'source'     => "local",
            "type"       => Utils::getMimeType($server_path . $path . '/' . $filename),
            'created_at' => time(),
        ];

        if (file_exists(base_path() . '/' . $path . '/' . $filename)) {
            unlink(base_path() . '/' . $path . '/' . $filename);
        }

        $file          = mDB::collection("images")->insertOne($fields);
        $id            = $file->getInsertedId();
        $fields['_id'] = (string) $id;

        if ($fields['user']) {
            $fields['user'] = (string) $fields['user'];
        }

        return $fields;
    }

    public static function video(Request $request)
    {

        $video_file = $request->file;

        $name        = md5($video_file->getClientOriginalName() . time());
        $server_path = base_path() . '/';
        $path        = 'storage/files/videos';
        Utils::createPath($path);

        $filename       = $name . '.' . $video_file->getClientOriginalExtension() ?: 'mp4';
        $filename_cover = $name . '_cover.png';

        $video_file->move($server_path . $path, $filename);

        $ffmpeg = FFMpeg::create();

        $video = $ffmpeg->open($server_path . $path . '/' . $filename);

        $duration = $ffmpeg->getFFProbe()->format($server_path . $path . '/' . $filename)->get('duration');

        $video->frame(TimeCode::fromSeconds(1))->save($server_path . $path . '/' . $filename_cover);

        $size = getimagesize($server_path . $path . '/' . $filename_cover);

        $mime = Utils::getMimeType($server_path . $path . '/' . $filename);

        $url =  Utils::saveToS3($server_path . $path . '/' . $filename, $filename, "videos");
        unlink($server_path . $path . '/' . $filename);

        $url_cover =  Utils::saveToS3($server_path . $path . '/' . $filename_cover, $filename_cover, "videos");
        unlink($server_path . $path . '/' . $filename_cover);

        $fields = [
            'success'    => true,
            'user'       => Auth::$user ? Auth::$user->_id : null,
            'name'       => $video_file->getClientOriginalName(),
            'url'        => $url,
            'cover'      => $url_cover,
            'duration'   => $duration,
            'width'      => $size[0],
            'height'     => $size[1],
            "type"       => $mime,
            'created_at' => time(),
        ];

        $file          = mDB::collection("videos")->insertOne($fields);
        $id            = $file->getInsertedId();
        $fields['_id'] = (string) $id;

        if ($fields['user']) {
            $fields['user'] = (string) $fields['user'];
        }

        return $fields;
    }

    public function audio(Request $request)
    {


        $file = $request->file;

        $name        = md5($file->getClientOriginalName() . time());
        $server_path = base_path() . '/';
        $path        = 'storage/files/audio';
        Utils::createPath($path);

        $filename = $name . '.' . $file->getClientOriginalExtension() ?: 'mp3';

        $file->move($server_path . $path, $filename);

        $waveform = new Waveform($server_path . $path . '/' . $filename);
        $data     = $waveform->getWaveformData(10, true);
        $wave     = $data['lines1'];
        $duration = $waveform->getDuration();

        if ($request->has('wave')) {
            $wave = explode(",", $request->wave);
            foreach ($wave as &$val) {
                $val = (float) $val;
            }
        }

        $fields = [
            'success'    => true,
            'user'       => Auth::$user ? Auth::$user->_id : null,
            'name'       => $file->getClientOriginalName(),
            'url'        => url() . '/' . $path . '/' . $filename,
            "type"       => Utils::getMimeType($server_path . $path . '/' . $filename),
            "wave"       => $wave,
            "duration"   => $duration,
            'created_at' => time(),
        ];

        $file          = mDB::collection("audio")->insertOne($fields);
        $id            = $file->getInsertedId();
        $fields['_id'] = (string) $id;

        if ($fields['user']) {
            $fields['user'] = (string) $fields['user'];
        }

        return $fields;
    }
}
