<?php

namespace DevSpark\Utils;

use DevSpark\Engine\Core;

class Response
{

   


    public static function isQraphQL()
    {

        $requestData = json_decode(file_get_contents('php://input'), true);
        if (isset($requestData['query'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function response($data = [], $status = 200)
    {

        if (self::isQraphQL()) {

            $msg = "";

            if (is_string($data))  $msg = $data;
            else {

                foreach ($data as $key => $val) {
                    if (is_string($val))  $msg = $msg . $val . ' ';
                }
            }

            throw new \GraphQL\Error\Error(extensions: [
                "type" => "VALID",
                "message" => $msg
            ]);
        } else {

            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Methods: *");
            header("Content-Type: application/json;charset=utf-8");
            header("Access-Control-Allow-Headers: X-Requested-With");
            http_response_code($status);
            echo is_string($data) ? $data : json_encode($data);
            exit;
        }
    }


    public static function hasValueValidator($keys, $params)
    {

        foreach ($keys as $key) {
            if (!isset($params[$key])) {
                self::response([$key => "Заполните все необходимые поля"], 422);
            }
        }
    }

    public static function hasValue($keys, $params)
    {

        foreach ($keys as $key) {
            if (!isset($params[$key])) {
                return false;
                break;
            }
        }
        return true;
    }

    public static function prepareFilterIds($filter)
    {

        foreach ($filter as $key => &$val) {
            if (in_array(gettype($val), ['object', 'array'])) {
                $val = self::prepareFilterIds($val);
            } else {
                if (strlen($val) == 24) {
                    $val = mDB::id($val);
                }
            }
        }
        return $filter;
    }

    public static function prepareFindParams($key, $params)
    {

        $collection = Core::get($key)->collection;

        $fields  = $params['fields'] ?? [];
        $limit   = $params['limit'] ?? 20;
        $skip    = $params['skip'] ?? 0;
        $sort    = $params['sort'] ?? [];
        $filter  = $params['filter'] ?? [];
        $export  = $params['export'] ?? false;
        $search  = $params['search'] ?? null;
        $searchField = $params['search_field'] ?? 'search_string';
        $reverse = $params['reverse'] ?? false;
        $_id     = $params['_id'] ?? null;

        if ($limit == 'all')  $limit = null;

        $pipeline = [];

        $match = [
            "deleted_at" => ['$exists' => false],
        ];

        if ($_id) {
            $match['_id'] = MongoHelper::id($_id);
        } else {
            if (count($filter) > 0) {
                $match = array_merge($match, self::prepareFilterIds($filter));
            }
        }

        if (in_array($collection, Schema::$personalCollections)) {
            $match['manager'] = Auth::$manager_id;
        }

        if ($search) {
            $search = trim($search);

            if (strlen($search) > 0) {

                $search = str_replace(["+"], "", $search);

                $match[$searchField] = ['$regex' => $search, '$options' => 'i'];
            }
        }

        $projection = [];

        if (!$export) {

            if (count($fields) > 0) {
                $projection = [];

                foreach ($fields as $field) {
                    $projection[explode('.', $field)[0]] = true;
                }

                $projection['id'] = true;
            }
        }

        if (count($sort) > 0) {
            $sort = $sort;
        } else {

            if ($reverse) {
                $sort = [

                    "_id" => -1,
                ];
            }
        }

        if ($export) {

            $limit = 0;
            $skip  = 0;
        }

        return [
            'collection' => $collection,
            'filter'     => $match,
            'options'    => [
                'projection'   => $projection,
                'limit'        => $limit,
                'skip'         => $skip,
                'sort'         => $sort,
                'allowDiskUse' => true,
            ],

        ];
    }

    public static function prepareAggregateParams($key, $params)
    {

        $collection = Objects::getObject($key)->collection;

        $fields  = $params['fields'] ?? [];
        $limit   = $params['limit'] ?? 20;
        $skip    = $params['skip'] ?? 0;
        $sort    = $params['sort'] ?? [];
        $filter  = $params['filter'] ?? [];
        $sample = $params['sample'] ?? false;
        $export  = $params['export'] ?? false;
        $search  = $params['search'] ?? null;
        $searchField = $params['search_field'] ?? 'search_string';
        $reverse = $params['reverse'] ?? false;
        $_id     = $params['_id'] ?? null;



        $pipeline = [];

        $pipeline[] =  [
            '$match' => [
                "deleted_at" => ['$exists' => false]
            ]
        ];


        if (in_array($collection, Schema::$personalCollections)) {

            $pipeline[] =  [
                '$match' => [
                    "manager" =>  Auth::$manager_id
                ]
            ];
        }


        if ($_id) {

            $pipeline[] =  [
                '$match' => [
                    "_id" => mDB::getId($_id)
                ]
            ];
        } else {
            if (count($filter) > 0) {

                $prepareFilter = self::prepareFilterIds($filter);
                foreach ($prepareFilter as $key => $value) {

                    if (is_array($value) && isset($value['$match'])) {
                        $pipeline[] = $value;
                    } else {

                        $pipeline[] =  [
                            '$match' => [
                                $key => $value
                            ]
                        ];
                    }
                }
            }
        }


        if ($search) {
            $search = trim($search);

            if (strlen($search) > 0) {

                $search = str_replace(["+"], "", $search);

                $pipeline[] =  [
                    '$match' => [
                        $searchField => ['$regex' => $search, '$options' => 'i']
                    ]
                ];
            }
        }


        //get random record:
        if ($sample) {
            $pipeline[] =  [
                '$sample' => [
                    'size' =>  $limit
                ]
            ];
        }



        if (!$export) {

            if (count($fields) > 0) {
                $projection = [];

                foreach ($fields as $field) {
                    $projection[explode('.', $field)[0]] = true;
                }

                $projection['id'] = true;


                $pipeline[] = [
                    '$project' => $projection
                ];
            }
        }

        if (count($sort) > 0) {
            $pipeline[] = [
                '$sort' => $sort
            ];
        } else {

            if ($reverse) {

                $pipeline[] = [
                    '$sort' => [
                        "_id" => -1,
                    ]
                ];
            }
        }

        if (!$export) {



            $pipeline[] = [
                '$skip' => $skip,
            ];

            if ($limit != 'all')
                $pipeline[] = [
                    '$limit' => $limit,
                ];
        }

        //        dd($pipeline);

        return $pipeline;
    }

    public static function packingPipeline($pipeline)
    {

        $packingPipeline = [];
        $matchСollector = [];

        foreach ($pipeline as $index => $step) {

            if (isset($step['$match']) && !isset($step['$match']['$or'])) {
                $matchСollector = array_merge($matchСollector, $step['$match']);
            } else {
                $packingPipeline[] = $step;
            }
        }


        if (count($matchСollector) > 0) {
            return [['$match' => $matchСollector], ...$packingPipeline];
        } else {
            return $pipeline;
        }
    }

    public static function validRelationships($data)
    {
        $relationships = Core::$relationships;

        $keys_relationships = array_keys($relationships);
        foreach ($data as $key => &$val) {
            if (in_array($key, $keys_relationships)) {

                $relationship = $relationships[$key];

                $type_rel       = $relationship[1];
                $collection_rel = $relationship[0];

                if ($type_rel == 'many') {
                    if (is_array($val)) {
                        foreach ($val as &$id_val) {
                            if (isset($id_val['_id'])) {
                                $id_val = mDB::id($id_val['_id']);
                            } else {
                                $id_val = mDB::id($id_val);
                            }
                        }
                    } else {
                        $val = [];
                    }
                } else {
                    if ($val) {
                        if (isset($val['_id'])) {
                            $val = mDB::id($val['_id']);
                        } else {
                            $val = mDB::id($val);
                        }
                    } else {
                        $val = null;
                    }
                }
            }
        }

        return $data;
    }

    public static function ObjectIdToString($data)
    {


        if (in_array(gettype($data), ['array', 'object'])) {


            foreach ($data as $key => $val) {
                if (in_array(gettype($data), ['array', 'object'])) {



                    if (gettype($val) == 'object' && get_class($val) == "MongoDB\BSON\ObjectId") {

                        $data[$key] = (string)$val;
                    } else {
                        $data[$key] = self::ObjectIdToString($data[$key]);
                    }
                }
            }
        }
        return $data;
    }

    public static function relatedData($data, $deep = 2)
    {

        $relationships = Core::$relationships;

        $keys_relationships = array_keys($relationships);

        //Сбор вложенных коллекция для минимизации запросов
        $buffer = [];
        foreach ($data as $index => &$item) {

            foreach ($item as $key => &$val) {
                if ($val && gettype($val) == 'object') {
                    if (in_array($key, $keys_relationships)) {

                        $relationship = $relationships[$key];

                        $type_rel       = $relationship[1];
                        $collection_rel = $relationship[0];

                        if (!isset($buffer[$collection_rel])) {
                            $buffer[$collection_rel] = [];
                        }

                        if ($type_rel == 'many') {
                            if (get_class($val) == "MongoDB\Model\BSONArray") {

                                $buffer[$collection_rel] = array_merge($buffer[$collection_rel], (array) $val);
                            }
                        } else {
                            if (get_class($val) == "MongoDB\BSON\ObjectId") {

                                $buffer[$collection_rel][] = $val;
                            }
                        }
                    }
                }
            }
        }

        //Сбор данных общим запросом
        foreach ($buffer as $collection => &$itemBufferData) {
            $itemBufferData = mDB::nArray(mDB::collection($collection)->find([
                "_id" => ['$in' => $itemBufferData],
            ]));
        }

        foreach ($data as $index => &$item) {

            foreach ($item as $key => &$val) {

                if ($key == '_id') {
                    $item['_id'] = (string) $item["_id"];
                }

                if ($val && gettype($val) == 'object') {
                    if (in_array($key, $keys_relationships)) {

                        $relationship = $relationships[$key];

                        $type_rel       = $relationship[1];
                        $collection_rel = $relationship[0];

                        if ($type_rel == 'many') {
                            if (get_class($val) == "MongoDB\Model\BSONArray") {

                                $ids = [];
                                foreach ($val as $id) {
                                    $ids[] = (string) $id;
                                }

                                $val = [];
                                foreach ($buffer[$collection_rel] as $itemBuffer) {
                                    if (isset($itemBuffer->_id) && in_array($itemBuffer->_id, $ids)) {
                                        $val[] = $itemBuffer;
                                    }
                                }

                                if ($deep > 1) {
                                    $val = self::relatedData($val, $deep - 1);
                                }
                            } else {
                                $val = [];
                            }
                        } else {

                            if (get_class($val) == "MongoDB\BSON\ObjectId") {

                                $val = (string) $val;

                                foreach ($buffer[$collection_rel] as $itemBuffer) {

                                    if ((isset($itemBuffer->_id) ? $itemBuffer->_id : null) == $val) {
                                        $val = $itemBuffer;
                                    }
                                }

                                /*  $val = mDB::collection($collection_rel)->findOne([
                                '_id' => $val,
                                ]);*/
                                if (isset($val['_id'])) {

                                    if ($deep > 1) {
                                        $val = self::relatedData([$val], $deep - 1)[0];
                                    }
                                } else {
                                    $val = null;
                                }
                            } else {
                                $val = null;
                            }
                        }
                    } else {
                        if (get_class($val) == "MongoDB\BSON\ObjectId") {
                            $val = (string) $val;
                        }
                    }
                }
            }
        }

        return $data;
    }
}
