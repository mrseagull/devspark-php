@include('barabaas::knowledgebase.header', ['small' => true])


@php
    
    $category = null;
    $article = null;
    
    if (isset($key)) {
        $category = \BaraBaaS\Knowledgebase::getCategory($key);
    }
    
    if (isset($articleKey)) {
        $article = \BaraBaaS\Knowledgebase::getArticle($key, $articleKey);
    }
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $logo = DevSpark\Engine\Config::$logo;
    
@endphp

<div class="uk-section uk-section-muted">
    <div class="uk-container">
@if (!$category || !$article)
      
<div>
    <div class="uk-card uk-card-default uk-card-body">
        <h3 class="uk-card-title">{{ \BaraBaaS\Knowledgebase::Lang('Кажется, такой статьи не существует.')}}</h3>
        <p>
            {{ \BaraBaaS\Knowledgebase::Lang('К сожалению, мы не можем отобразить запрошенную статью. Возможно, она была удалена или перемещена в другое место. Если вы уверены, что статья должна быть доступна, попробуйте обновить страницу или свяжитесь с нашей службой поддержки для получения дополнительной информации.')}}</p>
    </div>
</div>

@else

        <ul class="uk-breadcrumb uk-margin-medium-top-">
            <li><a onclick="goMain()">{{ \BaraBaaS\Knowledgebase::Lang('База Знаний') }}</a></li>
            <li><a onclick="goCategory('{{ $category->url }}')">{{ $category->title }}</a></li>
            <li><span>{{ $article->title }}</span></li>
        </ul>
        <div class="uk-background-default uk-border-rounded uk-box-shadow-small">
            <div class="uk-container uk-container-xsmall uk-padding-large" style=" 
                overflow: hidden;
              ">
                <article class="uk-article">
                    <h1 class="uk-article-title">{{ $article->title }}</h1>
                    <div class="uk-article-meta uk-margin uk-flex uk-flex-middle">
                        <img class="uk-border-circle uk-avatar-small" src="{{ $logo }}">
                        <div>
                            {{ $title }}
                            <br>
                            {{ \BaraBaaS\Knowledgebase::Lang('Обновлено:')}} <time>{{ date('d.m.Y', $article->updated_at) }}</time>
                        </div>
                    </div>
                    {!! $article->content !!}


                    <div class="like-block share uk-text-center uk-margin-large-top">
                        <p>{{ \BaraBaaS\Knowledgebase::Lang('Эта статья ответила на ваш вопрос?')}}</p>
                        <a onclick="setLike(false)" title="Dislike"><span class="uk-icon-button uk-text-primary uk-icon"
                                data-uk-icon="icon: close; ratio: 1.2"></span></a>
                        <a onclick="setLike(true)" class="uk-margin-small-left" title="Like"><span
                                class="uk-icon-button uk-text-primary uk-icon"
                                data-uk-icon="icon: check; ratio: 1.2"></span></a>
                    </div>


                    <div class="uk-alert-danger like-minus" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>{{ \BaraBaaS\Knowledgebase::Lang('Большое спасибо за оценку нашей статьи! Мы очень ценим ваше мнение и сожалеем, что вы не
                            смогли найти ответ на ваш вопрос. Всегда стараемся создавать контент максимально полезным
                            для наших клиентов и поэтому хотим сообщить, что мы обязательно дополним статью, чтобы вы
                            могли получить все необходимые сведения.')}}
                            </p>
                    </div>

                    <div class="uk-alert-primary like-plus" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>{{ \BaraBaaS\Knowledgebase::Lang('Спасибо за положительную оценку статьи! Мы рады, что наш материал оказался полезным для вас.
                            Если у вас есть какие-либо вопросы или предложения по улучшению нашей работы, пожалуйста, не
                            стесняйтесь связаться с нами. Мы всегда рады услышать обратную связь от наших клиентов.')}}</p>
                    </div>


                    <div class="uk-margin-large-top paginate-post">
                        <div class="uk-child-width-expand@s uk-grid-large uk-grid" data-uk-grid="">
                            @if (isset($article->prev->_id))
                                <div class="uk-first-column">
                                    <h5>{{ $article->prev->title }}</h5>
                                    <div><a class="uk-remove-underline hvr-back"
                                            onclick="goArticle('{{ $article->prev->url }}')">←
                                            {{ \BaraBaaS\Knowledgebase::Lang('Предыдущая')}}</a></div>
                                </div>
                            @endif

                            @if (isset($article->next->_id))
                                <div class="uk-text-right">
                                    <h5>{{ $article->next->title }}</h5>
                                    <div><a class="uk-remove-underline hvr-forward"
                                            onclick="goArticle('{{ $article->next->url }}')">{{ \BaraBaaS\Knowledgebase::Lang('Следующая')}} →</a></div>
                                </div>
                            @endif


                        </div>
                    </div>
                </article>
            </div>
        </div>



@endif

</div>
</div>

<style>
    .like-plus,
    .like-minus {
        display: none;
    }
</style>

@include('barabaas::knowledgebase.footer')
