@include('barabaas::knowledgebase.header', ['menu' => true,'article'=>true])

@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;
    
   
    
  
    
    $path = base_path('vendor/antonchaykin/barabaas/src/BaraBaaS') . '/storage/login.jpg';
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $loginScreen = 'data:image/' . $type . ';base64,' . base64_encode($data);
    
    $pageTitle = 'Как зарегистрировать аккаунт в '.$title;
@endphp


<div class="uk-section section-sub-nav uk-padding-remove">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-2-3@m">
                <ul class="uk-breadcrumb uk-visible@m">
                    <li><a href="/knowledgebase">Главная</a></li>
                    <li><a href="/knowledgebase/category/start">Начало работы</a></li>
                    <li><span href="">{{ $pageTitle }}</span></li>
                </ul>
            </div>
            <div class="uk-width-1-3@m">
                 <div class="uk-margin">
                    <form class="uk-search uk-search-default">
                        <a href="" class="uk-search-icon-flip" uk-search-icon></a>
                        <input id="autocomplete" class="uk-search-input"  disabled type="search" autocomplete="off"
                            placeholder="Поиск">
                    </form>
                </div>
            </div>
        </div>
        <div class="border-top"></div>
    </div>
</div>

<div class="uk-section uk-section-small uk-padding-remove-bottom section-content">
    <div class="uk-container container-xs">
        <article class="uk-article">
            <header>
                <h1 class="uk-article-title uk-margin-bottom">{{ $pageTitle }}</h1>
                <div class="author-box uk-card">
                    <div class="uk-card-header uk-padding-remove">
                        <div class="uk-grid-small uk-flex-middle  uk-position-relative" uk-grid>
                            <div class="uk-width-auto">
                                <img class="uk-border-circle" width="40" height="40" src="{{ $logo }}">
                            </div>
                            <div class="uk-width-expand">
                                <h5 class="uk-card-title">{{ $title }}</h5>
                                        <p class="uk-article-meta uk-margin-remove-top">Тех. поддержка</p>
                            </div>

                        </div>
                    </div>
                </div>
            </header>
            <div class="entry-content uk-margin-medium-top">
                <p class="uk-text-lead">
                    Чтобы начать работу, заполните 2-шаговую форму на нашем сайте, чтобы получить доступ в админ-панель.
                </p>

               

                @include('barabaas::knowledgebase.static.blocks.helpful')
                <div class="uk-child-width-1-2@s text-dark article-related uk-margin-medium-top" uk-grid>
                    <div>
                        <h3>Recently Viewed Articles</h3>
                        <ul class="uk-list uk-list-large">
                            <li><a href="#">Sed ut perspiciatis unde omnis iste laudantium, totam rem aperiam</a>
                            </li>
                            <li><a href="#">Lorem ipsum dolor sit amet, elabore et dolore magna aliqua</a></li>
                            <li><a href="#">Dolor sit amet, consectetur adipiscing elit, sed do eiusmod</a></li>
                        </ul>
                    </div>
                    <div>
                        <h3>Related Articles</h3>
                        <ul class="uk-list uk-list-large">
                            <li><a href="#">Apsum dolor sit amet, consectet tempor incididunt ut dolore magna
                                    aliqua</a></li>
                            <li><a href="#">Hadem sit amet, consectetur adipiscing elit, sed do eiusmod</a></li>
                            <li><a href="#">Lorem ipsum dolor tempor incididunt ut labore et dolore magna
                                    aliqua</a>
                            </li>
                        </ul>
                    </div>
                </div>
              
                
        </article>
    </div>
</div>




@include('barabaas::knowledgebase.footer')
