
@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;

    $pwaScreen = '/knowledgebase/assets/pwa-screens.jpg';

   
@endphp


                <p class="uk-text-lead">
                    Админ. панель можно быстро и легко установить в виде приложения (PWA) на свой смартфон. В этой
                    инструкции мы расскажем, как установить PWA на разных платформах и разных браузерах.
                </p>

                <img src="{{ $pwaScreen}}" />

                <h3>Установка на Android</h3>

                 
                <h4>Chrome</h4>
                <ol class="ol-pretty uk-list-large">
                    <li>Откройте админ. панель на телефоне в браузере Chrome
                    </li>
                    <li>Нажмите на значок меню (три точки) в правом верхнем углу экрана.</li>
                    <li>Нажмите «Добавить на главный экран».</li>
                    <li>Нажмите «Добавить» и в списке ваших приложений появится {{$title}}</li>

                </ol>


                <h4>Firefox</h4>
                <ol class="ol-pretty uk-list-large">
                    <li>Откройте админ. панель на телефоне в браузере Firefox
                    </li>
                    <li>Нажмите на значок меню (три точки) в правом верхнем углу экрана.</li>
                    <li>Нажмите «Добавить на главный экран».</li>
                    <li>Нажмите «Добавить» и в списке ваших приложений появится {{$title}}</li>

                </ol>


                <h3>Установка на iOS</h3>

                <h4>Safari</h4>
                <ol class="ol-pretty uk-list-large">
                    <li>Откройте админ. панель на телефоне в браузере Safari
                    </li>
                    <li>Нажмите на значок «Поделиться» внизу экрана.</li>
                    <li>Нажмите «На главный экран».</li>
                    <li>Нажмите «Добавить» и в списке ваших приложений появится {{$title}}</li>

                </ol>


              
