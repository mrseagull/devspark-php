@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;
    
    $emailAuth = DevSpark\Engine\Config::$emailAuth;
    $socAuth = DevSpark\Engine\Config::$socAuth;
    
    $loginScreen = '/knowledgebase/assets/login.jpg';
    
@endphp


<p class="uk-text-lead">
    Админ-панель - это важный инструмент для управления вашим аккаунтом в {{ $title }}.
    Здесь вы можете управлять своими данными, выбирать настройки, которые соответствуют вашим
    потребностям.
</p>

<div class="uk-alert-primary uk-alert" uk-alert="">
    <p>Админ-панель поможет вам максимально эффективно использовать ресурсы {{ $title }} и получить
        максимум от работы с этой платформой.</p>
</div>


@if ($emailAuth)
    <h2>Вход по логину</h2>


    <div class="device device-macbook-pro-2018">
        <div class="device-frame">


            <img class="device-screen" src="{{ $loginScreen }}" loading="lazy">


        </div>
        <div class="device-power">&nbsp;</div>
        <div class="device-home">&nbsp;</div>
    </div>


    <h4>Чтобы войти в админ-панель:</h4>
    <ol class="ol-pretty uk-list-large">
        <li>В браузере Chrome или Safari перейдите на сайт Админ-панели {{ $title }}.
        </li>
        <li>Введите email и пароль, которые указали при регистрации.</li>
        <li>Нажмите Войти.</li>

    </ol>



    <p>💡 Админ-панель {{ $title }} доступна в браузере Chrome или Safari с компьютера, по вашему
        логину и паролю.</p>


    <p>☝️ Если вы не помните email или пароль, обратитесь в тех. поддержку.</p>
@endif


@if ($socAuth)
    <h2>Вход через соц. сети</h2>


    <div class="device device-macbook-pro-2018">
        <div class="device-frame">


            <img class="device-screen" src="{{ $loginScreen }}" loading="lazy">


        </div>
        <div class="device-power">&nbsp;</div>
        <div class="device-home">&nbsp;</div>
    </div>


    <h4>Чтобы войти в админ-панель:</h4>
    <ol class="ol-pretty uk-list-large">
        <li>В браузере Chrome или Safari перейдите на сайт Админ-панели {{ $title }}.
        </li>
        <li>Нажмите на одну из предложенных соц. сетей для входа.</li>
        <li>В браузере будет открытая новая страница с выбранной соц. сетью</li>
        <li>Сделайте авторизацию на сайте соц. сети</li>
        <li>Вернетесь на сайта админ. панели</li>

    </ol>



    <p>💡 Админ-панель {{ $title }} доступна в браузере Chrome или Safari с компьютера, по вашему
        акканут соц. сети.</p>



    <p>☝️ Если вы при входе через соц. сети возникли сложности, обратитесь в тех. поддержку.</p>
@endif
