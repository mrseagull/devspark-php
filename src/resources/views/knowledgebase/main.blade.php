@include('barabaas::knowledgebase.header')

@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    
    $faqs = [
        [
            'q' => 'Какие темы охватывает база знаний?',
            'a' => 'База знаний охватывает широкий спектр тем, включая инструкции по использованию сервиса, решения проблем, технические характеристики и многое другое.',
        ],
        [
            'q' => 'Как я могу найти нужную мне информацию в базе знаний?',
            'a' => 'Вы можете воспользоваться функцией поиска на странице базы знаний и ввести ключевые слова, чтобы найти нужную вам информацию.',
        ],
        [
            'q' => 'Я не могу найти ответ на свой вопрос в базе знаний. Что мне делать?',
            'a' => 'Если вы не можете найти ответ на свой вопрос в базе знаний, вы можете связаться с нашей службой поддержки клиентов и получить ответ на свой вопрос.',
        ],
        [
            'q' => 'Обновляется ли информация в базе знаний?',
            'a' => 'Да, информация в базе знаний регулярно обновляется и дополняется новыми материалами.',
        ],
        [
            'q' => 'Могу ли я предложить свои материалы для включения в базу знаний?',
            'a' => 'Да, мы приветствуем предложения наших клиентов и готовы рассмотреть возможность включения вашей информации в базу знаний.',
        ],
        [
            'q' => 'Как я могу оценить полезность материалов в базе знаний?',
            'a' => "На странице каждой статьи в базе знаний есть кнопка \"Да\" или \"Нет\", которую вы можете нажать, чтобы оценить полезность статьи.",
        ],
    
        [
            'q' => 'Как я могу предложить улучшения для базы знаний?',
            'a' => 'Если у вас есть предложения по улучшению базы знаний, вы можете связаться с нашей службой поддержки клиентов и поделиться своими идеями. Мы всегда готовы услышать ваши предложения и рассмотреть возможность их внедрения.',
        ],
    ];
    
	foreach ($faqs as $key => &$value) {
		$value['q'] = \BaraBaaS\Knowledgebase::Lang($value['q']);
		$value['a'] = \BaraBaaS\Knowledgebase::Lang($value['a']);
	}

@endphp




<div class="uk-section uk-section-muted">
    <div class="uk-container">
        <div class="uk-child-width-1-2@m uk-grid-match- uk-grid-small" data-uk-grid>

            @php
                
                $categories = \BaraBaaS\Knowledgebase::getCategories();
                
            @endphp

            @foreach ($categories as $category)
                <div>
                    <div class="category-card uk-card uk-card uk-card-default uk-card-hover uk-card-body uk-inline uk-border-rounded">
                        <a class="uk-position-cover" onclick="goCategory('{{ $category->url }}')"></a>
                        <div class="uk-grid-small" data-uk-grid>
                            <div class="categpry-icon uk-width-auto  uk-flex uk-flex-middle">
                                {!! $category->icon !!}
                            </div>
                            <div class="uk-width-expand">
                                <h3 class="uk-card-title uk-margin-remove ">{{ $category->title }}</h3>
                                <p class="uk-text-muted uk-margin-remove uk-article-preview">
                                    {{ $category->description }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

<div class="uk-section uk-section-muted">
    <div class="uk-container uk-container-small">
        <h2 class="uk-text-center">{{ \BaraBaaS\Knowledgebase::Lang('Часто задаваемые вопросы') }}</h2>
        <ul class="uk-margin-medium-top" data-uk-accordion="multiple: true">


			@foreach ($faqs  as $item)
				
			<li>
                <a class="uk-accordion-title uk-box-shadow-hover-small" href="#">{{$item['q']}}</a>
                <div class="uk-article-content uk-accordion-content link-primary">
                    <p>
                       {{$item['a']}}
                    </p>
                </div>
            </li>

			@endforeach


        </ul>
    </div>
</div>

<div class="uk-section uk-section-muted">
    <div class="uk-container">
        <h2 class="uk-text-center">{{ \BaraBaaS\Knowledgebase::Lang('Не нашли ответ?') }}</h2>
        <p class="uk-text-muted uk-text-center uk-text-lead">
			{{ \BaraBaaS\Knowledgebase::Lang('Наша команда находится на расстоянии одного сообщения и
            готова ответить на
            ваши вопросы.') }}</p>
       
        <div class="uk-margin-medium-top uk-text-center"
            data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true">
            <a class="uk-button uk-button-primary uk-border-rounded" href="javascript:jivo_api.open()">{{ \BaraBaaS\Knowledgebase::Lang('Написать нам')}}</a>
        </div>
    </div>
</div>

<div id="offcanvas-docs" data-uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" data-uk-close></button>
        <h5 class="uk-margin-top">Getting Started</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li class="uk-active"><a href="article.html">Template setup</a></li>
            <li><a href="article.html">Basic theme setup</a></li>
            <li><a href="article.html">Navigation bar</a></li>
            <li><a href="article.html">Footer options</a></li>
            <li><a href="article.html">Creating your first post</a></li>
            <li><a href="article.html">Creating docs posts</a></li>
            <li><a href="article.html">Enabling comments</a></li>
            <li><a href="article.html">Google Analytics</a></li>
        </ul>
        <h5 class="uk-margin-top">Product Features</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li><a href="article.html">Hero page header</a></li>
            <li><a href="article.html">Category boxes section</a></li>
            <li><a href="article.html">Fearured docs section</a></li>
            <li><a href="article.html">Video lightbox boxes section</a></li>
            <li><a href="article.html">Frequently asked questions section</a></li>
            <li><a href="article.html">Team members section</a></li>
            <li><a href="article.html">Call to action section</a></li>
            <li><a href="article.html">Creating a changelog</a></li>
            <li><a href="article.html">Contact form</a></li>
            <li><a href="article.html">Adding media to post and doc content</a></li>
            <li><a href="article.html">Adding table of contents to docs</a></li>
            <li><a href="article.html">Adding alerts to content</a></li>
        </ul>
        <h5 class="uk-margin-top">Customization</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li><a href="article.html">Translation</a></li>
            <li><a href="article.html">Customization</a></li>
            <li><a href="article.html">Development</a></li>
            <li><a href="article.html">Sources and credits</a></li>
        </ul>
        <h5 class="uk-margin-top">Help</h5>
        <ul class="uk-nav uk-nav-default doc-nav">
            <li><a href="article.html">Contacting support</a></li>
        </ul>
    </div>
</div>

@include('barabaas::knowledgebase.footer')
