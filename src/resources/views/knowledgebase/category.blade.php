
@include('barabaas::knowledgebase.header',['small'=>true])


@php
        
        $category = null;
        $articles = null;

        if(isset($key)){
        $category = \BaraBaaS\Knowledgebase::getCategory($key);
        $articles = \BaraBaaS\Knowledgebase::getArticles($key);
        }

          
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $logo = DevSpark\Engine\Config::$logo;
    

      @endphp



<div class="uk-section uk-section-muted">

   



  <div class="uk-container">

    @if (!$category)
      
    <div>
        <div class="uk-card uk-card-default uk-card-body">
            <h3 class="uk-card-title">{{ \BaraBaaS\Knowledgebase::Lang('Кажется, такой категории не существует.') }}</h3>
            <p>
              {{ \BaraBaaS\Knowledgebase::Lang('К сожалению, мы не можем отобразить запрошенную категорию. Возможно, она была удалена или перемещена в другое место. Если вы уверены, что категория должна быть доступна, попробуйте обновить страницу или свяжитесь с нашей службой поддержки для получения дополнительной информации.') }}</p>
        </div>
    </div>

   @else
    
    <ul class="uk-breadcrumb">
      <li><a  onclick="goMain()">{{ \BaraBaaS\Knowledgebase::Lang('База Знаний') }}</a></li>
      <li><span>{{$category->title}}</span></li>
    </ul>
    <div class="uk-grid-small" data-uk-grid>
      <div class="categpry-icon uk-width-auto">
        {!!$category->icon!!}
      </div>
      <div class="uk-width-expand">
        <h1 class="uk-article-title uk-margin-remove">{{$category->title}}</h1>
        <p class=" uk-text-muted uk-margin-small-top">{{$category->description}}</p>
      </div>
    </div>
    <div class="uk-margin-medium-top">

        @if (!$articles)
            
        @else
            
            @foreach ($articles as $article)
                

            <div class="uk-card uk-card-category uk-card-default uk-card-hover uk-card-body uk-inline uk-border-rounded uk-width-1-1">
                <a class="uk-position-cover" onclick="goArticle('{{$article->url}}')" ></a>
                <h3 class="uk-card-title uk-margin-remove">{{$article->title}}</h3>
                <p class="uk-margin-small-top uk-article-preview uk-text-muted">
                    {{mb_substr(str_replace("&nbsp;",'',strip_tags($article->content)),0,1000).'...'}}
                </p>

               
                <div class="uk-article-meta uk-flex uk-flex-middle">
                  <img class="uk-border-circle uk-avatar-small" src="{{$logo}}">
                  <div>
                    {{$title}}
                    <br>
                    {{ \BaraBaaS\Knowledgebase::Lang('Обновлено:')}} <time datetime="2019-12-30">{{date("d.m.Y", $article->updated_at)}}</time>
                  </div>
                </div>
              </div>

            @endforeach


        @endif

    </div>
   
    @endif 
  </div>
</div>


@include('barabaas::knowledgebase.footer')
