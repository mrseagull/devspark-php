
    @php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $logo = DevSpark\Engine\Config::$logo;
    
	$cover = DevSpark\Engine\Config::$cover;
    
@endphp






<!DOCTYPE html>
<html lang="ru" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ \BaraBaaS\Knowledgebase::Lang('База знаний') }} - {{ $title }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ $logo }}" >
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.16.6/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.16.6/js/uikit-icons.min.js"></script>

    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;500;700&display=swap" rel="stylesheet">

    @include('barabaas::knowledgebase.decor')
</head>

<body>
@if(!isset($ghost))
<header class="uk-background-primary uk-background-norepeat uk-background-blend-multiply uk-background-cover uk-background-center-center uk-light" 
	style="background-image: url({{ $cover }});">
	<nav class="uk-navbar-container">
	  <div class="uk-container">
	    <div data-uk-navbar>
	      <div class="uk-navbar-left">
	        <a class="uk-navbar-item uk-logo uk-visible@m"  onclick="goMain()">{{$title}}</a>
	   
	      </div>
	      <div class="uk-navbar-center uk-hidden@m">
	        <a class="uk-navbar-item uk-logo"  onclick="goMain()">{{$title}}</a>
	      </div>
	      <div class="uk-navbar-right">
	        <ul class="uk-navbar-nav uk-visible@m">
	          <li class="uk-active"><a   onclick="goMain()">{{ \BaraBaaS\Knowledgebase::Lang('Категории') }}</a></li>
	        
	          <!--li ><a href="#">{{ \BaraBaaS\Knowledgebase::Lang('Обновления') }}</a></li-->
	          <li>

             
	            <a id="active-lang"></a>
	            <div class="uk-navbar-dropdown">
	              <ul class="uk-navbar-dropdown-nav uk-nav-parent-icon" data-uk-nav>
                    <li><a  onclick="changeLanguage('ru')">🇷🇺 Русский</a></li>
                    <li><a onclick="changeLanguage('en')">🇺🇸 English</a></li>
	                <li><a onclick="changeLanguage('de')" href="#">🇩🇪 Deutsch</a></li>
                 
	              </ul>
	            </div>            
	          </li>
	          <li>
	            <div class="uk-navbar-item">
	              <a class="uk-button uk-button-small uk-button-primary-outline" href="javascript:jivo_api.open()">{{ \BaraBaaS\Knowledgebase::Lang('Написать нам') }}</a>
	            </div>
	          </li>
	        </ul>
	        <a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas" data-uk-toggle><span
	          data-uk-navbar-toggle-icon></span> <span class="uk-margin-small-left">{{ \BaraBaaS\Knowledgebase::Lang('Меню') }}</span></a>
	      </div>
	    </div>
	  </div>
	</nav>
	

    @if (isset($small))
        
    <div class="uk-section uk-section-small uk-section-hero uk-position-relative" data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true">
		<div class="uk-container">
			<div class="hero-search uk-margin-bottom">
				<div class="uk-position-relative">
					<form class="uk-search uk-search-default uk-width-1-1" name="search-hero" onsubmit="return false;">
						<span data-uk-search-icon="ratio: 1.2"></span>
						<input id="search-hero" class="uk-search-input uk-form-large uk-border-rounded" type="search"
							placeholder="{{ \BaraBaaS\Knowledgebase::Lang('Поиск ответов') }}" autocomplete="off" data-minchars="1" data-maxitems="30">
					</form>
				</div>
			</div>
		</div>
	</div>
    @else
    <div class="uk-section uk-position-relative uk-position-z-index" data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true">
		<div class="uk-container">
			<h1 class="uk-text-center uk-margin-remove-top">{{ \BaraBaaS\Knowledgebase::Lang('База знаний') }} {{$title}}</h1>
			<div class="hero-search uk-margin-medium-top">
				<form class="uk-search uk-search-default uk-width-1-1" name="search-hero" onsubmit="return false;">
					<span data-uk-search-icon="ratio: 1.2"></span>
					<input id="search-hero" class="uk-search-input uk-form-large uk-border-rounded" type="search"
						placeholder="{{ \BaraBaaS\Knowledgebase::Lang('Поиск ответов') }}" autocomplete="off" data-minchars="1" data-maxitems="30">
				</form>
			</div>
		</div>
	</div>
    @endif

	
</header>

@endif

