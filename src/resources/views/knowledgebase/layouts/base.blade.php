
@include('barabaas::knowledgebase.header')

@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    
   
@endphp




<div class="uk-section uk-section-muted">
	<div class="uk-container">
		<div class="uk-child-width-1-2@m uk-grid-match- uk-grid-small" data-uk-grid>

      @php
        
        $categories = \BaraBaaS\Knowledgebase::getCategories();

        

      @endphp

      @foreach ($categories as $category)
      <div>
				<div class="uk-card uk-card uk-card-default uk-card-hover uk-card-body uk-inline uk-border-rounded">
					<a class="uk-position-cover" href="{{$category->url}}"></a>
					<div class="uk-grid-small" data-uk-grid>
						<div class="categpry-icon uk-width-auto uk-text-primary uk-flex uk-flex-middle">
              {!!$category->icon!!}
						</div>
						<div class="uk-width-expand">
							<h3 class="uk-card-title uk-margin-remove uk-text-primary">{{$category->title}}</h3>
							<p class="uk-text-muted uk-margin-remove">Работа с данными раздела "{{$category->title}}" в административной панели.</p>
						</div>
					</div>
				</div>
			</div>

      @endforeach
   
		</div>
	</div>
</div>

<div class="uk-section uk-section-muted">
	<div class="uk-container uk-container-small">
		<h2 class="uk-text-center">Часто задаваемые вопросы</h2>
		<ul class="uk-margin-medium-top" data-uk-accordion="multiple: true">
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Какие темы охватывает база знаний?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>База знаний охватывает широкий спектр тем, включая инструкции по использованию сервиса, решения проблем, технические характеристики и многое другое.</p>
				</div>
			</li>
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Как я могу найти нужную мне информацию в базе знаний?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>Вы можете воспользоваться функцией поиска на странице базы знаний и ввести ключевые слова, чтобы найти нужную вам информацию.</p>
					
				</div>
			</li>
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Я не могу найти ответ на свой вопрос в базе знаний. Что мне делать?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>Если вы не можете найти ответ на свой вопрос в базе знаний, вы можете связаться с нашей службой поддержки клиентов и получить ответ на свой вопрос.</p>
				
				</div>
			</li>
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Обновляется ли информация в базе знаний?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>Да, информация в базе знаний регулярно обновляется и дополняется новыми материалами.</p>
				</div>
			</li>
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Могу ли я предложить свои материалы для включения в базу знаний?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>Да, мы приветствуем предложения наших клиентов и готовы рассмотреть возможность включения вашей информации в базу знаний.</p>
				</div>
			</li>
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Как я могу оценить полезность материалов в базе знаний?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>На странице каждой статьи в базе знаний есть кнопка "Да" или "Нет", которую вы можете нажать, чтобы оценить полезность статьи.</p>
				</div>
			</li>
			<li>
				<a class="uk-accordion-title uk-box-shadow-hover-small" href="#">Как я могу предложить улучшения для базы знаний?</a>
				<div class="uk-article-content uk-accordion-content link-primary">
					<p>Если у вас есть предложения по улучшению базы знаний, вы можете связаться с нашей службой поддержки клиентов и поделиться своими идеями. Мы всегда готовы услышать ваши предложения и рассмотреть возможность их внедрения.</p>
				</div>
			</li>
		</ul>
	</div>
</div>

<div class="uk-section uk-section-muted">
	<div class="uk-container">
		<h2 class="uk-text-center">Не нашли ответ?</h2>
		<p class="uk-text-muted uk-text-center uk-text-lead">Наша команда находится на расстоянии одного сообщения и готова ответить
      ваши вопросы.</p>
		<div class="uk-margin-medium-top uk-flex-center uk-text-center uk-margin-medium-top uk-grid-small" data-uk-grid>
			<div>
				<div class="uk-card">
					<img class="uk-avatar uk-border-circle" src="https://source.unsplash.com/n4KewLKFOZw/200x200" alt="Evan Wells" />
					<h5 class="uk-margin-remove-bottom uk-margin-small-top">Evan Wells</h5>
					<p class="uk-article-meta uk-margin-xsmall-top">Support</p>
				</div>
			</div>
			<div>
				<div class="uk-card">
					<img class="uk-avatar uk-border-circle" src="https://source.unsplash.com/d2MSDujJl2g/200x200" alt="John Brown" />
					<h5 class="uk-margin-remove-bottom uk-margin-small-top">John Brown</h5>
					<p class="uk-article-meta uk-margin-xsmall-top">Support</p>
				</div>
			</div>
			<div>
				<div class="uk-card">
					<img class="uk-avatar uk-border-circle" src="https://source.unsplash.com/OjYD2ADfhjU/200x200" alt="Sara Galen" />
					<h5 class="uk-margin-remove-bottom uk-margin-small-top">Sara Galen</h5>
					<p class="uk-article-meta uk-margin-xsmall-top">Lead Developer</p>
				</div>
			</div>
			
			<div>
				<div class="uk-card">
					<img class="uk-avatar uk-border-circle" src="https://source.unsplash.com/8PMvB4VyVXA/200x200" alt="Daniel Shultz" />
					<h5 class="uk-margin-remove-bottom uk-margin-small-top">Daniel Shultz</h5>
					<p class="uk-article-meta uk-margin-xsmall-top">Developer</p>
				</div>
			</div>
			<div>
				<div class="uk-card">
					<img class="uk-avatar uk-border-circle" src="https://source.unsplash.com/5EIW3DDX6dw/200x200" alt="John Brown" />
					<h5 class="uk-margin-remove-bottom uk-margin-small-top">John Brown</h5>
					<p class="uk-article-meta uk-margin-xsmall-top">Support</p>
				</div>
			</div>
		</div>
		<div class="uk-margin-medium-top uk-text-center" 
			data-uk-scrollspy="cls: uk-animation-slide-bottom-medium; repeat: true">
			<a class="uk-button uk-button-primary uk-border-rounded" href="contact.html">Contact Us</a>
		</div>
	</div>
</div>

<div id="offcanvas-docs" data-uk-offcanvas="overlay: true">
  <div class="uk-offcanvas-bar">
    <button class="uk-offcanvas-close" type="button" data-uk-close></button>
    <h5 class="uk-margin-top">Getting Started</h5>
    <ul class="uk-nav uk-nav-default doc-nav">
      <li class="uk-active"><a href="article.html">Template setup</a></li>
      <li><a href="article.html">Basic theme setup</a></li>
      <li><a href="article.html">Navigation bar</a></li>
      <li><a href="article.html">Footer options</a></li>
      <li><a href="article.html">Creating your first post</a></li>
      <li><a href="article.html">Creating docs posts</a></li>
      <li><a href="article.html">Enabling comments</a></li>
      <li><a href="article.html">Google Analytics</a></li>
    </ul>
    <h5 class="uk-margin-top">Product Features</h5>
    <ul class="uk-nav uk-nav-default doc-nav">
      <li><a href="article.html">Hero page header</a></li>
      <li><a href="article.html">Category boxes section</a></li>
      <li><a href="article.html">Fearured docs section</a></li>
      <li><a href="article.html">Video lightbox boxes section</a></li>
      <li><a href="article.html">Frequently asked questions section</a></li>
      <li><a href="article.html">Team members section</a></li>
      <li><a href="article.html">Call to action section</a></li>
      <li><a href="article.html">Creating a changelog</a></li>
      <li><a href="article.html">Contact form</a></li>
      <li><a href="article.html">Adding media to post and doc content</a></li>
      <li><a href="article.html">Adding table of contents to docs</a></li>
      <li><a href="article.html">Adding alerts to content</a></li>
    </ul>
    <h5 class="uk-margin-top">Customization</h5>
    <ul class="uk-nav uk-nav-default doc-nav">
      <li><a href="article.html">Translation</a></li>
      <li><a href="article.html">Customization</a></li>
      <li><a href="article.html">Development</a></li>
      <li><a href="article.html">Sources and credits</a></li>
    </ul>
    <h5 class="uk-margin-top">Help</h5>
    <ul class="uk-nav uk-nav-default doc-nav">
      <li><a href="article.html">Contacting support</a></li>
    </ul>
  </div>
</div>

@include('barabaas::knowledgebase.footer')
