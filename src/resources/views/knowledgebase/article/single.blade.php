@php
   
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;
    
    $tableScreen = '/knowledgebase/assets/table.jpg';
    
@endphp

<div class="entry-content uk-margin-medium-top">

    @if (isset($group))
        <p>
            Чтобы изменить
            параметры "{{ $group->title }}",
            в главном меню админ. панели перейдите в раздел "{{ $object->title }}", затем выберите пункт
            "{{ $group->title }}".
        </p>

        <p>{!! $group->description !!}</p>


        <div class="device device-macbook-pro-2018">
            <div class="device-frame">


                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">
                <div class="select-frame-cnt"></div>

            </div>
            <div class="device-power">&nbsp;</div>
            <div class="device-home">&nbsp;</div>
        </div>


        @if ($group->fieldsKeys)
            @php
                $count = 0;
                $countEditFields = 0;
                
                $groupFields = $group->getFields($object);
                foreach ($groupFields as $field) {
                    if (!$field->onlyshow) {
                        $countEditFields = $countEditFields + 1;
                    }
                    $count = $count + 1;
                }
            @endphp
            @if ($count > 0)



                @if ($countEditFields > 0)
                    <p>
                        В "{{ $group->title }}" вам доступны для редактирования следующие
                        поля:
                    </p>



                    <table class="uk-table uk-table-divider uk-table-striped">
                        <tbody>
                            @foreach ($groupFields as $field)
                                @if ($field->onlyshow == false)
                                    <tr>
                                        <td>{{ $field->title }}</td>
                                        <td>


                                            {!! strlen($field->description) > 1 ? $field->description : $field->getInputDescription() !!}

                                        </td>
                                    </tr>
                                @endif
                            @endforeach

                        </tbody>
                    </table>
                @endif


            @endif
        @endif


        @if ($countEditFields > 0)
            <p>После внесений изменений, обязательно сохраните их нажав на кнопку - Сохранить
                изменения в левом
                нижнем
                углу
                экрана.</p>
        @endif



    @endif



    @if (isset($stats))


        <p>
            Чтобы просмотреть статистику
            "{{ $stats->title ?? "" }}",
            в главном меню админ. панели перейдите в раздел "{{ $object->title }}", затем выберите пункт
            "{{ $stats->title ?? "" }}".
        </p>

        <p>{!! $stats->tip['description'] ?? '' !!}</p>


        <div class="device device-macbook-pro-2018">
            <div class="device-frame">


                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">
                <div class="select-frame-cnt"></div>

            </div>
            <div class="device-power">&nbsp;</div>
            <div class="device-home">&nbsp;</div>
        </div>


        @if ($stats->blocks)

            <p>
                В "{{ $stats->title ?? "" }}" вам доступны следующие
                данные статистики:
            </p>




            @foreach ($stats->blocks as $block)
                <h3>
                    
                    @switch($block->type)
                        @case('table')
                            Таблица -
                            @break

                            @case('line')
                            График -
                            @break
                            @case('dashboard')
                            Дашборд -
                            @break
                    
                        @default
                            
                    @endswitch


                    {{ $block->title ?? "" }}</h3>


                
                <p>
                    {!! $block->description ?? '' !!}
                </p>
            @endforeach






        @endif

    @endif



</div>
