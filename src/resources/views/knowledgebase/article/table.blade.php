
@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;
    
    $object = BaraBaaS\Schema\Objects::get($key);
   
   
    $tableScreen = "/knowledgebase/assets/table.jpg";

    
    $deleteButton = "/knowledgebase/assets/delete-button.jpeg";

 
    

   
    $exportButton = "/knowledgebase/assets/export-button.jpg";

   
    $paginationButton = "/knowledgebase/assets/pagination-button.jpg";

  
    $colors = [
        0 => '#42A5F5',
        1 => '#EC407A',
        2 => '#757575',
        3 => '#26C6DA',
        4 => '#AB47BC',
        5 => '#9CCC65',
        6 => '#F57F17',
        7 => '#7E57C2',
        8 => '#5C6BC0',
        9 => '#6D4C41',
        10 => '#EF5350',
        11 => '#CDDC39',
        12 => '#66BB6A',
        13 => '#F4511E',
        14 => '#26A69A',
        15 => '#29B6F6',
        16 => '#546E7A',
    ];
    

    
@endphp

                    <div class="entry-content uk-margin-medium-top">
                        <p >
                            @switch($countIndex%3+1)
                                @case(1)
                                    В данном разделе админ. панели "{{ $object->title }}" вы сможете найти таблицу с
                                    основными сведениями, кратко
                                    изложенными в одном месте.
                                @break

                                @case(2)
                                    Добро пожаловать в раздел админ. панели "{{ $object->title }}"!
                                    <br /> Здесь помещена
                                    таблица с краткой
                                    информацией.
                                @break

                                @case(3)
                                    В админ. панели "{{ $object->title }}" вы сможете ознакомиться с таблицей, содержащей
                                    основную информацию,
                                    которая представлена в сокращенном виде.
                                @break
                            @endswitch
                        </p>






                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">


                                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">


                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>






                        <p>В таблице вы можете увидеть следующие поля:</p>
                            @php
                                $fieldsInTable = [];
                              
                                if ($object->intable) {
                                    foreach ($object->intable as $fieldKey) {
                                        if ($object->fields[$fieldKey]) {
                                            $fieldsInTable[] = $object->fields[$fieldKey];
                                        }
                                    }
                                } else {
                                    foreach ($object->fields as $field) {
                                        $object->intable[] = $field->key;
                                        $fieldsInTable[] = $field;
                                    }
                                }
                                
                            @endphp


                          
                     
                           
                           


                           <table class="uk-table uk-table-divider uk-table-striped">
                            
                            <tbody>
                                @foreach ($fieldsInTable as $val)
                                <tr>
                                    <td>{{$val->title}}</td>
                                    <td>
                                      
                                        {!! strlen($val->description) > 1 ? $val->description : $val->getInputDescription() !!}
                                        
                                        </td>
                                </tr>
                                @endforeach
                               
                            </tbody>
                        </table>
                     

                           
                           <p>Эти поля помогут вам быстро найти
                            необходимую информацию.</p>


                        <h2 id="animation-table">Работа с таблицей</h2>


                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">


                                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">
                                
                               
                                
                                <img style="position: absolute;
                                width: 300px;
                                z-index: 999;
                                bottom: 10%;
                                right: 20%;
                                border:2px solid #D32F2F;
                                "
                                        height="70px" src="{{ $paginationButton }}" />


                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>


                     

                        @switch($countIndex%3+1)
                            @case(1)
                                <p>Под таблицей вы найдете элементы управления, позволяющие перемещаться по страницам и выбирать
                                    количество записей,
                                    которые вы желаете видеть одновременно. Это помогает быстро находить нужную информацию и
                                    легко перемещаться между
                                    страницами.</p>
                            @break

                            @case(2)
                                <p>Под таблицей вы найдете навигацию по всем страницам с функцией выбора количества записей,
                                    отображаемых на одной
                                    странице. Это делает переход между страницами быстрым и удобным, а также помогает быстро
                                    найти нужную информацию.
                                </p>
                            @break

                            @case(3)
                                <p>Ниже таблицы вы найдете навигацию по страницам и возможность выбрать количество записей,
                                    которые вы хотите видеть
                                    на одной странице. Это позволяет удобно переключаться между страницами и быстро находить
                                    нужные записи.</p>
                            @break
                        @endswitch





                        @switch($countIndex%3+1)
                            @case(1)
                                <p>По умолчанию, таблица сортирует данные по дате создания. То есть, созданные раньше записи в
                                    таблице будут выше.
                                    Однако, вы можете отсортировать записи по другому критерию, просто нажав на название
                                    колонки. Это очень удобно,
                                    если
                                    вы хотите отсортировать записи по полю
                                @break

                                @case(2)
                                <p>По умолчанию, таблица сортирует данные по времени создания. Таким образом, более ранние
                                    записи в таблице будут выше.
                                    Однако, вы можете отсортировать записи по другому критерию, просто нажав на заголовок
                                    колонки. Это очень удобно,
                                    если вы хотите произвести сортировку по полю:
                                @break

                                @case(3)
                                <p>Таблица по умолчанию сортирует данные по дате создания, то есть, самые новые записи
                                    поднимаются вверх. Но вы можете
                                    изменить это, кликнув на название колонки. Это поможет сортировать данные по другим
                                    параметрам, например, по полю:
                                @break
                            @endswitch
                            @php
                                $field1 = null;
                                $field2 = null;
                                
                                foreach ($object->intable as $fieldKey) {
                                    if ($object->fields[$fieldKey]) {
                                        if (in_array($object->fields[$fieldKey]->input, ['phone', 'number', 'float', 'text', 'date', 'datetime'])) {
                                            if (!$field1) {
                                                $field1 = mb_strtolower($object->fields[$fieldKey]->title);
                                            } else {
                                                if (!$field2) {
                                                    $field2 = mb_strtolower($object->fields[$fieldKey]->title);
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            @endphp


                            {{ $field1 ?? '' }}@if ($field2)
                                или {{ $field2 }}
                            @endif.</p>


                        @if ($object->buttons)
                            <p>Вы также можете выбрать несколько записей в таблице, поставив галочку в начале строки
                                записи. Это позволяет
                                совершать быстрые групповые действия. Вы можете:
                                @php
                                    $buttonsTitle = [];
                                    foreach ($object->buttons as $button) {
                                        $buttonsTitle[] = mb_strtolower($button->title);
                                    }
                                @endphp
                                {{ implode(', ', $buttonsTitle) }}.
                                В открытом меню быстрых действий вы можете увидеть количество выбранных записей и
                                доступные
                                групповые действия.
                            </p>
                        @endif


                        <h2 id="animation-export">Выборка и экспорт</h2>

                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">


                              

                                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">

                                   
                                <img style="position: absolute;
                                width: 200px;
                                z-index: 999;
                                top: 8%;
                                right: 5%;
                                border:2px solid #D32F2F;
                                "
                                        height="70px" src="{{ $exportButton }}" />


                              
                              

                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>
                        <p>Над таблицей доступен быстрый поиск, который ищет по всем данным таблицы.
                            Если вы ищете конкретную запись, то
                            быстрый поиск поможет найти ее быстро и легко. Вы можете искать записи по полю
                            @php
                                $filedsTitle = [];
                                
                                foreach ($object->intable as $fieldKey) {
                                    if ($object->fields[$fieldKey]) {
                                        if (in_array($object->fields[$fieldKey]->input, ['phone', 'text', 'longtext'])) {
                                            $filedsTitle[] = mb_strtolower($object->fields[$fieldKey]->title);
                                        }
                                    }
                                }
                                
                            @endphp
                            {{ implode(', ', $filedsTitle) }} или по другим критериям.</p>


                        @if ($object->tabs && count($object->tabs) > 0)
                            <p>В разделе "{{ $object->title }}" записи разделенны на группы:<p>
                              
                              
                                <table class="uk-table uk-table-divider uk-table-striped">
                                   
                                    <tbody>
                                        @foreach ($object->tabs as $tabs) 
                                        <tr>
                                            <td>{{$tabs['title']}}</td>
                                            <td>
                                                {{$tabs['description'] ?? " - "}}
                                                </td>
                                        </tr>
                                        @endforeach
                                       
                                    </tbody>
                                </table>
                             
                                  
                              

                                
                                <p>Это позволяет быстро находить нужную информацию, не отвлекаясь на неактуальные записи.
                                Кроме того, вы можете
                                быстро переключаться между группами и быстро находить нужные записи.</p>


                            </p>
                        @endif



                        @php
                            $filters = $object->getFilterFields();
                        @endphp

                        @if (count($filters) > 0)
                            <p>Через фильтр вы можете сделать выборку интересующих вас записей. Например, вы можете
                                отфильтровать записи по
                                полю

                                @php
                                    $filedsTitle = [];
                                    
                                    foreach ($filters as $field) {
                                        $filedsTitle[] = mb_strtolower($field->title);
                                    }
                                    
                                @endphp



                                {{ $filedsTitle[0] }} @if (isset($filedsTitle[1]))
                                    или {{ $filedsTitle[1] }}
                                @endif. Это позволяет быстро находить нужную информацию и
                                упрощает
                                работу с
                                таблицей.</p>
                        @endif

                        <p>Все данные доступны для экспорта данных в Excel. @if (count($filters) > 0)
                                Также вы можете сделать выгрузку данных с учетом настроенных
                                фильтров.
                            @endif Это очень удобно, если вам нужно выгрузить данные для дальнейшей
                            обработки в другой
                            программе.</p>

                        <h2 id="animation-view">Просмотр записи</h2>

                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">


                                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">
                                <div class="select-frame-cnt"></div>

                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>


                      

                        <p>Чтобы перейти к просмотру @if ($object->access['update'])
                                или редактированию
                            @endif записи, нужно просто нажать на нужную строку в таблице. На
                            экране
                            просмотра вы сможете @if ($object->access['update'])
                                внести изменения и
                            @endif увидеть более подробную информацию.</p>



                        <p>💡 @switch($countIndex%3+1)
                                @case(1)
                                    Для вашего удоства мы постарались перенести весь функционал админ. панели в мобильный
                                    интерфейс. Вы можете пользоваться всеми его возможностями, используя только свой
                                    телефон.
                                @break

                                @case(2)
                                    Мы упростили использование админ. панели, перенеся его функционал в мобильное приложение,
                                    которое можно
                                    использовать на телефоне.
                                @break

                                @case(3)
                                    Вы можете пользоваться всеми функциями админ. панели на
                                    своём мобильном телефоне.
                                @break
                            @endswitch

                        </p>


                        <h2 id="animation-advice">Советы</h2>




                        <p>Вот несколько советов, которые помогут вам работать с разделом "{{ $object->title }}":</p>

                        <ul class="ol-pretty uk-list-large">



                            <li>😎 Используйте быстрый поиск, чтобы быстро находить нужные записи.</li>
                            <li>🤴 Используйте фильтры, чтобы упростить работу с таблицей.</li>
                            <li>🤟 Не забывайте о возможности групповых действий. Они помогут вам быстро изменить
                                информацию
                                для нескольких
                                записей
                                одновременно.</li>
                            <li>📄 Экспортируйте данные в Excel для дальнейшей обработки в другой программе. Это может
                                быть
                                полезно, если вам
                                нужно проанализировать данные или создать отчет.</li>
                            <li>❤️ Если у вас возникнут вопросы, не стесняйтесь обращаться к нашей службе поддержки. Мы
                                всегда готовы помочь вам
                                с любыми вопросами, связанными с разделом "{{ $object->title }}".</li>
                        </ul>
                        <p>
                            Надеемся, что эта информация была полезной! Если у вас есть какие-либо вопросы, не
                            стесняйтесь обращаться к нам.
                            Мы
                            всегда рады помочь!</p>



                    </div>
                 
                   