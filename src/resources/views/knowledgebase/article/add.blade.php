
@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;
    
    $object = BaraBaaS\Schema\Objects::get($key);


    $tableScreen = "/knowledgebase/assets/table.jpg";
    $addScreen = "/knowledgebase/assets/add.jpg";
    
    $colors = [
        0 => '#42A5F5',
        1 => '#EC407A',
        2 => '#757575',
        3 => '#26C6DA',
        4 => '#AB47BC',
        5 => '#9CCC65',
        6 => '#F57F17',
        7 => '#7E57C2',
        8 => '#5C6BC0',
        9 => '#6D4C41',
        10 => '#EF5350',
        11 => '#CDDC39',
        12 => '#66BB6A',
        13 => '#F4511E',
        14 => '#26A69A',
        15 => '#29B6F6',
        16 => '#546E7A',
    ];
    
@endphp
@php
    $menuItems = [];
@endphp



                    <div class="entry-content uk-margin-medium-top">
                        <p>
                            @switch($countIndex%3+1)
                                @case(1)
                                    Для создания новой записи в разделе "{{ $object->title }}" вам необходимо перейти в
                                    соответствующий раздел
                                    в админ. панели и нажать кнопку "Добавить" в правом верхнем углу экрана.
                                @break

                                @case(2)
                                    Чтобы создать новую запись в разделе "{{ $object->title }}", нужно перейти в админ. панель, зайти
                                    в соответствующий раздел и кликнуть на кнопку "Добавить", расположенную в правом верхнем
                                    углу экрана.
                                @break

                                @case(3)
                                    Чтобы создать новую запись в разделе "{{ $object->title }}", нужно перейти в этот раздел в
                                    админ. панели
                                    и нажать на кнопку "Добавить" в правом верхнем углу экрана.
                                @break
                            @endswitch
                        </p>


                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">


                                <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">
                                <div class="select-frame-top"></div>

                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>

                        <p>
                            @switch($countIndex%3+1)
                                @case(1)
                                    После этого вы перейдете на экран создания новой записи,
                                    где вам необходимо заполнить (как минимум) все обязательные поля, которые помечены
                                    звездочкой около названия
                                    поля ввода.
                                    Обратите внимание, что некоторые поля доступны только для уже созданных записей и помечены
                                    значком замка при
                                    создании.
                                @break

                                @case(2)
                                    Далее, вы перейдете на страницу, где вы можете создать новую запись. Необходимо заполнить
                                    как минимум все
                                    обязательные поля, которые помечены звездочкой около названия поля ввода. Обратите внимание,
                                    что при создании
                                    некоторые поля могут быть недоступны и помечены значком замка, так как они доступны только
                                    для уже созданных
                                    записей.
                                @break

                                @case(3)
                                    После этого вы перейдете на страницу создания новой записи, где нужно заполнить обязательные
                                    поля, помеченные
                                    звездочкой
                                    около названия поля ввода. Имейте в виду, что в некоторых случаях некоторые поля могут быть
                                    недоступны для
                                    заполнения
                                    и помечены значком замка, так как доступны только для уже созданных записей.
                                @break
                            @endswitch
                        </p>


                        <h2 id="animation-nav">Навигация</h2>
                        @php
                            $menuItems['animation-nav'] = 'Навигация';
                        @endphp
                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">

                                <img class="device-screen" src="{{ $addScreen }}" loading="lazy">

                                <div class="select-frame-right"></div>
                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>



                        @if ($object->groups)
                            @if (count($object->groups) > 1)
                                <p>
                                    @switch($countIndex%3+1)
                                        @case(1)
                                            Все поля разбиты на группы, которые можно увидеть в левом меню экрана.
                                            Каждая группа содержит определенный набор полей, которые важно заполнить для
                                            создания полноценной записи в
                                            разделе "{{ $object->title }}".
                                            При переключении между группами данные, заполненные в других группах, не теряются и
                                            все также доступны, вы
                                            можете вернуться к ним позже.
                                        @break

                                        @case(2)
                                            Все поля находятся в группах, которые можно найти в левом меню экрана. Каждая группа
                                            содержит свой набор
                                            полей,
                                            которые нужно заполнить для создания полной записи в разделе
                                            "{{ $object->title }}". Если вы переключитесь
                                            на другую
                                            группу,
                                            данные, которые вы уже ввели в других группах, не исчезнут и всё ещё будут доступны.
                                            Вы можете вернуться к
                                            ним позже.
                                        @break

                                        @case(3)
                                            Все поля расположены в группах, которые находятся в левом меню экрана. Каждая группа
                                            содержит свой набор
                                            полей,
                                            которые нужно заполнить для создания полной записи в разделе
                                            "{{ $object->title }}". Если вы переключитесь
                                            на другую группу,
                                            то данные, которые уже введены в других группах, не исчезнут и будут доступны. Вы
                                            можете вернуться к ним
                                            позже.
                                        @break
                                    @endswitch
                                <p>

                                    @php
                                        $groupTitles = [];
                                        foreach ($object->groups as $group) {
                                            $groupTitles[] = $group->title;
                                        }
                                    @endphp


                                <p>Для раздела "{{ $object->title }}" доступны следующие группы:
                                    @foreach ($groupTitles as $val)
                                        <span class="uk-label"
                                            style="background: {{ $colors[$loop->index] }}">{{ $val }}</span>
                                    @endforeach
                                </p>


                                <p>Рассмотрим
                                    каждую из них подробнее.</p>
                            @endif
                        @endif

                        @foreach ($object->groups as $group)
                                @if ($group->fieldsKeys)
                                    @php
                                        $count = 0;
                                        $countEditFields  = 0;
                                        $groupFields = $group->getFields($object);
                                        foreach ($groupFields as $field) {
                                            if (!$field->onlyshow) {
                                                $countEditFields = $countEditFields + 1;
                                               
                                                $count = $count + 1;
                                            }
                                        }
                                    @endphp
                                    @if ($countEditFields > 0)
                                        <h2 id="animation-group-{{ $group->key }}"><span class="uk-label"
                                                style="background: {{ $colors[$loop->index] }}">ㅤㅤㅤ</span>
                                            {{ $group->title }}</h2>

                                        @php
                                            $menuItems['animation-group-' . $group->key] = "Группа: ".$group->title;
                                        @endphp

                                        <p>{{ $group->description }}</p>
                                        <p>
                                            В группе "{{ $group->title }}" вам доступны для редактирования следующие
                                            поля:
                                        </p>



                                        <table class="uk-table uk-table-divider uk-table-striped">
                                            <tbody>
                                                @foreach ($groupFields as $field)
                                                @if ($field->onlyshow == false)
                                                <tr>
                                                    <td>{{$field->title}}</td>
                                                    <td>
                                                        {!! strlen($field->description) > 1 ? $field->description : $field->getInputDescription() !!}
                                                    
                                                        </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                               
                                            </tbody>
                                        </table>
                             
                                        

                                    @endif

                                  
                                @endif
                            @endforeach
                        <h2 id="animation-save">Сохранить</h2>

                        @php
                            $menuItems['animation-save'] = 'Сохранить';
                        @endphp

                        <div class="device device-macbook-pro-2018">
                            <div class="device-frame">

                                <img class="device-screen" src="{{ $addScreen }}" loading="lazy">

                                <div class="select-frame-right-btm"></div>
                            </div>
                            <div class="device-power">&nbsp;</div>
                            <div class="device-home">&nbsp;</div>
                        </div>

                        <p>
                            @switch($countIndex%3+1)
                                @case(1)
                                    После того как вы заполните все необходимые поля, нажмите кнопку "Сохранить и создать
                                    запись" в левом нижнем углу
                                    экрана. Вы получите оповещение об успешно созданной записи.
                                @break

                                @case(2)
                                    После заполнения всех обязательных полей необходимо нажать на кнопку "Сохранить и создать
                                    запись" в левом нижнем
                                    углу экрана. После этого вы получите уведомление об успешном создании записи.
                                @break

                                @case(3)
                                    После заполнения всех обязательных полей нажмите кнопку "Сохранить и создать запись",
                                    расположенную в левом нижнем
                                    углу экрана. После этого вы получите уведомление о том, что запись была успешно создана.
                                @break
                            @endswitch
                        </p>

                        <h2 id="animation-advice">Советы</h2>

                        @php
                            $menuItems['animation-advice'] = 'Советы';
                        @endphp

                        <p>✍️ Не забывайте про правила орфографии и пунктуации. Наличие ошибок может отразиться на
                            восприятии.</p>
                        <p>👨‍👩‍👦 Помните о целевой аудитории. Старайтесь использовать язык, который понятен вашим
                            клиентам, и учитывайте
                            их интересы и потребности.</p>
                        <p>📸 Используйте яркие и привлекательные изображения, чтобы привлечь внимание к вашей записи.
                        <p>
                        <p>📅 Будьте внимательны при выборе даты и времени. Убедитесь, что они выбраны правильно и
                            соответствуют вашим планам.</p>

                        <p>🚀 Проверьте, что все данные, которые вы ввели, точны и актуальны перед сохранением записи.
                        </p>
                        <p>⚡️ Не забывайте проверять, обновлять и улучшать свои записи регулярно, чтобы они оставались
                            интересными и
                            актуальными для вашей аудитории.</p>

                        <p>Удачного использования!</p>




                    </div>
              
                   



