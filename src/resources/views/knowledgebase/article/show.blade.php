@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $cover = DevSpark\Engine\Config::$cover;
    $logo = DevSpark\Engine\Config::$logo;
    
    $object = BaraBaaS\Schema\Objects::get($key);
    
    $tableScreen = '/knowledgebase/assets/table.jpg';
    
    $viewScreen = '/knowledgebase/assets/view.jpg';
    
    $deleteScreen = '/knowledgebase/assets/delete.jpg';
    
    $deleteButton = '/knowledgebase/assets/delete-button.jpeg';
    
    $copyButton = '/knowledgebase/assets/copy-button.jpg';
    
    $colors = [
        0 => '#42A5F5',
        1 => '#EC407A',
        2 => '#757575',
        3 => '#26C6DA',
        4 => '#AB47BC',
        5 => '#9CCC65',
        6 => '#F57F17',
        7 => '#7E57C2',
        8 => '#5C6BC0',
        9 => '#6D4C41',
        10 => '#EF5350',
        11 => '#CDDC39',
        12 => '#66BB6A',
        13 => '#F4511E',
        14 => '#26A69A',
        15 => '#29B6F6',
        16 => '#546E7A',
    ];
    
@endphp

<div class="entry-content uk-margin-medium-top">
    <p>
        Чтобы просмотреть @if ($object->access['update'])
            или изменить
            @endif запись в разделе "{{ $object->title }}",
            перейдите в соответствующий раздел админ. панели и
            выберите нужную строку в таблице. На экране просмотра вы сможете @if ($object->access['update'])
                внести изменения и
            @endif увидеть более подробную
            информацию.
    </p>

    <div class="device device-macbook-pro-2018">
        <div class="device-frame">


            <img class="device-screen" src="{{ $tableScreen }}" loading="lazy">
            <div class="select-frame-cnt"></div>

        </div>
        <div class="device-power">&nbsp;</div>
        <div class="device-home">&nbsp;</div>
    </div>

    <p>
        Чтобы просмотреть @if ($object->access['update'])
            или изменить
            @endif запись в разделе "{{ $object->title }}",
            перейдите в соответствующий раздел админ. панели и
            выберите нужную строку в таблице. На экране просмотра вы сможете @if ($object->access['update'])
                внести изменения и
            @endif увидеть более подробную
            информацию.
    </p>


    <h2 id="animation-nav">Навигация</h2>
   
    <div class="device device-macbook-pro-2018">
        <div class="device-frame">

            <img class="device-screen" src="{{ $viewScreen }}" loading="lazy">

            <div class="select-frame-right-top"></div>
        </div>
        <div class="device-power">&nbsp;</div>
        <div class="device-home">&nbsp;</div>
    </div>



    <p>

        В меню записи раздела, расположенном в левом углу экрана, вы можете выбирать группы полей,
        выполнять быстрые
        действия,
        просматривать связанные данные и аналитику.</p>



    <h2 id="animation-edit">Просмотр
        @if ($object->access['update'] == true)
            и редактирование
        @endif
    </h2>

    <p>Все поля разбиты на группы, которые можно увидеть в левом меню экрана. В меню группы полей
        идут в начале
        списка блока: "Данные".</p>
    <p> При переключении между группами данные, заполненные в других группах, не теряются и все
        также доступны, вы
        можете вернуться к ним позже.
    <p>

        @php
            
            $objectGroups = $object->getGroups();
            
            $groupTitles = [];
            foreach ($objectGroups as $group) {
                if ($group->fieldsKeys) {
                    $groupTitles[] = $group->title;
                }
            }
        @endphp



        @if (count($groupTitles) > 0)
            <p>Для раздела "{{ $object->title }}" доступны следующие группы: @foreach ($groupTitles as $item)
                    <span class="uk-label" style="background: {{ $colors[$loop->index] }}">{{ $item }}</span>
                @endforeach
            </p>

        @endif
        @php
            
            $countEditFields = 0;
            
        @endphp

        @foreach ($objectGroups as $group)
            @if ($group->fieldsKeys)
                @php
                    $count = 0;
                    
                    $groupFields = $group->getFields($object);
                    foreach ($groupFields as $field) {
                        if (!$field->onlyshow) {
                            $countEditFields = $countEditFields + 1;
                        }
                        $count = $count + 1;
                    }
                @endphp
                @if ($count > 0)
                    <h2 id="animation-group-{{ $group->key }}"><span class="uk-label"
                            style="background: {{ $colors[$loop->index] }}">ㅤㅤㅤ</span>
                        {{ $group->title }}</h2>


                    <p>{{ $group->description }}</p>


                    @if ($countEditFields > 0)
                        <p>
                            В группе "{{ $group->title }}" вам доступны для редактирования следующие
                            поля:
                        </p>



                        <table class="uk-table uk-table-divider uk-table-striped">
                            <tbody>
                                @foreach ($groupFields as $field)
                                    @if ($field->onlyshow == false)
                                        <tr>
                                            <td>{{ $field->title }}</td>
                                            <td>


                                                {!! strlen($field->description) > 1 ? $field->description : $field->getInputDescription() !!}

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                            </tbody>
                        </table>
                    @endif


                    @if ($count > $countEditFields)
                        <p>
                            Группа содержит следующие информационные поля, которые не доступны для
                            редактирования.
                        </p>


                        <table class="uk-table uk-table-divider uk-table-striped">
                            <tbody>
                                @foreach ($groupFields as $field)
                                    @if ($field->onlyshow == true)
                                        <tr>
                                            <td>{{ $field->title }}</td>
                                            <td>
                                                {!! strlen($field->description) > 1 ? $field->description : $field->getInputDescription() !!}

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach

                            </tbody>
                        </table>
                    @endif
                @endif
            @endif
        @endforeach

        @if ($countEditFields > 0)
            <p>После внесений изменений, обязательно сохраните их нажав на кнопку - Сохранить
                изменения в левом
                нижнем
                углу
                экрана.</p>
        @endif


        @if ($object->access['add'])
            <h2 id="animation-copy">Копирование</h2>
            @php
                $menuItems['animation-copy'] = 'Копирование';
            @endphp


            <div class="device device-macbook-pro-2018">
                <div class="device-frame">

                    <img class="device-screen" src="{{ $viewScreen }}" loading="lazy">



                    <img style="position: absolute;
                                    width: 200px;
                                    z-index: 999;
                                    bottom: 15%;
                                    left: 5%;
                                    border:2px solid #D32F2F;
                                    "
                        height="70px" src="{{ $copyButton }}" />

                </div>
                <div class="device-power">&nbsp;</div>
                <div class="device-home">&nbsp;</div>
            </div>




            <p>Чтобы создать копию записи, нажмите Копировать в левом нижнем углу экрана, после
                этого вы перейдете
                на экран
                создания новой записи с уже заполнеными данными, продолжите работу как и при
                создании новой записи.
            </p>
        @endif

        @if ($object->access['delete'])
            <h2 id="animation-delete">Удаление</h2>
            @php
                $menuItems['animation-delete'] = 'Удаление';
            @endphp


            <div class="device device-macbook-pro-2018">
                <div class="device-frame">


                    <img class="device-screen" src="{{ $deleteScreen }}" loading="lazy">


                    <img style="position: absolute;
                                        width: 200px;
                                        z-index: 999;
                                        bottom: 15%;
                                        left: 5%;
                                        "
                        height="70px" src="{{ $deleteButton }}" />

                </div>
                <div class="device-power">&nbsp;</div>
                <div class="device-home">&nbsp;</div>
            </div>





            <p>Для удаления записи нажмите кнопку Удалить в нижнем левом углу экрана, при этом
                необходимо будет
                указать
                причину, так как данные
                будут
                удалены безвозвратно. Если вы случайно удалил важные данные, то срочно сообщите об
                этом нам.</p>
        @endif

        @php
            $countAddDataGroup = 0;
            
            foreach ($objectGroups as $group) {
                if ($group->from) {
                    $countAddDataGroup = $countAddDataGroup + 1;
                }
            }
            
        @endphp

        @php
            $fromGroupCount = 0;
            
            foreach ($objectGroups as $group) {
                if ($group->from) {
                    $fromGroupCount++;
                }
            }
            
        @endphp
        @if ($fromGroupCount)

            <h2 id="animation-from">Дополнительные данные</h2>


            <p>Эти таблицы выборки были созданы на основе данных из других разделов, которые представлены в
                готовом
                виде для
                удобного
                просмотра. Они содержат всю необходимую информацию, которую можно найти в
                соответствующих
                разделах, а также
                все
                доступные функции для дополнительной обработки данных. Например, вы можете
                сортировать по
                разным столбцам,
                фильтровать данные по заданным условиям или сделать экспорт для более детального
                анализа.
            </p>

            @foreach ($objectGroups as $group)
                @if ($group->from)
                    <h4 id="animation-from">{{ $group->title }}</h4>
                    @php
                        $from = \BaraBaaS\Schema\Objects::get($group->from);
                        $foreignField = $from->getField($group->foreignField);
                        $icon = $from->getIcon();
                    @endphp




                    <div class="uk-card  uk-card-primary  uk-card-body">


                        <div class="uk-grid-small uk-grid" data-uk-grid="">
                            <div class="white-icon uk-width-auto uk-flex uk-flex-middle uk-first-column">
                                {!! $icon !!}
                            </div>
                            <div class="uk-width-expand">
                                <p>Здесь представлена готовая выборка записей из раздела "{{ $from->title }}",
                                    которые связаны с текущей записью через поле "{{ $foreignField->title ?? '' }}".
                                </p>
                            </div>
                        </div>

                    </div>



                @endif
            @endforeach

         

          


            <p> Это позволяет быстро и эффективно работать с данными и получать ценную информацию
                для
                принятия важных
                решений.</p>

        @endif



</div>
