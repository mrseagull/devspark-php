@php
    
    $title = DevSpark\Engine\Config::$title;
    $subtitle = DevSpark\Engine\Config::$subtitle;
    $logo = DevSpark\Engine\Config::$logo;
    
@endphp

@if (!isset($ghost))
    <div id="offcanvas" data-uk-offcanvas="flip: true; overlay: true">
        <div class="uk-offcanvas-bar">
            <a class="uk-logo" onclick="goMain()">{{ $title }}</a>
            <button class="uk-offcanvas-close" type="button" data-uk-close></button>
            <ul class="uk-nav uk-nav-primary uk-nav-offcanvas uk-margin-top uk-text-center">
                <li class="uk-active"><a onclick="goMain()">{{ \BaraBaaS\Knowledgebase::Lang('Категории') }}</a></li>




                <li><a href="javascript:jivo_api.open()">{{ \BaraBaaS\Knowledgebase::Lang('Написать нам') }}</a></li>




            </ul>


         

        </div>
    </div>


    <footer class="uk-section uk-text-center uk-text-muted">
        <div class="uk-container uk-container-small">
            <div class="uk-text-meta">
                {{ \BaraBaaS\Knowledgebase::Lang('База знаний по работе в админ. панели ') }} - {{ $title }}.
                <br />
                {{ \BaraBaaS\Knowledgebase::Lang('Техническая поддержка предоставляется только клиентам с действующим договором.') }}
            </div>


        </div>
    </footer>


    <script>
        var data = window.location.pathname.split('/');
        var activeLang = null;
        var splice = 0;
        for (var i = 0; i < data.length; i++) {

            if (data[i].length == 2) {
                splice = data.length - i - 1;
                activeLang = data[i];
            }
        }
        if (splice)
            data.splice(splice * -1);

        var endpoint = data.join("/");
        console.log(endpoint, data, splice);


        if (activeLang) {

            var langs = {
                "ru": "🇷🇺 Русский",
                "en": "🇺🇸 English",
                "de": "🇩🇪 Deutsch",
                "kz": "🇰🇿 Қазақ тілі"
            }
            if (langs[activeLang]) {
                document.getElementById("active-lang").innerHTML = langs[activeLang];
            }

        }

        function goMain() {
            window.location.href = endpoint;
        }


        function goCategory(url) {

            window.location.href = endpoint + url;
        }


        function goArticle(url) {
            window.location.href = endpoint + url;
        }

        function setLike(like) {
            document.getElementsByClassName('like-block')[0].style.display = "none";


            if (like) {
                document.getElementsByClassName('like-plus')[0].style.display = "block";
            } else {
                document.getElementsByClassName('like-minus')[0].style.display = "block";
            }

        }

        function changeLanguage(language) {

            var data = window.location.pathname.split('/');

            var splice = 0;
            for (var i = 0; i < data.length; i++) {

                if (data[i].length == 2) {
                    data[i] = language;
                }
            }


            window.location.href = data.join('/');
        }
    </script>
@endif

<script src="//code.jivo.ru/widget/j8GOxM7CXb" async></script>

</body>

</html>
