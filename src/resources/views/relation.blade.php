<div class="flex" style="flex-wrap: wrap;">
    @if (isset($val) && count($val) > 0)
        @foreach ($val as $item)
            @if ($loop->index <= 2)
                <div class='border-2 mr-2 px-1 mb-1'><span class='mx-2'>{{ $item }}</span></div>
            @endif
        @endforeach
        @if (count($val) > 2)
            <div class='border-2 mr-2 px-1 mb-1'><span class='mx-2'> и еще {{ count($val) - 3 }}</span></div>
        @endif
    @endif

</div>
