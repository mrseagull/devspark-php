<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, viewport-fit=cover" />


    <script async src="https://unpkg.com/pwacompat" crossorigin="anonymous"></script>
    <meta name="theme-color" content="#000000" />
    <meta name="description" content="" />
    <title>{{ $title }}</title>

    <link rel="manifest" href="{{ $url }}/admin/assets/manifest.json?hash={{ $hash }}">
    <link rel="icon" href="{{ $logo }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ $logo }}">
    <meta name="theme-color" content="black" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="apple-mobile-web-app-title" content="{{ $title }}">
    <meta name="msapplication-TileImage" content="{{ $logo }}">
    <meta name="msapplication-TileColor" content="#000">

    <link rel="stylesheet" href="{{ $url }}/admin/assets/main.css?hash={{ $hash }}">
    <script>
        if ("serviceWorker" in navigator) {
            window.addEventListener("load", () => {
                navigator.serviceWorker && navigator.serviceWorker.register(
                    "{{ $url }}/admin/assets/sw.js?hash={{ $hash }}");
            });
        }
    </script>



</head>

<body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"></div>
</body>
<script url="{{ $url }}" src="{{ $url }}/admin/assets/main.js?hash={{ $hash }}"></script>

</html>
